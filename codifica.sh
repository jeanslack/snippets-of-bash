﻿#!/bin/bash
#codifica.sh
#Semplice script che codifica in mpe4 e xvid tramite mencoder
# Tratto dalla rivista 

echo "Scegli il codec"
select codec in "mpeg4" "xvid"
do
  if [ "$codec" = "mpeg4" ]; then  
     for f in "$@" ;
     do OUT=`basename "$f" .ogg`.avi
     mencoder "$f" -oac mp3lame -lameopts mode=2:cbr:br=128 -ovc lavc -lavcopts vcodec=mpeg4 -o "$OUT";
     done
     break;
  elif [ "$codec" = "xvid" ]; then
     for f in "$@" ;
     do OUT=`basename "$f" .ogg`.avi
     mencoder "$f" -oac mp3lame -lameopts mode=2:cbr:br=128 -ovc xvid -xvidencopts pass=1  -o "$OUT";
     done
     break;
  fi
done

