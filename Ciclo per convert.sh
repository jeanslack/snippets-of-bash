#!/bin/bash
 

for f in *.jpg
do
    echo $f
    convert -resize "65x65" "$f" "Resize-$f"
done
 
if ls | grep gif 
then 

    for s in *.gif
    do
	echo $s
	convert -resize "65x65" "$s" "Resize-$s"
    done
fi
