BASH=/bin/bash
BASHOPTS=checkwinsize:cmdhist:expand_aliases:extglob:extquote:force_fignore:histappend:interactive_comments:progcomp:promptvars:sourcepath
BASH_ALIASES=()
BASH_ARGC=()
BASH_ARGV=()
BASH_CMDS=()
BASH_COMPLETION=/etc/bash_completion
BASH_COMPLETION_COMPAT_DIR=/etc/bash_completion.d
BASH_COMPLETION_DIR=/etc/bash_completion.d
BASH_LINENO=()
BASH_SOURCE=()
BASH_VERSINFO=([0]="4" [1]="1" [2]="5" [3]="1" [4]="release" [5]="i486-pc-linux-gnu")
BASH_VERSION='4.1.5(1)-release'
COLORTERM=gnome-terminal
COLUMNS=80
DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-wJ0E7FnYru,guid=1f307a9be18ce8932b4a1aea00000049
DESKTOP_SESSION=default
DIRSTACK=()
DISPLAY=:0.0
EUID=1000
GDMSESSION=default
GDM_LANG=it_IT.UTF-8
GDM_XSERVER_LOCATION=local
GNOME_DESKTOP_SESSION_ID=this-is-deprecated
GNOME_KEYRING_CONTROL=/tmp/keyring-WnFxFe
GNOME_KEYRING_PID=1953
GPG_AGENT_INFO=/tmp/seahorse-sxiWr7/S.gpg-agent:2083:1
GROUPS=()
GTK_MODULES=canberra-gtk-module
GTK_RC_FILES=/etc/gtk/gtkrc:/home/gianluca/.gtkrc-1.2-gnome2
HISTCONTROL=ignoreboth
HISTFILE=/home/gianluca/.bash_history
HISTFILESIZE=500
HISTSIZE=500
HOME=/home/gianluca
HOSTNAME=debian
HOSTTYPE=i486
IFS=$' \t\n'
LANG=it_IT.UTF-8
LINES=24
LOGNAME=gianluca
LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:'
MACHTYPE=i486-pc-linux-gnu
MAILCHECK=60
OPTERR=1
OPTIND=1
ORBIT_SOCKETDIR=/tmp/orbit-gianluca
OSTYPE=linux-gnu
PATH=/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games:/home/gianluca/bin
PIPESTATUS=([0]="1" [1]="0")
PPID=2740
PS1='\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
PS2='> '
PS4='+ '
PWD=/home/gianluca
SESSION_MANAGER=local/debian:@/tmp/.ICE-unix/2028,unix/debian:/tmp/.ICE-unix/2028
SHELL=/bin/bash
SHELLOPTS=braceexpand:emacs:hashall:histexpand:history:interactive-comments:monitor
SHLVL=1
SSH_AGENT_PID=2069
SSH_AUTH_SOCK=/tmp/keyring-WnFxFe/ssh
TERM=xterm
UID=1000
USER=gianluca
USERNAME=gianluca
WINDOWID=41943044
WINDOWPATH=7
XAUTHORITY=/home/gianluca/.Xauthority
XDG_DATA_DIRS=/usr/share/gnome:/usr/local/share/:/usr/share/:/usr/share/gdm/
XDG_SESSION_COOKIE=1e53e049317f41ec0b3789ab0000000a-1281709333.261582-1791894282
_=
_scp_path_esc='[][(){}<>",:;^&\!$=?`|\ '\'']'
_ImageMagick () 
{ 
    local cur prev;
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -channel)
            COMPREPLY=($( compgen -W 'Red Green Blue Opacity \
                Matte Cyan Magenta Yellow Black' -- "$cur" ));
            return 0
        ;;
        -colormap)
            COMPREPLY=($( compgen -W 'shared private' -- "$cur" ));
            return 0
        ;;
        -colorspace)
            COMPREPLY=($( compgen -W 'GRAY OHTA RGB Transparent \
                XYZ YCbCr YIQ YPbPr YUV CMYK' -- "$cur" ));
            return 0
        ;;
        -compose)
            COMPREPLY=($( compgen -W 'Over In Out Atop Xor Plus \
                Minus Add Subtract Difference Multiply Bumpmap\
                Copy CopyRed CopyGreen CopyBlue CopyOpacity' -- "$cur" ));
            return 0
        ;;
        -compress)
            COMPREPLY=($( compgen -W 'None BZip Fax Group4 JPEG \
                Lossless LZW RLE Zip' -- "$cur" ));
            return 0
        ;;
        -dispose)
            COMPREPLY=($( compgen -W 'Undefined None Background Previous'                 -- "$cur" ));
            return 0
        ;;
        -encoding)
            COMPREPLY=($( compgen -W 'AdobeCustom AdobeExpert \
                AdobeStandard AppleRoman BIG5 GB2312 Latin2 \
                None SJIScode Symbol Unicode Wansung' -- "$cur"));
            return 0
        ;;
        -endian)
            COMPREPLY=($( compgen -W 'MSB LSB' -- "$cur" ));
            return 0
        ;;
        -filter)
            COMPREPLY=($( compgen -W 'Point Box Triangle Hermite \
                Hanning Hamming Blackman Gaussian Quadratic \
                Cubic Catrom Mitchell Lanczos Bessel Sinc' -- "$cur" ));
            return 0
        ;;
        -format)
            COMPREPLY=($( compgen -W "$( convert -list format |                 awk '/ [r-][w-][+-] / {print $1}' |                 tr -d '*' | tr [:upper:] [:lower:] )" -- "$cur" ));
            return 0
        ;;
        -gravity)
            COMPREPLY=($( compgen -W 'Northwest North NorthEast \
                West Center East SouthWest South SouthEast' -- "$cur" ));
            return 0
        ;;
        -intent)
            COMPREPLY=($( compgen -W 'Absolute Perceptual \
                Relative Saturation' -- "$cur" ));
            return 0
        ;;
        -interlace)
            COMPREPLY=($( compgen -W 'None Line Plane Partition' -- "$cur" ));
            return 0
        ;;
        -limit)
            COMPREPLY=($( compgen -W 'Disk File Map Memory' -- "$cur" ));
            return 0
        ;;
        -list)
            COMPREPLY=($( compgen -W 'Delegate Format Magic Module Resource \
                Type' -- "$cur" ));
            return 0
        ;;
        -map)
            COMPREPLY=($( compgen -W 'best default gray red green blue'                 -- "$cur" ));
            _filedir;
            return 0
        ;;
        -noise)
            COMPREPLY=($( compgen -W 'Uniform Gaussian Multiplicative \
                Impulse Laplacian Poisson' -- "$cur" ));
            return 0
        ;;
        -preview)
            COMPREPLY=($( compgen -W 'Rotate Shear Roll Hue \
                Saturation Brightness Gamma Spiff \
                Dull Grayscale Quantize Despeckle \
                ReduceNoise AddNoise Sharpen Blur \
                Treshold EdgeDetect Spread Shade \
                Raise Segment Solarize Swirl Implode \
                Wave OilPaint CharcoalDrawing JPEG' -- "$cur" ));
            return 0
        ;;
        -mask | -profile | -texture | -tile | -write)
            _filedir;
            return 0
        ;;
        -type)
            COMPREPLY=($( compgen -W 'Bilevel Grayscale Palette PaletteMatte \
                TrueColor TrueColorMatte ColorSeparation ColorSeparationlMatte \
                Optimize' -- "$cur" ));
            return 0
        ;;
        -units)
            COMPREPLY=($( compgen -W 'Undefined PixelsPerInch \
                PixelsPerCentimeter' -- "$cur" ));
            return 0
        ;;
        -virtual-pixel)
            COMPREPLY=($( compgen -W 'Constant Edge mirror tile' -- "$cur" ));
            return 0
        ;;
        -visual)
            COMPREPLY=($( compgen -W 'StaticGray GrayScale StaticColor \
                PseudoColor TrueColor DirectColor defaut visualid' -- "$cur" ));
            return 0
        ;;
    esac
}
__expand_tilde_by_ref () 
{ 
    if [ "${!1:0:1}" = "~" ]; then
        if [ "${!1}" != "${!1//\/}" ]; then
            eval $1="${!1/%\/*}"/'${!1#*/}';
        else
            eval $1="${!1}";
        fi;
    fi
}
__get_cword_at_cursor_by_ref () 
{ 
    local cword words=();
    __reassemble_comp_words_by_ref "$1" words cword;
    local i cur2;
    local cur="$COMP_LINE";
    local index="$COMP_POINT";
    for ((i = 0; i <= cword; ++i ))
    do
        while [[ "${#cur}" -ge ${#words[i]} && "${cur:0:${#words[i]}}" != "${words[i]}" ]]; do
            cur="${cur:1}";
            ((index--));
        done;
        if [[ "$i" -lt "$cword" ]]; then
            local old_size="${#cur}";
            cur="${cur#${words[i]}}";
            local new_size="${#cur}";
            index=$(( index - old_size + new_size ));
        fi;
    done;
    if [[ "${words[cword]:0:${#cur}}" != "$cur" ]]; then
        cur2=${words[cword]};
    else
        cur2=${cur:0:$index};
    fi;
    local "$2" "$3" "$4" && _upvars -a${#words[@]} $2 "${words[@]}" -v $3 "$cword" -v $4 "$cur2"
}
__gvfs_multiple_uris () 
{ 
    local IFS='
';
    local cur="${COMP_WORDS[COMP_CWORD]}";
    COMPREPLY=($(compgen -W '$(gvfs-ls --show-completions "$cur")' -- ""));
    case "$cur" in 
        *:*)
            case "$COMP_WORDBREAKS" in 
                *:*)
                    colon_prefix=$(echo $cur | sed 's/:[^:]*$/:/' );
                    COMPREPLY=${COMPREPLY##${colon_prefix}}
                ;;
            esac
        ;;
    esac
}
__linux_fstab_unescape () 
{ 
    eval $1="'${!1//\'/\047}'";
    eval $1="'${!1/%\\/\\\\}'";
    eval "$1=$'${!1}'"
}
__ltrim_colon_completions () 
{ 
    if [[ "$1" == *:* && ( ${BASH_VERSINFO[0]} -lt 4 || ( ${BASH_VERSINFO[0]} -ge 4 && "$COMP_WORDBREAKS" == *:* ) ) ]]; then
        local colon_word=${1%${1##*:}};
        local i=${#COMPREPLY[*]};
        while [ $((--i)) -ge 0 ]; do
            COMPREPLY[$i]=${COMPREPLY[$i]#"$colon_word"};
        done;
    fi
}
__reassemble_comp_words_by_ref () 
{ 
    local exclude i j ref;
    if [[ -n $1 ]]; then
        exclude="${1//[^$COMP_WORDBREAKS]}";
    fi;
    eval $3=$COMP_CWORD;
    if [[ -n $exclude ]]; then
        for ((i=0, j=0; i < ${#COMP_WORDS[@]}; i++, j++))
        do
            while [[ $i -gt 0 && -n ${COMP_WORDS[$i]} && ${COMP_WORDS[$i]//[^$exclude]} == ${COMP_WORDS[$i]} ]]; do
                [ $j -ge 2 ] && ((j--));
                ref="$2[$j]";
                eval $2[$j]=\${!ref}\${COMP_WORDS[i]};
                [ $i = $COMP_CWORD ] && eval $3=$j;
                (( $i < ${#COMP_WORDS[@]} - 1)) && ((i++)) || break 2;
            done;
            ref="$2[$j]";
            eval $2[$j]=\${!ref}\${COMP_WORDS[i]};
            [ $i = $COMP_CWORD ] && [[ -n ${COMP_WORDS[i]} ]] && eval $3=$j;
        done;
    else
        eval $2=\( \"\${COMP_WORDS[@]}\" \);
    fi
}
_alias () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    case $COMP_LINE in 
        *[^=])
            COMPREPLY=($( compgen -A alias -- "$cur" ))
        ;;
        *=)
            COMPREPLY=("$( alias ${cur%=} 2>/dev/null | sed                 -e 's|^alias '"$cur"'\(.*\)$|\1|' )")
        ;;
    esac
}
_animate () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-alpha -authenticate -backdrop \
            -background -bordercolor -borderwidth -channel \
            -coalesce -colormap -colors -colorspace -crop -debug \
            -decipher -define -delay -density -depth -display \
            -dispose -dither -extract -filter -flatten -font \
            -foreground -format -gamma -geometry -help \
            -iconGeometry -iconic -identify -immutable -interlace \
            -interpolate -limit -list -log -loop -map -mattecolor \
            -mattecolor -monitor -monochrome -name -page -pause \
            -quantize -quiet -regard-warnings -remote -repage \
            -resample -resize -respect-parenthesis -rotate \
            -sampling-factor -scenes -seed -set -shared-memory \
            -size -strip -title -transparent-color -treedepth \
            -trim -verbose -version -virtual-pixel -visual \
            -window' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug +dither +gamma +map +matte'             -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_apt_cache () 
{ 
    local cur prev special i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [ "$cur" != show ]; then
        for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
        do
            if [[ ${COMP_WORDS[i]} == @(add|depends|dotty|madison|policy|rdepends|show?(pkg|src|)) ]]; then
                special=${COMP_WORDS[i]};
            fi;
        done;
    fi;
    if [ -n "$special" ]; then
        case $special in 
            add)
                _filedir;
                return 0
            ;;
            showsrc)
                COMPREPLY=($( apt-cache dumpavail |                 command grep "^Source: $cur" | sort -u | cut -f2 -d" " ));
                return 0
            ;;
            *)
                COMPREPLY=($( apt-cache --no-generate pkgnames "$cur" 2> /dev/null ));
                return 0
            ;;
        esac;
    fi;
    case $prev in 
        -c | -p | -s | --config-file | --pkg-cache | --src-cache)
            _filedir;
            return 0
        ;;
        search)
            if [[ "$cur" != -* ]]; then
                return 0;
            fi
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h -v -p -s -q -i -f -a -g -c \
                -o --help --version --pkg-cache --src-cache \
                --quiet --important --full --all-versions \
                --no-all-versions --generate --no-generate \
                --names-only --all-names --recurse \
                --config-file --option --installed' -- "$cur" ));
    else
        COMPREPLY=($( compgen -W 'add gencaches show showpkg showsrc \
                stats dump dumpavail unmet search search \
                depends rdepends pkgnames dotty xvcg \
                policy madison' -- "$cur" ));
    fi;
    return 0
}
_apt_get () 
{ 
    local cur prev special i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        if [[ ${COMP_WORDS[i]} == @(install|remove|autoremove|purge|source|build-dep) ]]; then
            special=${COMP_WORDS[i]};
        fi;
    done;
    if [ -n "$special" ]; then
        case $special in 
            remove | autoremove | purge)
                if [ -f /etc/debian_version ]; then
                    COMPREPLY=($( _comp_dpkg_installed_packages $cur ));
                else
                    _rpm_installed_packages;
                fi;
                return 0
            ;;
            source)
                COMPREPLY=($( apt-cache --no-generate pkgnames "$cur"                     2> /dev/null ) $( apt-cache dumpavail |                     command grep "^Source: $cur" | sort -u | cut -f2 -d" " ));
                return 0
            ;;
            *)
                COMPREPLY=($( apt-cache --no-generate pkgnames "$cur"                     2> /dev/null ));
                return 0
            ;;
        esac;
    fi;
    case $prev in 
        -c | --config-file)
            _filedir;
            return 0
        ;;
        -t | --target-release | --default-release)
            COMPREPLY=($( apt-cache policy |                  command grep "release.o=Debian,a=$cur" |                  sed -e "s/.*a=\(\w*\).*/\1/" | uniq 2> /dev/null));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-d -f -h -v -m -q -s -y -u -t -b -c -o \
            --download-only --fix-broken --help --version --ignore-missing \
            --fix-missing --no-download --quiet --simulate --just-print \
            --dry-run --recon --no-act --yes --assume-yes --show-upgraded \
            --only-source --compile --build --ignore-hold --target-release \
            --no-upgrade --force-yes --print-uris --purge --reinstall \
            --list-cleanup --default-release --trivial-only --no-remove \
            --diff-only --no-install-recommends --tar-only --config-file \
            --option --auto-remove' -- "$cur" ));
    else
        COMPREPLY=($( compgen -W 'update upgrade dselect-upgrade \
            dist-upgrade install remove purge source build-dep \
            check clean autoclean autoremove' -- "$cur" ));
    fi;
    return 0
}
_aptitude () 
{ 
    local cur dashoptions prev special i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    dashoptions='-S -u -i -h --help --version -s --simulate -d \
        --download-only -P --prompt -y --assume-yes -F \
        --display-format -O --sort -w --width -f -r -g \
        --with-recommends -R -G --without-recommends -t \
        --target-release -V --show-versions -D --show-deps\
        -Z -v --verbose --purge-unused --schedule-only';
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        if [[ ${COMP_WORDS[i]} == @(@(|re)install|@(|un)hold|@(|un)markauto|@(dist|full)-upgrade|download|show|forbid-version|purge|remove|changelog|why@(|-not)|keep@(|-all)|build-dep|@(add|remove)-user-tag) ]]; then
            special=${COMP_WORDS[i]};
        fi;
        [[ ${COMP_WORDS[i]} == '-u' ]] && dashoptions=${dashoptions/-i};
        [[ ${COMP_WORDS[i]} == '-i' ]] && dashoptions=${dashoptions/-u};
    done;
    if [[ -n "$special" ]]; then
        case $special in 
            install | hold | markauto | unmarkauto | dist-upgrade | full-upgrade | download | show | changelog | why | why-not | build-dep | add-user-tag | remove-user-tag)
                COMPREPLY=($( apt-cache pkgnames $cur 2> /dev/null ));
                return 0
            ;;
            purge | remove | reinstall | forbid-version)
                COMPREPLY=($( _comp_dpkg_installed_packages "$cur" ));
                return 0
            ;;
            unhold)
                COMPREPLY=($( _comp_dpkg_hold_packages "$cur" ));
                return 0
            ;;
        esac;
    fi;
    case $prev in 
        autoclean | clean | forget-new | search | safe-upgrade | upgrade | update | keep-all)
            return 0
        ;;
        -S)
            _filedir;
            return 0
        ;;
        -t | --target-release | --default-release)
            COMPREPLY=($( apt-cache policy |                 command grep "release.o=Debian,a=$cur" |                 sed -e "s/.*a=\(\w*\).*/\1/" | uniq 2> /dev/null ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W "$dashoptions" -- "$cur" ));
    else
        COMPREPLY=($( compgen -W 'update upgrade safe-upgrade forget-new \
            clean autoclean install reinstall remove \
            hold unhold purge markauto unmarkauto why why-not \
            dist-upgrade full-upgrade download search show \
            forbid-version changelog keep-all build-dep \
            add-user-tag remove-user-tag' -- "$cur" ));
    fi;
    return 0
}
_aspell () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -c | -p | check | --conf | --personal | --repl | --per-conf)
            _filedir;
            return 0
        ;;
        --conf-dir | --data-dir | --dict-dir | --home-dir | --local-data-dir | --prefix)
            _filedir -d;
            return 0
        ;;
        dump | create | merge)
            COMPREPLY=($( compgen -W 'master personal repl' -- "$cur" ));
            return 0
        ;;
        --mode)
            COMPREPLY=($( compgen -W 'none url email sgml tex' -- "$cur" ));
            return 0
        ;;
        --sug-mode)
            COMPREPLY=($( compgen -W 'ultra fast normal bad-speller' -- "$cur" ));
            return 0
        ;;
        --keymapping)
            COMPREPLY=($( compgen -W 'aspell ispell' -- "$cur" ));
            return 0
        ;;
        -d | --master)
            _aspell_dictionary;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--conf --conf-dir --data-dir --dict-dir \
            --encoding --add-filter --rem-filter --mode \
            --add-extra-dicts --rem-extra-dicts \
            --home-dir --ignore --ignore-accents \
            --dont-ignore-accents --ignore-case --dont-ignore-case \
            --ignore-repl --dont-ignore-repl --jargon --keyboard \
            --lang --language-tag --local-data-dir --master \
            --module --add-module-search-order \
            --rem-module-search-order --per-conf --personal \
            --prefix --repl --run-together --dont-run-together \
            --run-together-limit --run-together-min --save-repl \
            --dont-save-repl --set-prefix --dont-set-prefix --size \
            --spelling --strip-accents --dont-strip-accents \
            --sug-mode --add-word-list-path --rem-word-list-path \
            --backup --dont-backup --reverse --dont-reverse \
            --time --dont-time --keymapping --add-email-quote \
            --rem-email-quote --email-margin --add-tex-command \
            --rem-tex-command --tex-check-comments \
            --dont-tex-check-comments --add-tex-extension \
            --rem-tex-extension --add-sgml-check --rem-sgml-check \
            --add-sgml-extension --rem-sgml-extension' -- "$cur" ));
    else
        COMPREPLY=($( compgen -W 'usage help check pipe list \
            config soundslike filter version dump create merge' -- "$cur" ));
    fi
}
_aspell_dictionary () 
{ 
    local datadir;
    datadir=$( aspell config data-dir 2>/dev/null || echo /usr/lib/aspell );
    COMPREPLY=($( command ls $datadir/*.alias 2>/dev/null ));
    COMPREPLY=(${COMPREPLY[@]%.alias});
    COMPREPLY=(${COMPREPLY[@]#$datadir/});
    COMPREPLY=("${COMPREPLY[@]}" $( aspell dicts 2>/dev/null ));
    COMPREPLY=($( compgen -W '${COMPREPLY[@]}' -- "$cur" ))
}
_available_interfaces () 
{ 
    local cmd;
    if [ "${1:-}" = -w ]; then
        cmd="iwconfig";
    else
        if [ "${1:-}" = -a ]; then
            cmd="ifconfig";
        else
            cmd="ifconfig -a";
        fi;
    fi;
    COMPREPLY=($( eval PATH="$PATH:/sbin" $cmd 2>/dev/null |         awk '/^[^ \t]/ { print $1 }' ));
    COMPREPLY=($( compgen -W '${COMPREPLY[@]/%[[:punct:]]/}' -- "$cur" ))
}
_avctrl () 
{ 
    local cur;
    COMPREPLY=();
    cur=`_get_cword`;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h --help -q --quiet' -- "$cur" ));
    else
        _count_args;
        if [ $args -eq 1 ]; then
            COMPREPLY=($( compgen -W 'discover switch' -- "$cur" ));
        fi;
    fi
}
_axi_cache () 
{ 
    local cur prev cmd;
    COMPREPLY=();
    COMP_WORDBREAKS=${COMP_WORDBREAKS//:};
    type _get_comp_words_by_ref &>/dev/null && { 
        _get_comp_words_by_ref -n: cur prev
    } || { 
        cur=$(_get_cword ":");
        prev=${COMP_WORDS[$COMP_CWORD-1]}
    };
    cmd=${COMP_WORDS[1]};
    case "$prev" in 
        *axi-cache*)
            COMPREPLY=($(compgen -W "help more search show again showpkg showsrc depends rdepends policy madison" -- "$cur"));
            return 0
        ;;
        --sort)
            COMPREPLY=($(compgen -W "$(egrep ^[a-z] /var/lib/apt-xapian-index/values | awk -F"\t" '{print $1}')" -- "$cur"));
            return 0
        ;;
    esac;
    case "$cmd" in 
        search | again)
            if [[ "$cur" == -* ]]; then
                COMPREPLY=($(compgen -W "--sort --tags" -- "$cur"));
                return 0;
            fi
        ;;
        show | showpkg | showsrc | depends | rdepends | policy | madison)
            if [[ "$cur" == -* ]]; then
                COMPREPLY=($(compgen -W "--last" -- "$cur"));
                return 0;
            fi
        ;;
        *)
            return 0
        ;;
    esac;
    if [ -n "$cur" ]; then
        COMPREPLY=($(compgen -W "$(${COMP_WORDS[@]} --tabcomplete=partial)" -- "$cur"));
    else
        COMPREPLY=($(compgen -W "$(${COMP_WORDS[@]} --tabcomplete=plain)" -- "$cur"));
    fi;
    return 0
}
_bluetooth_adresses () 
{ 
    if [ -n "${COMP_BLUETOOTH_SCAN:-}" ]; then
        COMPREPLY=(${COMPREPLY[@]:-} $( compgen -W "$( hcitool scan |             awk '/^\t/{print $1}' )" -- "$cur" ));
    fi
}
_bluetooth_devices () 
{ 
    COMPREPLY=(${COMPREPLY[@]:-} $( compgen -W "$( hcitool dev |         awk '/^\t/{print $1}' )" -- "$cur" ))
}
_bluetooth_packet_types () 
{ 
    COMPREPLY=($( compgen -W 'DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3'         -- "$cur" ))
}
_bluetooth_services () 
{ 
    COMPREPLY=($( compgen -W 'DID SP DUN LAN FAX OPUSH FTP HS HF HFAG \
        SAP NAP GN PANU HCRP HID CIP A2SRC A2SNK AVRCT AVRTG UDIUE \
        UDITE SYNCML' -- "$cur" ))
}
_bzip2 () 
{ 
    local cur prev xspec helpopts;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    helpopts=`_parse_help ${COMP_WORDS[0]}`;
    case $prev in 
        -b | -h | --help | -p)
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W "${helpopts//#/} -2 -3 -4 -5 -6 -7 -8 -9"             -- "$cur" ));
        return 0;
    fi;
    local IFS='	
';
    xspec="*.bz2";
    if [[ "$prev" == --* ]]; then
        [[ "$prev" == --decompress || "$prev" == --list || "$prev" == --test ]] && xspec="!"$xspec;
        [[ "$prev" == --compress ]] && xspec=;
    else
        if [[ "$prev" == -* ]]; then
            [[ "$prev" == -*[dt]* ]] && xspec="!"$xspec;
            [[ "$prev" == -*z* ]] && xspec=;
        fi;
    fi;
    _expand || return 0;
    COMPREPLY=($( compgen -f -X "$xspec" -- "$cur" ) $( compgen -d -- "$cur" ))
}
_bzr () 
{ 
    cur=${COMP_WORDS[COMP_CWORD]};
    prev=${COMP_WORDS[COMP_CWORD-1]};
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($( compgen -W "$(_bzr_commands)" $cur ));
    else
        if [ $COMP_CWORD -eq 2 ]; then
            case "$prev" in 
                help)
                    COMPREPLY=($( compgen -W "$(_bzr_commands) commands" $cur ))
                ;;
            esac;
        fi;
    fi
}
_bzr_commands () 
{ 
    bzr help commands | sed -r 's/^([-[:alnum:]]*).*/\1/' | grep '^[[:alnum:]]'
}
_cancel () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -W "$( lpstat | cut -d' ' -f1 )" -- "$cur" ))
}
_cd () 
{ 
    local cur IFS='	
' i j k;
    _get_comp_words_by_ref cur;
    if [[ "$cur" == ?(\\)\$* ]]; then
        COMPREPLY=($( compgen -v -P '$' -- "${cur#?(\\)$}" ));
        return 0;
    fi;
    compgen -f /non-existing-dir/ > /dev/null;
    if [[ -z "${CDPATH:-}" || "$cur" == ?(.)?(.)/* ]]; then
        _filedir -d;
        return 0;
    fi;
    local -r mark_dirs=$(_rl_enabled mark-directories && echo y);
    local -r mark_symdirs=$(_rl_enabled mark-symlinked-directories && echo y);
    for i in ${CDPATH//:/'	'};
    do
        k="${#COMPREPLY[@]}";
        for j in $( compgen -d $i/$cur );
        do
            if [[ ( -n $mark_symdirs && -h $j || -n $mark_dirs && ! -h $j ) && ! -d ${j#$i/} ]]; then
                j="${j}/";
            fi;
            COMPREPLY[k++]=${j#$i/};
        done;
    done;
    _filedir -d;
    if [[ ${#COMPREPLY[@]} -eq 1 ]]; then
        i=${COMPREPLY[0]};
        if [[ "$i" == "$cur" && $i != "*/" ]]; then
            COMPREPLY[0]="${i}/";
        fi;
    fi;
    return 0
}
_cd_devices () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -f -d -X "!*/?([amrs])cd*" -- "${cur:-/dev/}" ))
}
_cdrecord () 
{ 
    local cur prev i generic_options track_options track_mode;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [[ "$cur" == *=* ]]; then
        prev=${cur/=*/};
        cur=${cur/*=/};
        case $prev in 
            textfile | cuefile)
                _filedir;
                return 0
            ;;
            blank)
                COMPREPLY=($( compgen -W 'help all fast \
                    track unreserve trtail unclose session'                     -- "$cur" ));
                return 0
            ;;
            driveropts)
                COMPREPLY=($( compgen -W 'burnfree noburnfree\
                    varirec= audiomaster forcespeed noforcespeed\
                    speedread nospeedread singlesession \
                    nosinglesession hidecdr nohidecdr tattooinfo\
                    tattoofile=' -- "$cur" ));
                return 0
            ;;
        esac;
    fi;
    generic_options=(-version -v -V -d -silent -force -immed -dummy -dao -raw -raw96r -raw96p -raw16 -multi -msinfo -toc -atip -fix -nofix -waiti -load -lock -eject -format -setdropts -checkdrive -prcap -inq -scanbus -reset -abort -overburn -ignsize -useinfo -packet -noclose -text debug= kdebug= kd= minbuf= speed= blank= fs= dev= gracetime= timeout= driver= driveropts= defpregap= pktsize= mcn= textfile= cuefile=);
    track_options=(-audio -swab -data -mode2 -xa -xa1 -xa2 -xamix -cdi -isosize -pad padsize= -nopad -shorttrack -noshorttrack pregap= -preemp -nopreemp -copy -nocopy -scms tcsize= isrc= index=);
    track_mode=0;
    if [ $COMP_CWORD -gt 1 ]; then
        if [ -f "$prev" ]; then
            track_mode=1;
        else
            for ((i=0; i < ${#track_options[@]}; i++ ))
            do
                if [[ "${track_options[i]}" == "$prev" ]]; then
                    track_mode=1;
                    break;
                fi;
            done;
        fi;
    fi;
    _filedir;
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W '${track_options[@]}' -- "$cur" ));
    if [ $track_mode -eq 0 ]; then
        COMPREPLY=("${COMPREPLY[@]}" $( compgen -W '${generic_options[@]}' -- "$cur" ));
    fi
}
_chage () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -d | --lastday | -E | --expiredate | -h | --help | -I | --inactive | -m | --mindays | -M | --maxdays | -W | --warndays)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--lastday --expiredate --help --inactive \
            --list --mindays --maxdays --warndays' -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -u -- "$cur" ))
}
_chgrp () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    cur=${cur//\\\\/};
    _split_longopt && split=true;
    if [[ "$prev" == --reference ]]; then
        _filedir;
        return 0;
    fi;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        local w opts;
        for w in "${COMP_WORDS[@]}";
        do
            [[ "$w" == -@(R|-recursive) ]] && opts="-H -L -P" && break;
        done;
        COMPREPLY=($( compgen -W '-c -h -f -R -v --changes --dereference \
            --no-dereference --silent --quiet --reference --recursive \
            --verbose --help --version $opts' -- "$cur" ));
        return 0;
    fi;
    if [[ $COMP_CWORD -eq 1 && "$cur" != -* || "$prev" == -* ]]; then
        local IFS='
';
        COMPREPLY=($( compgen -g "$cur" 2>/dev/null ));
    else
        _filedir || return 0;
    fi;
    return 0
}
_chown () 
{ 
    local cur prev split=false;
    _get_comp_words_by_ref -n : cur prev;
    _split_longopt && split=true;
    case "$prev" in 
        --from)
            _usergroup;
            return 0
        ;;
        --reference)
            _filedir;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        local w opts;
        for w in "${COMP_WORDS[@]}";
        do
            [[ "$w" == -@(R|-recursive) ]] && opts="-H -L -P" && break;
        done;
        COMPREPLY=($( compgen -W '-c -h -f -R -v --changes --dereference \
            --no-dereference --from --silent --quiet --reference --recursive \
            --verbose --help --version $opts' -- "$cur" ));
    else
        local args;
        _count_args :;
        if [[ $args == 1 ]]; then
            _usergroup;
        else
            _filedir;
        fi;
    fi
}
_chpasswd () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -c | --crypt)
            COMPREPLY=($( compgen -W 'DES MD5 NONE SHA256 SHA512'                 -- "$cur" ));
            return 0
        ;;
        -s | --sha-rounds)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--crypt-method --encrypted \
            --help --md5 --sha-rounds' -- "$cur" ));
        return 0;
    fi
}
_chsh () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        --list-shells | --help | -v | --version)
            return 0
        ;;
        -s | --shell)
            _shells;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* && "$( uname -s )" == @(Linux|GNU|GNU/*) ]]; then
        COMPREPLY=($( compgen -W '--shell --list-shells --help --version'             -- "$cur" ));
    else
        COMPREPLY=($( compgen -u -- "$cur" ));
    fi;
    return 0
}
_ciptool () 
{ 
    local cur prev;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    case $prev in 
        -i)
            _bluetooth_devices;
            _bluetooth_adresses;
            return 0
        ;;
    esac;
    _get_command;
    if [ -z $command ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-h --help -i' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'show search connect release \
                loopback' -- "$cur" ));
        fi;
    else
        case $command in 
            @(connect|release|loopback))
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                fi
            ;;
        esac;
    fi
}
_command () 
{ 
    local offset i;
    offset=1;
    for ((i=1; i <= COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" != -* ]]; then
            offset=$i;
            break;
        fi;
    done;
    _command_offset $offset
}
_command_offset () 
{ 
    local cur func cline cspec noglob cmd i char_offset word_offset _COMMAND_FUNC _COMMAND_FUNC_ARGS;
    word_offset=$1;
    local first_word=${COMP_WORDS[$word_offset]};
    for ((i=0; i <= ${#COMP_LINE}; i++ ))
    do
        if [[ "${COMP_LINE:$i:${#first_word}}" == "$first_word" ]]; then
            char_offset=$i;
            break;
        fi;
    done;
    COMP_LINE=${COMP_LINE:$char_offset};
    COMP_POINT=$(( COMP_POINT - $char_offset ));
    for ((i=0; i <= COMP_CWORD - $word_offset; i++ ))
    do
        COMP_WORDS[i]=${COMP_WORDS[i+$word_offset]};
    done;
    for ((i; i <= COMP_CWORD; i++ ))
    do
        unset COMP_WORDS[i];
    done;
    COMP_CWORD=$(( $COMP_CWORD - $word_offset ));
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ $COMP_CWORD -eq 0 ]]; then
        COMPREPLY=($( compgen -c -- "$cur" ));
    else
        cmd=${COMP_WORDS[0]};
        if complete -p $cmd &>/dev/null; then
            cspec=$( complete -p $cmd );
            if [ "${cspec#* -F }" != "$cspec" ]; then
                func=${cspec#*-F };
                func=${func%% *};
                if [[ ${#COMP_WORDS[@]} -ge 2 ]]; then
                    $func $cmd "${COMP_WORDS[${#COMP_WORDS[@]}-1]}" "${COMP_WORDS[${#COMP_WORDS[@]}-2]}";
                else
                    $func $cmd "${COMP_WORDS[${#COMP_WORDS[@]}-1]}";
                fi;
                if [ "${cspec#*-o }" != "$cspec" ]; then
                    cspec=${cspec#*-o };
                    cspec=${cspec%% *};
                    if [[ "$cspec" != @(dir|file)names ]]; then
                        COMPREPLY=("${COMPREPLY[@]//\\\\:/:}");
                    fi;
                fi;
            else
                if [ -n "$cspec" ]; then
                    cspec=${cspec#complete};
                    cspec=${cspec%%$cmd};
                    COMPREPLY=($( eval compgen "$cspec" -- "$cur" ));
                fi;
            fi;
        fi;
    fi;
    [ ${#COMPREPLY[@]} -eq 0 ] && _filedir
}
_comp_dpkg_hold_packages () 
{ 
    command grep -B 2 'hold' /var/lib/dpkg/status | command grep "Package: $1" | cut -d\  -f2
}
_comp_dpkg_installed_packages () 
{ 
    command grep -A 1 "Package: $1" /var/lib/dpkg/status | command grep -B 1 -Ee "ok installed|half-installed|unpacked|             half-configured|config-files" -Ee "^Essential: yes" | command grep "Package: $1" | cut -d\  -f2
}
_compare () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-alpha -authenticate -channel \
            -colorspace -compress -debug -decipher -define \
            -density -depth -encipher -extract -format -fuzz \
            -help -highlight-color -identify -interlace -limit \
            -list -log -metric -monitor -passphrase -profile \
            -quality -quantize -quiet -regard-warnings \
            -respect-parenthesis -sampling-factor -seed -set \
            -size -transparent-color -type -verbose -version \
            -virtual-pixel' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_complete () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -o)
            COMPREPLY=($( compgen -W 'bashdefault default dirnames filenames \
                nospace plusdirs' -- "$cur" ));
            return 0
        ;;
        -A)
            COMPREPLY=($( compgen -W 'alias arrayvar binding \
                builtin command directory disabled enabled \
                export file function group helptopic hostname \
                job keyword running service setopt shopt \
                signal stopped user variable' -- "$cur" ));
            return 0
        ;;
        -C)
            COMPREPLY=($( compgen -A command -- "$cur" ));
            return 0
        ;;
        -F)
            COMPREPLY=($( compgen -A function -- "$cur" ));
            return 0
        ;;
        -p | -r)
            COMPREPLY=($( complete -p | sed -e 's|.* ||' ));
            COMPREPLY=($( compgen -W '${COMPREPLY[@]}' -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-a -b -c -d -e -f -g -j -k -o -s -v -u -A \
            -G -W -P -S -X -F -C' -- "$cur" ));
    else
        COMPREPLY=($( compgen -A command -- "$cur" ));
    fi
}
_composite () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-affine -alpha -authenticate \
            -blend -blue-primary -border -bordercolor -channel \
            -colors -colorspace -comment -compose -compress \
            -debug -decipher -define -density -depth -displace \
            -display -dispose -dissolve -dither -encipher \
            -encoding -endian -extract -filter -font -format \
            -geometry -gravity -green-primary -help -identify \
            -interlace -interpolate -label -limit -list -log \
            -monitor -monochrome -negate -page -profile -quality \
            -quantize -quiet -red-primary -regard-warnings \
            -repage -resize -respect-parenthesis -rotate \
            -sampling-factor -scene -seed -sharpen -shave -size \
            -stegano -stereo -strip -swap -thumbnail -tile \
            -transform -transparent-color -treedepth -type -units \
            -unsharp -verbose -version -virtual-pixel -watermark \
            -white-point -write' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+compress +debug +dither +endian +label \
            +matte +negate +page +write' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_configure () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        --prefix | --exec-prefix | --*dir)
            _filedir -d
        ;;
    esac;
    $split && return 0;
    [[ "$cur" != -* ]] && return 0;
    if [ -n "$COMP_CONFIGURE_HINTS" ]; then
        COMPREPLY=($( compgen -W "$( $1 --help 2>&1 |             awk '/^  --[A-Za-z]/ { print $1; \
            if ($2 ~ /--[A-Za-z]/) print $2 }' | sed -e 's/[[,].*//g' )"             -- "$cur" ));
    else
        COMPREPLY=($( compgen -W "$( $1 --help 2>&1 |             awk '/^  --[A-Za-z]/ { print $1; \
            if ($2 ~ /--[A-Za-z]/) print $2 }' | sed -e 's/[[,=].*//g' )"             -- "$cur" ));
    fi
}
_configured_interfaces () 
{ 
    if [ -f /etc/debian_version ]; then
        COMPREPLY=($( compgen -W "$( sed -ne 's|^iface \([^ ]\{1,\}\).*$|\1|p'            /etc/network/interfaces )" -- "$cur" ));
    else
        if [ -f /etc/SuSE-release ]; then
            COMPREPLY=($( compgen -W "$( printf '%s\n'             /etc/sysconfig/network/ifcfg-* |             sed -ne 's|.*ifcfg-\(.*\)|\1|p' )" -- "$cur" ));
        else
            if [ -f /etc/pld-release ]; then
                COMPREPLY=($( compgen -W "$( command ls -B             /etc/sysconfig/interfaces |             sed -ne 's|.*ifcfg-\(.*\)|\1|p' )" -- "$cur" ));
            else
                COMPREPLY=($( compgen -W "$( printf '%s\n'             /etc/sysconfig/network-scripts/ifcfg-* |             sed -ne 's|.*ifcfg-\(.*\)|\1|p' )" -- "$cur" ));
            fi;
        fi;
    fi
}
_conjure () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-debug -help -list -log -monitor -quiet \
            -regard-warnings -seed -verbose -version' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_convert () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-adaptive-blur -adaptive-resize \
            -adaptive-sharpen -adjoin -affine -alpha -annotate \
            -antialias -append -attenuate -authenticate \
            -auto-orient -average -background -bench -bias \
            -black-point-compensation -black-threshold \
            -blue-primary -blur -border -bordercolor -caption \
            -channel -charcoal -chop -clip -clip-mask -clip-path \
            -clone -clut -coalesce -colorize -colors -colorspace \
            -combine -comment -compose -composite -compress \
            -contrast -contrast-stretch -convolve -crop -cycle \
            -debug -decipher -deconstruct -define -delay -delete \
            -density -depth -despeckle -display -dispose -distort \
            -dither -draw -edge -emboss -encipher -encoding \
            -endian -enhance -equalize -evaluate -extent -extract \
            -family -fill -filter -flatten -flip -floodfill -flop \
            -font -format -frame -fuzz -fx -gamma -gaussian-blur \
            -geometry -gravity -green-primary -help -identify \
            -implode -insert -intent -interlace -interpolate \
            -label -lat -layers -level -limit -linear-stretch \
            -liquid-rescale -list -log -loop -map -mask \
            -mattecolor -median -modulate -monitor -monochrome \
            -morph -mosaic -motion-blur -negate -noise -normalize \
            -opaque -ordered-dither -orient -page -paint -ping \
            -pointsize -polaroid -posterize -preview -print \
            -process -profile -quality -quantize -quiet \
            -radial-blur -raise -random-threshold -recolor \
            -red-primary -regard-warnings -region -render -repage \
            -resample -resize -respect-parenthesis -reverse -roll \
            -rotate -sample -sampling-factor -scale -scene -seed \
            -segment -separate -sepia-tone -set -shade -shadow \
            -sharpen -shave -shear -sigmoidal-contrast -size \
            -sketch -solarize -splice -spread -stretch -strip \
            -stroke -strokewidth -style -swap -swirl -taint \
            -texture -threshold -thumbnail -tile -tile-offset \
            -tint -transform -transparent -transparent-color \
            -transpose -transverse -treedepth -trim -type \
            -undercolor -unique-colors -units -unsharp -verbose \
            -version -view -vignette -virtual-pixel -wave \
            -weight -white-point -white-threshold \
            -write' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+adjoin +append +compress \
            +contrast +debug +dither +endian +gamma +label +map \
            +mask +matte +negate +noise +page +raise +render \
            +write' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_count_args () 
{ 
    local i cword words;
    __reassemble_comp_words_by_ref "$1" words cword;
    args=1;
    for i in "${words[@]:1:cword-1}";
    do
        [[ "$i" != -* ]] && args=$(($args+1));
    done
}
_cpan2dist () 
{ 
    local cur prev packagelist cpandirs;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        --format)
            COMPREPLY=($( compgen -W '$(perl -MCPANPLUS::Dist -e \
                "print map { \"\$_\n\" } CPANPLUS::Dist->dist_types")'                 -- "$cur" ));
            return 0
        ;;
        --banlist | --ignorelist | --modulelist | --logfile)
            _filedir;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--help --skiptest --force --install \
            --verbose --keepsource --makefile --buildprereq \
            --archive --flushcache --defaults --edit-metafile \
            --format --ban --banlist --ignore --ignorelist \
            --modulelist --logfile --timeout --set-config \
            --set-program --dist-opts --default-banlist \
            --default-ignorelist' -- $cur ));
    else
        cpandirs=("$HOME/.cpanplus/" "$HOME/.cpan/source/modules/");
        for dir in ${cpandirs[@]};
        do
            [[ -d "$dir" && -r "$dir/02packages.details.txt.gz" ]] && packagelist="$dir/02packages.details.txt.gz";
        done;
        COMPREPLY=($( zgrep "^${cur//-/::}"             $packagelist 2>/dev/null             | awk '{print $1}' | sed -e 's/::/-/g' ));
    fi
}
_cpio () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    _split_longopt && split=true;
    case $prev in 
        -H | --format)
            _cpio_format;
            return 0
        ;;
        -E | -F | -I | --file | --pattern-file)
            _filedir;
            return 0
        ;;
        -R | --owner)
            _usergroup;
            return 0
        ;;
        --rsh-command)
            COMPREPLY=($( compgen -c -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($( compgen -W '-o --create -i --extract -p --pass-through \
                                   -? --help --license --usage --version' -- "$cur" ));
    else
        case ${COMP_WORDS[1]} in 
            -o | --create)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '-0 -a -c -v -A -B\
                        -L -V -C -H -M -O -F --file --format\
                        --message --null --reset-access-time\
                        --verbose --dot --append --block-size\
                        --dereference --io-size --quiet\
                        --force-local --rsh-command --help\
                        --version' -- "$cur" ));
                fi
            ;;
            -i | --extract)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '-b -c -d -f -m -n -r\
                        -t -s -u -v -B -S -V -C -E -H -M -R -I\
                        -F --file --make-directories\
                        --nonmatching\
                        --preserve-modification-time\
                        --numeric-uid-gid --rename -t --list\
                        --swap-bytes --swap --dot\
                        --unconditional --verbose --block-size\
                        --swap-halfwords --io-size\
                        --pattern-file --format --owner\
                        --no-preserve-owner --message\
                        --force-local --no-absolute-filenames\
                        --sparse --only-verify-crc --quiet\
                        --rsh-command --help\
                        --to-stdout \
                        --version' -- "$cur" ));
                fi
            ;;
            -p | --pass-through)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '-0 -a -d -l -m -u -v\
                        -L -V -R --null --reset-access-time\
                        --make-directories --link --quiet\
                        --preserve-modification-time\
                        --unconditional --verbose --dot\
                        --dereference --owner\
                        --no-preserve-owner --sparse --help\
                        --version' -- "$cur" ));
                else
                    _filedir -d;
                fi
            ;;
        esac;
    fi
}
_cpio_format () 
{ 
    COMPREPLY=($( compgen -W 'bin odc newc crc tar ustar hpbin hpodc' -- "$cur" ))
}
_cryptsetup () 
{ 
    local cur prev arg;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=`_get_pword`;
    case $prev in 
        --key-file | --master-key-file | --header-backup-file | -d)
            _filedir;
            return 0
        ;;
    esac;
    _get_first_arg;
    if [ -z $arg ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '--hash --cipher --verify-passphrase \
                --key-file --master-key-file --key-slot --key-size --size \
                --offset --skip --readonly --iter-time --batch-mode --timeout \
                --tries --align-payload --version' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'create remove status resize luksFormat \
                luksOpen luksClose luksSuspend luksResume luksAddKey \
                luksRemoveKey luksKillSlot luksDelKey luksUUID isLuks \
                luksDump luksHeaderBackup luksHeaderRestore' -- "$cur" ));
        fi;
    else
        _count_args;
        case $arg in 
            create)
                case $args in 
                    2)
                        _cryptsetup_name
                    ;;
                    3)
                        _cryptsetup_device
                    ;;
                esac
            ;;
            remove | status | resize | luksClose | luksSuspend | luksResume)
                case $args in 
                    2)
                        _cryptsetup_name
                    ;;
                esac
            ;;
            luksFormat | luksAddKey | luksRemoveKey)
                case $args in 
                    2)
                        _cryptsetup_device
                    ;;
                    3)
                        _filedir
                    ;;
                esac
            ;;
            luksOpen)
                case $args in 
                    2)
                        _cryptsetup_device
                    ;;
                    3)
                        _cryptsetup_name
                    ;;
                esac
            ;;
            luksKillSlot | luksDelKey | luksUUID | isLuks | luksDump)
                case $args in 
                    2)
                        _cryptsetup_device
                    ;;
                esac
            ;;
            luksHeaderBackup | luksHeaderRestore)
                case $args in 
                    2)
                        _cryptsetup_device
                    ;;
                    3)
                        COMPREPLY=('--header-backup-file')
                    ;;
                esac
            ;;
        esac;
    fi;
    return 0
}
_cryptsetup_device () 
{ 
    cur=${cur:=/dev/};
    _filedir
}
_cryptsetup_name () 
{ 
    COMPREPLY=($( compgen -X control -W '$( command ls /dev/mapper )'         -- "$cur" ))
}
_dd () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref -n = cur;
    case $cur in 
        if=* | of=*)
            cur=${cur#*=};
            _filedir;
            return 0
        ;;
        conv=*)
            cur=${cur#*=};
            COMPREPLY=($( compgen -W 'ascii ebcdic ibm block unblock \
            lcase notrunc ucase swab noerror sync' -- "$cur" ));
            return 0
        ;;
    esac;
    _expand || return 0;
    COMPREPLY=($( compgen -W '--help --version' -- "$cur" ) $( compgen -W 'bs cbs conv count ibs if obs of seek skip'                -S '=' -- "$cur" ))
}
_debconf_show () 
{ 
    local cur;
    COMPREPLY=();
    cur=${COMP_WORDS[COMP_CWORD]};
    COMPREPLY=($( compgen -W '--listowners --listdbs --db=' -- $cur ) $( apt-cache pkgnames -- $cur ))
}
_deborphan () 
{ 
    local last_item_in_list;
    local cur=$2 prev=$3;
    local options='--help -h --version -v --status-file -f
    --show-deps -d --show-priority -P --show-size -z
    --show-section -s --no-show-section --nice-mode -n
    --all-packages -a --libdevel --exclude -e --priority -p
    --force-hold -H --find-config --add-keep -A --del-keep -R
    --list-keep -L --zero-keep -Z --keep-file -k
    --guess-perl --guess-python --guess-pike
    --guess-ruby --guess-interpreters
    --guess-section --guess-dev --guess-debug
    --guess-common --guess-data --guess-doc --guess-dummy
    --guess-all --guess-only
    ';
    COMPREPLY=();
    case "$prev" in 
        -@(-status-file|f|-keep-file|k))
            _filedir;
            return 0
        ;;
        -@(-add-keep|A|-del-keep|R))
            COMPREPLY=($( compgen -W "$( _comp_dpkg_installed_packages $cur ) -" -- $cur ));
            return 0
        ;;
        -@(-priority|p))
            COMPREPLY=($( compgen -W '1 2 3 4 5 required important standard optional extra' -- $cur));
            return 0
        ;;
        -@(-exclude|e))
            last_item_in_list="${cur##*,}";
            list_before_item="${cur%$last_item_in_list}";
            COMPREPLY=($(
                   compgen -S "," -P "$list_before_item" -W "$( _comp_dpkg_installed_packages $last_item_in_list )"
                  ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W "$options" -- $cur ));
    else
        COMPREPLY=($( _comp_dpkg_installed_packages $cur ));
    fi;
    return 0
}
_desktop_file_validate () 
{ 
    COMPRELY=();
    cur=${COMP_WORDS[COMP_CWORD]};
    _filedir '@(desktop)'
}
_dfutool () 
{ 
    local cur prev;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    case $prev in 
        -@(d|-device))
            _bluetooth_devices;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h --help -d --device' -- "$cur" ));
    else
        _count_args;
        case $args in 
            1)
                COMPREPLY=($( compgen -W 'verify modify \
                    upgrade archive' -- "$cur" ))
            ;;
            2)
                _filedir
            ;;
        esac;
    fi
}
_dhclient () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -cf | -lf | -pf | -sf)
            _filedir;
            return 0
        ;;
        -s)
            _known_hosts_real "$cur";
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-p -d -q -1 -r -lf -pf \
            -cf -sf -s -g -n -nw -w' -- "$cur" ));
    else
        _available_interfaces;
    fi
}
_display () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-alpha -antialias -authenticate \
            -auto-orient -backdrop -background -border \
            -bordercolor -borderwidth -channel -clip \
            -clip-path -coalesce -colormap -colors -colorspace \
            -comment -compress -contrast -crop -debug -decipher \
            -define -delay -density -depth -despeckle -display \
            -dispose -dither -edge -endian -enhance -extract \
            -filter -flatten -flip -flop -font -foreground \
            -format -frame -gamma -geometry -help -iconGeometry \
            -iconic -identify -immutable -interlace -interpolate \
            -label -limit -list -log -loop -map -mattecolor \
            -monitor -monochrome -name -negate -page -profile \
            -quality -quantize -quiet -raise -regard-warnings \
            -remote -repage -resample -resize \
            -respect-parenthesis -roll -rotate -sample \
            -sampling-factor -scenes -seed -segment -set \
            -shared-memory -sharpen -size -strip -texture -title \
            -transparent-color -treedepth -trim -update \
            -usePixmap -verbose -version -virtual-pixel -visual \
            -window -window-group -write' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+compress +contrast +debug +dither \
            +endian +gamma +label +map +matte +negate +page \
            +raise +write' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_dpkg () 
{ 
    local cur prev i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    i=$COMP_CWORD;
    _expand || return 0;
    _split_longopt;
    if [[ $cur != -* ]]; then
        while [[ $prev != -* && $i != 1 ]]; do
            i=$((i-1));
            prev=${COMP_WORDS[i-1]};
        done;
    fi;
    case $prev in 
        -c | -i | -A | -I | -f | -e | -x | -X | -W | --install | --unpack | --record-avail | --contents | --info | --fsys-tarfile | --field | --control | --extract | --show)
            _filedir '?(u)deb';
            return 0
        ;;
        -b | --build)
            _filedir -d;
            return 0
        ;;
        -s | -p | -l | --status | --print-avail | --list)
            COMPREPLY=($( apt-cache pkgnames "$cur" 2>/dev/null ));
            return 0
        ;;
        -S | --search)
            _filedir;
            return 0
        ;;
        -r | -L | -P | --remove | --purge | --listfiles)
            COMPREPLY=($( _comp_dpkg_installed_packages "$cur" ));
            return 0
        ;;
        *)
            COMPREPLY=($( compgen -W '-i --install --unpack -A --record-avail \
                --configure -r --remove -P --purge --get-selections \
                --set-selections --update-avail --merge-avail \
                --clear-avail  --command-fd --forget-old-unavail -s \
                --status -p --print-avail -L --listfiles -l --list \
                -S --search -C --audit --print-architecture \
                --print-gnu-build-architecture \
                --print-installation-architecture \
                --compare-versions --help --version --force-help \
                --force-all --force-auto-select --force-downgrade \
                --force-configure-any --force-hold --force-bad-path \
                --force-not-root --force-overwrite \
                --force-overwrite-diverted --force-bad-verify \
                --force-depends-version --force-depends \
                --force-confnew --force-confold --force-confdef \
                --force-confmiss --force-conflicts --force-architecture\
                --force-overwrite-dir --force-remove-reinstreq \
                --force-remove-essential -Dh \
                --debug=help --licence --admindir --root --instdir \
                -O --selected-only -E --skip-same-version \
                -G --refuse-downgrade -B --auto-deconfigure \
                --no-debsig --no-act -D --debug --status-fd \
                -b --build -I --info -f --field -c --contents \
                -x --extract -X --vextract --fsys-tarfile -e --control \
                --ignore-depends --abort-after' -- "$cur" ))
        ;;
    esac
}
_dpkg_reconfigure () 
{ 
    local cur prev opt;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -f | --frontend)
            opt=($( printf '%s\n' /usr/share/perl5/Debconf/FrontEnd/* ));
            opt=(${opt[@]##*/});
            opt=(${opt[@]%.pm});
            COMPREPLY=($( compgen -W '${opt[@]}' -- "$cur" ));
            return 0
        ;;
        -p | --priority)
            COMPREPLY=($( compgen -W 'low medium high critical' -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($(compgen -W '--frontend --priority --all --unseen-only \
                      --help --showold --force --terse' -- "$cur" ));
    else
        COMPREPLY=($( _comp_dpkg_installed_packages "$cur" ));
    fi
}
_dpkg_source () 
{ 
    local cur prev options work i action packopts unpackopts fields;
    packopts="-c -l -F -V -T -D -U -W -E -sa -i -I -sk -sr -ss -sA -sK -sP               -sU -sR";
    unpackopts="-sp -sn -su";
    options="-x -b $packopts $unpackopts";
    fields="Format Source Version Binary Maintainer Uploader Architecture             Standards-Version Build-Depends Files";
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    action="options";
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        if [[ ${COMP_WORDS[$i]} == "-x" ]]; then
            action=unpack;
        else
            if [[ ${COMP_WORDS[$i]} == "-b" ]]; then
                action=pack;
            else
                if [[ ${COMP_WORDS[$i]} == "-h" ]]; then
                    action=help;
                fi;
            fi;
        fi;
    done;
    case $action in 
        unpack)
            case $prev in 
                -x)
                    _filedir -d;
                    _filedir 'dsc';
                    return 0
                ;;
                *)
                    COMPREPLY=($( compgen -W "$unpackopts" -- "$cur" ));
                    _filedir -d;
                    _filedir;
                    return 0
                ;;
            esac;
            return 0
        ;;
        pack)
            case $prev in 
                -b)
                    _filedir -d;
                    return 0
                ;;
                -c | -l | -T | -i | -I)
                    _filedir -d;
                    _filedir;
                    return 0
                ;;
                -F)
                    COMPREPLY=($( command ls /usr/lib/dpkg/parsechangelog ));
                    return 0
                ;;
                -V | -D)
                    COMPREPLY=();
                    return 0
                ;;
                -D)
                    if [[ "$cur" == *=* ]]; then
                        COMPREPLY=();
                        return 0;
                    else
                        COMPREPLY=($( compgen -W "$fields" -- "$cur" ));
                        return 0;
                    fi
                ;;
                -U)
                    COMPREPLY=($( compgen -W "$fields" -- "$cur" ));
                    return 0
                ;;
                *)
                    COMPREPLY=($( compgen -W "$packopts $unpackopts" -- "$cur" ));
                    return 0
                ;;
            esac;
            return 0
        ;;
        *)
            COMPREPLY=($( compgen -W "$options" -- "$cur" ));
            return 0
        ;;
    esac
}
_dvd_devices () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -f -d -X "!*/?(r)dvd*" -- "${cur:-/dev/}" ))
}
_expand () 
{ 
    if [[ "$cur" == \~*/* ]]; then
        eval cur=$cur;
    else
        if [[ "$cur" == \~* ]]; then
            cur=${cur#\~};
            COMPREPLY=($( compgen -P '~' -u "$cur" ));
            [ ${#COMPREPLY[@]} -eq 1 ] && eval COMPREPLY[0]=${COMPREPLY[0]};
            return ${#COMPREPLY[@]};
        fi;
    fi
}
_export () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    case $COMP_LINE in 
        *=\$*)
            COMPREPLY=($( compgen -v -P '$' -- "${cur#*=\$}" ))
        ;;
        *[^=])
            COMPREPLY=($( compgen -v -S '=' -- "$cur" ))
        ;;
        *=)
            COMPREPLY=("$( eval echo -n \"$`echo ${cur%=}`\" |
                ( echo -n \'
                  sed -e 's/'\''/'\''\\\'\'''\''/g'
                  echo -n \' ) )")
        ;;
    esac
}
_faillog () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -h | --help | -l | --lock-time | -m | --maximum | -t | --time)
            return 0
        ;;
        -u | --user)
            COMPREPLY=($( compgen -u -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--all --help --lock-time \
            --maximum --reset --time --user' -- "$cur" ));
        return 0;
    fi
}
_filedir () 
{ 
    local i IFS='	
' xspec;
    __expand_tilde_by_ref cur;
    local -a toks;
    local quoted tmp;
    _quote_readline_by_ref "$cur" quoted;
    toks=(${toks[@]-} $(
        compgen -d -- "$quoted" | {
            while read -r tmp; do
                # TODO: I have removed a "[ -n $tmp ] &&" before 'printf ..',
                #       and everything works again. If this bug suddenly
                #       appears again (i.e. "cd /b<TAB>" becomes "cd /"),
                #       remember to check for other similar conditionals (here
                #       and _filedir_xspec()). --David
                printf '%s\n' $tmp
            done
        }
    ));
    if [[ "$1" != -d ]]; then
        xspec=${1:+"!*.$1"};
        if [[ ${cur:0:1} == "'" && ${BASH_VERSINFO[0]} -ge 4 ]]; then
            toks=(${toks[@]-} $(
                eval compgen -f -X \"\$xspec\" -- $quoted
            ));
        else
            toks=(${toks[@]-} $(
                compgen -f -X "$xspec" -- $quoted
            ));
        fi;
        if [ ${#toks[@]} -ne 0 ]; then
            compopt &>/dev/null && compopt -o filenames || [[ ( -n ${COMP_WORDS[0]} && "$(complete -p ${COMP_WORDS[0]})" == *"-o filenames"* ) ]] || { 
                for ((i=0; i < ${#toks[@]}; i++))
                do
                    if [[ ${cur:0:1} != "'" ]]; then
                        [[ -d ${toks[i]} ]] && toks[i]="${toks[i]}"/;
                        if [[ ${cur:0:1} == '"' ]]; then
                            toks[i]=${toks[i]//\\/\\\\};
                            toks[i]=${toks[i]//\"/\\\"};
                            toks[i]=${toks[i]//\$/\\\$};
                        else
                            toks[i]=$(printf %q ${toks[i]});
                        fi;
                    fi;
                done
            };
        fi;
    fi;
    COMPREPLY=("${COMPREPLY[@]}" "${toks[@]}")
}
_filedir_xspec () 
{ 
    local IFS cur xspec;
    IFS='	
';
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _expand || return 0;
    xspec=$( awk "/^complete[ \t]+.*[ \t]${1##*/}([ \t]|\$)/ { print \$0; exit }"         "$BASH_COMPLETION" );
    xspec=${xspec#*-X };
    xspec=${xspec%% *};
    local -a toks;
    local tmp;
    toks=(${toks[@]-} $(
        compgen -d -- "$(quote_readline "$cur")" | {
        while read -r tmp; do
            # see long TODO comment in _filedir() --David
            printf '%s\n' $tmp
        done
        }
        ));
    toks=(${toks[@]-} $(
        eval compgen -f -X "$xspec" -- "\$(quote_readline "\$cur")" | {
        while read -r tmp; do
            [ -n $tmp ] && printf '%s\n' $tmp
        done
        }
        ));
    COMPREPLY=("${toks[@]}")
}
_find () 
{ 
    local cur prev i exprfound onlyonce;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -maxdepth | -mindepth)
            COMPREPLY=($( compgen -W '0 1 2 3 4 5 6 7 8 9' -- "$cur" ));
            return 0
        ;;
        -newer | -anewer | -cnewer | -fls | -fprint | -fprint0 | -fprintf | -name | -iname | -lname | -ilname | -wholename | -iwholename | -samefile)
            _filedir;
            return 0
        ;;
        -fstype)
            _fstypes;
            if [[ "$( uname -s )" == *BSD ]]; then
                COMPREPLY=("${COMPREPLY[@]}" $( compgen -W 'local rdonly' -- "$cur" ));
            fi;
            return 0
        ;;
        -gid)
            _gids;
            return 0
        ;;
        -group)
            COMPREPLY=($( compgen -g -- "$cur" 2>/dev/null));
            return 0
        ;;
        -xtype | -type)
            COMPREPLY=($( compgen -W 'b c d p f l s' -- "$cur" ));
            return 0
        ;;
        -uid)
            _uids;
            return 0
        ;;
        -user)
            COMPREPLY=($( compgen -u -- "$cur" ));
            return 0
        ;;
        -exec | -execdir | -ok | -okdir)
            COMP_WORDS=(COMP_WORDS[0] "$cur");
            COMP_CWORD=1;
            _command;
            return 0
        ;;
        -[acm]min | -[acm]time | -iname | -lname | -wholename | -iwholename | -lwholename | -ilwholename | -inum | -path | -ipath | -regex | -iregex | -links | -perm | -size | -used | -printf | -context)
            return 0
        ;;
        -regextype)
            COMPREPLY=($( compgen -W 'emacs posix-awk posix-basic \
                posix-egrep posix-extended' -- "$cur" ));
            return 0
        ;;
    esac;
    _expand || return 0;
    for i in ${COMP_WORDS[@]};
    do
        [[ "$i" = [-\(\),\!]* ]] && exprfound=1 && break;
    done;
    if [[ "$exprfound" != 1 && "$cur" != [-\(\),\!]* ]]; then
        _filedir -d;
        return 0;
    fi;
    COMPREPLY=($( compgen -W '-daystart -depth -follow -help \
        -ignore_readdir_race -maxdepth -mindepth -mindepth -mount \
        -noignore_readdir_race -noleaf -regextype -version -warn -nowarn \
        -xdev \
        -amin -anewer -atime -cmin -cnewer -ctime -empty -executable -false \
        -fstype -gid -group -ilname -iname -inum -ipath -iregex -iwholename \
        -links -lname -mmin -mtime -name -newer -nogroup -nouser -path -perm \
        -readable -regex -samefile -size -true -type -uid -used -user \
        -wholename -writable -xtype -context \
        -delete -exec -execdir -fls -fprint -fprint0 -fprintf -ls -ok -okdir \
        -print -print0 -printf -prune -quit' -- "$cur" ));
    onlyonce=' -daystart -depth -follow -help -ignore_readdir_race -maxdepth \
       -mindepth -mount -noignore_readdir_race -noleaf -nowarn -regextype \
       -version -warn -xdev ';
    COMPREPLY=($(            (while read -d ' ' i; do
            [[ -z "$i" || "${onlyonce/ ${i%% *} / }" == "$onlyonce" ]] &&
            continue
            # flatten array with spaces on either side,
            # otherwise we cannot grep on word boundaries of
            # first and last word
            COMPREPLY=" ${COMPREPLY[@]} "
            # remove word from list of completions
            COMPREPLY=( ${COMPREPLY/ ${i%% *} / } )
            done
            printf '%s ' "${COMPREPLY[@]}") <<<"${COMP_WORDS[@]}"
          ));
    _filedir;
    return 0
}
_fstypes () 
{ 
    local fss;
    if [ -e /proc/filesystems ]; then
        fss="$( cut -d'	' -f2 /proc/filesystems )
             $( awk '! /\*/ { print $NF }' /etc/filesystems 2>/dev/null )";
    else
        fss="$( awk '/^[ \t]*[^#]/ { print $3 }' /etc/fstab 2>/dev/null )
             $( awk '/^[ \t]*[^#]/ { print $3 }' /etc/mnttab 2>/dev/null )
             $( awk '/^[ \t]*[^#]/ { print $4 }' /etc/vfstab 2>/dev/null )
             $( awk '{ print $1 }' /etc/dfs/fstypes 2>/dev/null )
             $( [ -d /etc/fs ] && command ls /etc/fs )";
    fi;
    [ -n "$fss" ] && COMPREPLY=("${COMPREPLY[@]}" $( compgen -W "$fss" -- "$cur" ))
}
_function () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [[ $1 == @(declare|typeset) ]]; then
        if [ "$prev" = -f ]; then
            COMPREPLY=($( compgen -A function -- "$cur" ));
        else
            if [[ "$cur" == -* ]]; then
                COMPREPLY=($( compgen -W '-a -f -F -i -r -x -p' -- "$cur" ));
            fi;
        fi;
    else
        if [ $COMP_CWORD -eq 1 ]; then
            COMPREPLY=($( compgen -A function -- "$cur" ));
        else
            COMPREPLY=("() $( type -- ${COMP_WORDS[1]} | sed -e 1,2d )");
        fi;
    fi
}
_fusermount () 
{ 
    COMPREPLY=();
    local cur prev;
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -h | -V | -o)
            return 0
        ;;
        -u)
            COMPREPLY=($( compgen -W "$( awk                 '{ if ($3 ~ /^fuse\./) print $2 }' /etc/mtab 2>/dev/null )"                 -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h -V -o -u -q -z' -- "$cur" ));
    else
        _filedir -d;
    fi
}
_gcc () 
{ 
    local cur cc backend;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _expand || return 0;
    case $1 in 
        gcj)
            backend=jc1
        ;;
        gpc)
            backend=gpc1
        ;;
        *77)
            backend=f771
        ;;
        *)
            backend=cc1
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        cc=$( $1 -print-prog-name=$backend );
        COMPREPLY=($( compgen -W "$( $cc --help 2>/dev/null |            tr '\t' ' ' |            sed -e '/^  *-/!d' -e 's/ *-\([^ ]*\).*/-\1/' |            sort -u )" -- "$cur" ));
    else
        _filedir;
    fi
}
_get_command () 
{ 
    local i;
    command=;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" != -* ]]; then
            command=${COMP_WORDS[i]};
            break;
        fi;
    done
}
_get_comp_words_by_ref () 
{ 
    local exclude flag i OPTIND=1;
    local cur cword words=();
    local upargs=() upvars=() vcur vcword vprev vwords;
    while getopts "c:i:n:p:w:" flag "$@"; do
        case $flag in 
            c)
                vcur=$OPTARG
            ;;
            i)
                vcword=$OPTARG
            ;;
            n)
                exclude=$OPTARG
            ;;
            p)
                vprev=$OPTARG
            ;;
            w)
                vwords=$OPTARG
            ;;
        esac;
    done;
    while [[ $# -ge $OPTIND ]]; do
        case ${!OPTIND} in 
            cur)
                vcur=cur
            ;;
            prev)
                vprev=prev
            ;;
            cword)
                vcword=cword
            ;;
            words)
                vwords=words
            ;;
            *)
                echo "bash: $FUNCNAME(): \`${!OPTIND}': unknown argument" 1>&2;
                return 1
            ;;
        esac;
        let "OPTIND += 1";
    done;
    __get_cword_at_cursor_by_ref "$exclude" words cword cur;
    [[ -n $vcur ]] && { 
        upvars+=("$vcur");
        upargs+=(-v $vcur "$cur")
    };
    [[ -n $vcword ]] && { 
        upvars+=("$vcword");
        upargs+=(-v $vcword "$cword")
    };
    [[ -n $vprev ]] && { 
        upvars+=("$vprev");
        upargs+=(-v $vprev "${words[cword - 1]}")
    };
    [[ -n $vwords ]] && { 
        upvars+=("$vwords");
        upargs+=(-a${#words[@]} $vwords "${words[@]}")
    };
    (( ${#upvars[@]} )) && local "${upvars[@]}" && _upvars "${upargs[@]}"
}
_get_cword () 
{ 
    local LC_CTYPE=C;
    local cword words;
    __reassemble_comp_words_by_ref "$1" words cword;
    if [[ -n ${2//[^0-9]/} ]]; then
        printf "%s" "${words[cword-$2]}";
    else
        if [[ "${#words[cword]}" -eq 0 ]] || [[ "$COMP_POINT" == "${#COMP_LINE}" ]]; then
            printf "%s" "${words[cword]}";
        else
            local i;
            local cur="$COMP_LINE";
            local index="$COMP_POINT";
            for ((i = 0; i <= cword; ++i ))
            do
                while [[ "${#cur}" -ge ${#words[i]} && "${cur:0:${#words[i]}}" != "${words[i]}" ]]; do
                    cur="${cur:1}";
                    ((index--));
                done;
                if [[ "$i" -lt "$cword" ]]; then
                    local old_size="${#cur}";
                    cur="${cur#${words[i]}}";
                    local new_size="${#cur}";
                    index=$(( index - old_size + new_size ));
                fi;
            done;
            if [[ "${words[cword]:0:${#cur}}" != "$cur" ]]; then
                printf "%s" "${words[cword]}";
            else
                printf "%s" "${cur:0:$index}";
            fi;
        fi;
    fi
}
_get_first_arg () 
{ 
    local i;
    arg=;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" != -* ]]; then
            arg=${COMP_WORDS[i]};
            break;
        fi;
    done
}
_get_pword () 
{ 
    if [ $COMP_CWORD -ge 1 ]; then
        _get_cword "${@:-}" 1;
    fi
}
_getent () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        passwd)
            COMPREPLY=($( compgen -u "$cur"  ));
            return 0
        ;;
        group)
            COMPREPLY=($( compgen -g "$cur"  ));
            return 0
        ;;
        services)
            COMPREPLY=($( compgen -s "$cur"  ));
            return 0
        ;;
        hosts)
            COMPREPLY=($( compgen -A hostname "$cur"  ));
            return 0
        ;;
        protocols | networks | ahosts | ahostsv4 | ahostsv6 | rpc)
            COMPREPLY=($( compgen -W "$( getent "$prev" |                 awk '{ print $1 }' )" -- "$cur" ));
            return 0
        ;;
        aliases | shadow)
            COMPREPLY=($( compgen -W "$( getent "$prev" | cut -d: -f1 )"                 -- "$cur" ));
            return 0
        ;;
    esac;
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($( compgen -W 'passwd group hosts services protocols \
            networks ahosts ahostsv4 ahostsv6 aliases ethers netgroup rpc \
            shadow' -- "$cur" ));
    fi
}
_gids () 
{ 
    if type getent &>/dev/null; then
        COMPREPLY=($( compgen -W '$( getent group | cut -d: -f3 )'             -- "$cur" ));
    else
        if type perl &>/dev/null; then
            COMPREPLY=($( compgen -W '$( perl -e '"'"'while (($gid) = (getgrent)[2]) { print $gid . "\n" }'"'"' )' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W '$( cut -d: -f3 /etc/group )' -- "$cur" ));
        fi;
    fi
}
_gpasswd () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -a | -d | -A | -M)
            COMPREPLY=($( compgen -u -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-a -d -r -R -A -M' -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -g -- "$cur" ))
}
_gpg () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -s | --sign | --clearsign | --decrypt-files | --load-extension)
            _filedir;
            return 0
        ;;
        --export | --sign-key | --lsignkey | --nrsignkey | --nrlsignkey | --editkey)
            COMPREPLY=($( compgen -W "$( gpg --list-keys 2>/dev/null |                 sed -ne 's@^pub.*/\([^ ]*\).*$@\1@p'                     -ne 's@^.*\(<\([^>]*\)>\).*$@\2@p' )" -- "$cur" ));
            return 0
        ;;
        -r | --recipient)
            COMPREPLY=($( compgen -W "$( gpg --list-keys 2>/dev/null |                 sed -ne 's@^.*<\([^>]*\)>.*$@\1@p')" -- "$cur" ));
            if [ -e ~/.gnupg/gpg.conf ]; then
                COMPREPLY=("${COMPREPLY[@]}" $( compgen -W "$( sed -ne                     's@^[ \t]*group[ \t][ \t]*\([^=]*\).*$@\1@p'                     ~/.gnupg/gpg.conf  )" -- "$cur"));
            fi;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '$(gpg --dump-options)' -- "$cur" ));
    fi
}
_groupadd () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -g | --gid | -K | --key | -p | --password)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--force --gid --help \
            --key --non-unique --password --system' -- "$cur" ));
        return 0;
    fi
}
_groupmod () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -g | --gid | -h | --help | -n | --new-name | -p | --password)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--gid --help --new-name \
            --non-unique --password' -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -g -- "$cur" ))
}
_grpck () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-r -s' -- "$cur" ));
        return 0;
    fi;
    _filedir
}
_gzip () 
{ 
    local cur prev xspec helpopts;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    helpopts=`_parse_help ${COMP_WORDS[0]}`;
    case $prev in 
        -b | --blocksize | -p | --processes | -S | --suffix | -h | --help | -V | --version)
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W "$helpopts -1 -2 -3 -4 -5 -6 -7 -8 -9"             -- "$cur" ));
        return 0;
    fi;
    local IFS='	
';
    xspec="*.@(gz|t[ag]z)";
    if [[ "$prev" == --* ]]; then
        [[ "$prev" == --decompress || "$prev" == --list || "$prev" == --test ]] && xspec="!"$xspec;
        [[ "$prev" == --force ]] && xspec=;
    else
        if [[ "$prev" == -* ]]; then
            [[ "$prev" == -*[dlt]* ]] && xspec="!"$xspec;
            [[ "$prev" == -*f* ]] && xspec=;
        else
            if [[ "$prev" == '>' || "$prev" == '>>' ]]; then
                xspec=;
            else
                if [ "$prev" = '<' ]; then
                    xspec=;
                fi;
            fi;
        fi;
    fi;
    _expand || return 0;
    COMPREPLY=($( compgen -f -X "$xspec" -- "$cur" ) $( compgen -d -- "$cur" ))
}
_hciattach () 
{ 
    local cur;
    COMPREPLY=();
    cur=`_get_cword`;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-n -p -t -b -s -l' -- "$cur" ));
    else
        _count_args;
        case $args in 
            1)
                COMPREPLY=($( command ls /dev/tty* ));
                COMPREPLY=($( compgen -W '${COMPREPLY[@]} \
                    ${COMPREPLY[@]#/dev/}' -- "$cur" ))
            ;;
            2)
                COMPREPLY=($( compgen -W 'any ericsson digi \
                    xircom csr bboxes swave bcsp 0x0105 \
                    0x080a 0x0160 0x0002' -- "$cur" ))
            ;;
            3)
                COMPREPLY=($( compgen -W '9600 19200 38400 \
                    57600 115200 230400 460800 921600' -- "$cur" ))
            ;;
            4)
                COMPREPLY=($( compgen -W 'flow noflow' -- "$cur" ))
            ;;
            5)
                _bluetooth_adresses
            ;;
        esac;
    fi
}
_hciconfig () 
{ 
    local cur;
    COMPREPLY=();
    cur=`_get_cword`;
    _get_command;
    if [ -z $command ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-h --help -a --all' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'up down reset rstat auth \
                noauth encrypt noencrypt secmgr nosecmgr \
                piscan noscan iscan pscan ptype name class \
                voice iac inqmode inqdata inqtype inqparams \
                pageparms pageto afhmode aclmtu scomtu putkey \
                delkey commands features version revision lm'                 -- "$cur" ));
        fi;
    else
        case $command in 
            @(putkey|delkey))
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                fi
            ;;
            lm)
                _count_args;
                if [ $args -eq 2 ]; then
                    COMPREPLY=($( compgen -W 'MASTER \
                        SLAVE NONE ACCEPT' -- "$cur" ));
                fi
            ;;
            ptype)
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_packet_types;
                fi
            ;;
        esac;
    fi
}
_hcitool () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    _split_longopt && split=true;
    case $prev in 
        -i)
            _bluetooth_devices;
            return 0
        ;;
        --role)
            COMPREPLY=($( compgen -W 'm s' -- "$cur" ));
            return 0
        ;;
        --pkt-type)
            _bluetooth_packet_types;
            return 0
        ;;
    esac;
    $split && return 0;
    _get_command;
    if [ -z $command ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-h -i' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'dev inq scan name info \
                spinq epinq cmd con cc dc sr cpt rssi lq tpl \
                afh lst auth enc key clkoff clock' -- "$cur" ));
        fi;
    else
        case $command in 
            @(name|info|dc|rssi|lq|afh|auth|key|clkoff|lst))
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                fi
            ;;
            cc)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '--role --pkt-type' -- "$cur" ));
                else
                    _count_args;
                    if [ $args -eq 2 ]; then
                        _bluetooth_adresses;
                    fi;
                fi
            ;;
            sr)
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                else
                    COMPREPLY=($( compgen -W 'master slave' -- "$cur" ));
                fi
            ;;
            cpt)
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                else
                    _bluetooth_packet_types;
                fi
            ;;
            @(tpl|enc|clock))
                _count_args;
                if [ $args -eq 2 ]; then
                    _bluetooth_adresses;
                else
                    COMPREPLY=($( compgen -W '0 1' -- "$cur" ));
                fi
            ;;
        esac;
    fi
}
_hid2hci () 
{ 
    local cur;
    COMPREPLY=();
    cur=`_get_cword`;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h --help -q --quiet -0 --tohci -1 \
            --tohid' -- "$cur" ));
    fi
}
_iconv () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -'?' | --help | --usage | -V | --version | --unicode-subst | --byte-subst | --widechar-subst)
            return 0
        ;;
        -f | --from-code | -t | --to-code)
            COMPREPLY=($( compgen -W '$( iconv -l | \
                sed -e "s@/*\$@@" -e "s/[,()]//g" )' -- "$cur" ));
            return 0
        ;;
        -o | --output)
            _filedir;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" = -* ]]; then
        COMPREPLY=($( compgen -W '--from-code --to-code --list -c
            --unicode-subst --byte-subst --widechar-subst --output --silent
            --verbose --help --usage --version' -- "$cur" ));
        return 0;
    fi
}
_id () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-a -g --group -G --groups -n --name\
            -r --real -u --user --help --version' -- "$cur" ));
    else
        COMPREPLY=($( compgen -u "$cur" ));
    fi
}
_identify () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-alpha -antialias -authenticate \
            -channel -colorspace -crop -debug -define -density \
            -depth -extract -format -fuzz -gamma -help -interlace \
            -interpolate -limit -list -log -monitor -ping -quiet \
            -regard-warnings -respect-parenthesis \
            -sampling-factor -seed -set -size -strip -units \
            -verbose -version -virtual-pixel' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_ifupdown () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [ $COMP_CWORD -eq 1 ]; then
        _configured_interfaces;
        COMPREPLY=($(compgen -W '${COMPREPLY[@]}' -- "$cur"));
    fi;
    return 0
}
_import () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-adjoin -annotate -border -channel \
            -colors -colorspace -comment -compress -crop -debug \
            -define -delay -density -depth -descend -display \
            -dispose -dither -encipher -encoding -endian -filter \
            -format -frame -geometry -gravity -help -identify \
            -interlace -interpolate -label -limit -list -log \
            -monitor -monochrome -negate -page -pause -pointsize \
            -quality -quantize -quiet -regard-warnings -repage \
            -resize -respect-parenthesis -rotate -sampling-factor \
            -scene -screen -seed -set -silent -snaps -strip \
            -thumbnail -transparent -transparent-color -treedepth \
            -trim -type -verbose -version -virtual-pixel \
            -window' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_info () 
{ 
    local cur i infopath;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _expand || return 0;
    if [[ "$cur" == */* ]]; then
        _filedir;
        return 0;
    fi;
    infopath='/usr/share/info';
    if [ "${INFOPATH: -1:1}" == ':' ]; then
        infopath=${INFOPATH}${infopath};
    else
        if [ ${INFOPATH:+set} ]; then
            infopath=$INFOPATH;
        fi;
    fi;
    infopath=$infopath:;
    if [ -n "$cur" ]; then
        infopath="${infopath//://$cur* }";
    else
        infopath="${infopath//:// }";
    fi;
    COMPREPLY=($( eval command ls "$infopath" 2>/dev/null ));
    COMPREPLY=(${COMPREPLY[@]##*/?(:)});
    for ((i=0 ; i < ${#COMPREPLY[@]} ; ++i ))
    do
        if [ "${COMPREPLY[$i]}" == 'dir' ]; then
            unset COMPREPLY[$i];
        fi;
    done;
    COMPREPLY=(${COMPREPLY[@]%.@(gz|bz2|xz|lzma)});
    COMPREPLY=($( compgen -W '${COMPREPLY[@]%.*}' -- "${cur//\\\\/}" ));
    return 0
}
_inkscape () 
{ 
    local cur;
    COMPREPLY=();
    cur=${COMP_WORDS[COMP_CWORD]};
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-? --help --usage -V --version \
			-z --without-gui -g --with-gui -f --file= -p --print= \
			-e --export-png= -d --export-dpi= -a --export-area= \
			-w --export-width= -h --export-height= -i --export-id= \
			-j --export-id-only  -t --export-use-hints -b --export-background= \
			-y --export-background-opacity= -l --export-plain-svg= -s --slideshow' -- $cur ));
    else
        _filedir '@(ai|ani|bmp|cur|dia|eps|gif|ggr|ico|jpe|jpeg|jpg|pbm|pcx|pdf|pgm|png|ppm|pnm|ps|ras|sk|svg|svgz|targa|tga|tif|tiff|txt|wbmp|wmf|xbm|xpm)';
    fi
}
_insmod () 
{ 
    local cur prev modpath;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [[ ${1##*/} == modprobe && "${COMP_WORDS[1]}" == -r ]]; then
        _installed_modules "$cur";
        return 0;
    fi;
    if [[ "$cur" == */* ]]; then
        _filedir '@(?(k)o?(.gz))';
        return 0;
    fi;
    if [[ $COMP_CWORD -gt 1 && "${COMP_WORDS[COMP_CWORD-1]}" != -* ]]; then
        COMPREPLY=($( compgen -W "$( /sbin/modinfo -p ${COMP_WORDS[1]} |             cut -d: -f1 )" -- "$cur" ));
    else
        _modules $(uname -r);
    fi;
    return 0
}
_insserv () 
{ 
    local cur prev sysvdir services options;
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    [ -d /etc/rc.d/init.d ] && sysvdir=/etc/rc.d/init.d || sysvdir=/etc/init.d;
    services=($(echo $sysvdir/!(README*|*.dpkg*|*.rpm@(orig|new|save))));
    services=(${services[@]#$sysvdir/});
    options=(-c --config -d -f -n -o --override -p --path -r -v);
    case "$prev" in 
        -c | --config)
            _filedir
        ;;
        -o | --override | -p | --path)
            _filedir -d
        ;;
        *)
            COMPREPLY=($( compgen -W '${options[@]} ${services[@]}' -- 		$cur ))
        ;;
    esac;
    return 0
}
_installed_alternatives () 
{ 
    local admindir;
    for i in alternatives dpkg/alternatives rpm/alternatives;
    do
        [ -d /var/lib/$i ] && admindir=/var/lib/$i && break;
    done;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" == --admindir ]]; then
            admindir=${COMP_WORDS[i+1]};
            break;
        fi;
    done;
    COMPREPLY=($( compgen -W '$( command ls $admindir )' -- "$cur" ))
}
_installed_modules () 
{ 
    COMPREPLY=($( compgen -W "$( PATH="$PATH:/sbin" lsmod |         awk '{if (NR != 1) print $1}' )" -- "$1" ))
}
_invoke_rc_d () 
{ 
    local cur prev sysvdir services options valid_options;
    _get_comp_words_by_ref cur prev;
    [ -d /etc/rc.d/init.d ] && sysvdir=/etc/rc.d/init.d || sysvdir=/etc/init.d;
    services=($( printf '%s '         $sysvdir/!(README*|*.sh|*.dpkg*|*.rpm@(orig|new|save)) ));
    services=(${services[@]#$sysvdir/});
    options=(--help --quiet --force --try-anyway --disclose-deny --query --no-fallback);
    if [[ ( $COMP_CWORD -eq 1 ) || ( "$prev" == --* ) ]]; then
        valid_options=($(         tr " " "\n" <<<"${COMP_WORDS[@]} ${options[@]}"         | sed -ne "/$( sed "s/ /\\\\|/g" <<<"${options[@]}" )/p"         | sort | uniq -u         ));
        COMPREPLY=($( compgen -W '${valid_options[@]} ${services[@]}' -- "$cur" ));
    else
        if [ -x $sysvdir/$prev ]; then
            COMPREPLY=($( compgen -W '`sed -e "y/|/ /" \
            -ne "s/^.*Usage:[ ]*[^ ]*[ ]*{*\([^}\"]*\).*$/\1/p" \
            $sysvdir/$prev`' -- "$cur" ));
        else
            COMPREPLY=();
        fi;
    fi;
    return 0
}
_iptables () 
{ 
    local cur prev table chain;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    chain='s/^Chain \([^ ]\{1,\}\).*$/\1/p';
    if [[ $COMP_LINE == *-t\ *filter* ]]; then
        table="-t filter";
    else
        if [[ $COMP_LINE == *-t\ *nat* ]]; then
            table="-t nat";
        else
            if [[ $COMP_LINE == *-t\ *mangle* ]]; then
                table="-t mangle";
            fi;
        fi;
    fi;
    _split_longopt;
    case $prev in 
        -*[AIDRPFXLZ])
            COMPREPLY=($( compgen -W '`iptables $table -nL | \
                sed -ne "s/^Chain \([^ ]\{1,\}\).*$/\1/p"`' -- "$cur" ))
        ;;
        -*t)
            COMPREPLY=($( compgen -W 'nat filter mangle' -- "$cur" ))
        ;;
        -j)
            if [[ "$table" == "-t filter" || -z "$table" ]]; then
                COMPREPLY=($( compgen -W 'ACCEPT DROP LOG ULOG REJECT \
                `iptables $table -nL | sed -ne "$chain" \
                -e "s/INPUT|OUTPUT|FORWARD|PREROUTING|POSTROUTING//"`' --                 "$cur" ));
            else
                if [ "$table" = "-t nat" ]; then
                    COMPREPLY=($( compgen -W 'ACCEPT DROP LOG ULOG REJECT \
                MIRROR SNAT DNAT MASQUERADE `iptables $table -nL | \
                sed -ne "$chain" -e "s/OUTPUT|PREROUTING|POSTROUTING//"`'                 -- "$cur" ));
                else
                    if [ "$table" = "-t mangle" ]; then
                        COMPREPLY=($( compgen -W 'ACCEPT DROP LOG ULOG REJECT \
                MARK TOS `iptables $table -nL | sed -ne "$chain" \
                -e "s/INPUT|OUTPUT|FORWARD|PREROUTING|POSTROUTING//"`' --                 "$cur" ));
                    fi;
                fi;
            fi
        ;;
        *)
            if [[ "$cur" == -* ]]; then
                COMPREPLY=($( compgen -W '--in-interface --out-interface --source \
                --destination --protocol --fragment --match --append \
                --delete --insert --replace --list --flush --zero --new \
                --delete-chain --policy --rename-chain --proto --source \
                --destination --in-interface --jump --match --numeric \
                --out-interface --table --verbose --line-numbers --exact \
                --fragment --modprobe --set-counters --version' -- "$cur"));
            fi
        ;;
    esac
}
_isql () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    [ -f "$ODBCINI" ] && COMPREPLY=($( command grep \\["$cur" "$ODBCINI" | tr -d \\[\\] ))
}
_java () 
{ 
    local cur prev i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    for ((i=1; i < $COMP_CWORD; i++))
    do
        case ${COMP_WORDS[$i]} in 
            -cp | -classpath)
                ((i++))
            ;;
            -*)

            ;;
            *)
                _filedir;
                return 0
            ;;
        esac;
    done;
    case $prev in 
        -cp | -classpath)
            _java_path;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-client -hotspot -server -classic \
            -classpath -D -verbose -verbose:class \
            -verbose:gc -version:jni -version \
            -showversion -help -X -jar \
            -enableassertions -disableassertions \
            -enablesystemassertions -disablesystemassertions ' -- "$cur" ));
    else
        if [[ "$prev" == -jar ]]; then
            _filedir jar;
        else
            _java_classes;
        fi;
    fi
}
_java_classes () 
{ 
    local classpath i;
    _java_find_classpath;
    cur=${cur//.//};
    for i in ${classpath//:/ };
    do
        if [[ -r $i && "$i" == *.@(jar|zip) ]]; then
            if type zipinfo &>/dev/null; then
                COMPREPLY=("${COMPREPLY[@]}" $( zipinfo -1                     "$i" "$cur*" 2>/dev/null |                     command grep '^[^$]*\.class$' ));
            else
                COMPREPLY=("${COMPREPLY[@]}" $( jar tf "$i"                     "$cur" | command grep '^[^$]*\.class$' ));
            fi;
        else
            if [ -d $i ]; then
                COMPREPLY=("${COMPREPLY[@]}" $( command ls $i/$cur*.class 2>/dev/null |                     sed -ne '/\$/d' -e "s|^$i//*||p" ));
            fi;
        fi;
    done;
    COMPREPLY=(${COMPREPLY[@]%.class});
    COMPREPLY=(${COMPREPLY[@]//\//.})
}
_java_find_classpath () 
{ 
    local i;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" == -@(cp|classpath) ]]; then
            classpath=${COMP_WORDS[i+1]};
            break;
        fi;
    done;
    [ -z "$classpath" ] && classpath=$CLASSPATH;
    [ -z "$classpath" ] && classpath=.
}
_java_find_sourcepath () 
{ 
    local i;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" == -sourcepath ]]; then
            sourcepath=${COMP_WORDS[i+1]};
            break;
        fi;
    done;
    if [ -z "$sourcepath" ]; then
        _java_find_classpath;
        sourcepath=$classpath;
    fi
}
_java_packages () 
{ 
    local sourcepath i;
    _java_find_sourcepath;
    cur=${cur//.//};
    for i in ${sourcepath//:/ };
    do
        if [ -d $i ]; then
            COMPREPLY=("${COMPREPLY[@]}" $( command ls -F -d                 $i/$cur* 2>/dev/null | sed -e 's|^'$i'/||' ));
        fi;
    done;
    COMPREPLY=($( tr " " "\n" <<<"${COMPREPLY[@]}" | command grep "/$" ));
    COMPREPLY=(${COMPREPLY[@]%/});
    cur=${COMPREPLY[@]//\//.}
}
_java_path () 
{ 
    cur=${cur##*:};
    _filedir '@(jar|zip)'
}
_kernel_versions () 
{ 
    COMPREPLY=($( compgen -W '$( command ls /lib/modules )' -- "$cur" ))
}
_kill () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ $COMP_CWORD -eq 1 && "$cur" == -* ]]; then
        _signals;
    else
        _pids;
    fi
}
_killall () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ $COMP_CWORD -eq 1 && "$cur" == -* ]]; then
        _signals;
    else
        _pnames;
    fi;
    return 0
}
_known_hosts () 
{ 
    local options;
    COMPREPLY=();
    [[ "$1" == -a || "$2" == -a ]] && options=-a;
    [[ "$1" == -c || "$2" == -c ]] && options="$options -c";
    _known_hosts_real $options "$(_get_cword :)"
}
_known_hosts_real () 
{ 
    local configfile flag prefix;
    local cur curd awkcur user suffix aliases i host;
    local -a kh khd config;
    local OPTIND=1;
    while getopts "acF:p:" flag "$@"; do
        case $flag in 
            a)
                aliases='yes'
            ;;
            c)
                suffix=':'
            ;;
            F)
                configfile=$OPTARG
            ;;
            p)
                prefix=$OPTARG
            ;;
        esac;
    done;
    [ $# -lt $OPTIND ] && echo "error: $FUNCNAME: missing mandatory argument CWORD";
    cur=${!OPTIND};
    let "OPTIND += 1";
    [ $# -ge $OPTIND ] && echo "error: $FUNCNAME("$@"): unprocessed arguments:" $(while [ $# -ge $OPTIND ]; do printf '%s\n' ${!OPTIND}; shift; done);
    [[ $cur == *@* ]] && user=${cur%@*}@ && cur=${cur#*@};
    kh=();
    if [ -n "$configfile" ]; then
        [ -r "$configfile" ] && config=("${config[@]}" "$configfile");
    else
        [ -r /etc/ssh/ssh_config ] && config=("${config[@]}" "/etc/ssh/ssh_config");
        [ -r "${HOME}/.ssh/config" ] && config=("${config[@]}" "${HOME}/.ssh/config");
        [ -r "${HOME}/.ssh2/config" ] && config=("${config[@]}" "${HOME}/.ssh2/config");
    fi;
    if [ ${#config[@]} -gt 0 ]; then
        local OIFS=$IFS IFS='
';
        local -a tmpkh;
        tmpkh=($( awk 'sub("^[ \t]*([Gg][Ll][Oo][Bb][Aa][Ll]|[Uu][Ss][Ee][Rr])[Kk][Nn][Oo][Ww][Nn][Hh][Oo][Ss][Tt][Ss][Ff][Ii][Ll][Ee][ \t]+", "") { print $0 }' "${config[@]}" | sort -u ));
        for i in "${tmpkh[@]}";
        do
            i=${i//\"};
            __expand_tilde_by_ref i;
            [ -r "$i" ] && kh=("${kh[@]}" "$i");
        done;
        IFS=$OIFS;
    fi;
    if [ -z "$configfile" ]; then
        [ -r /etc/ssh/ssh_known_hosts ] && kh=("${kh[@]}" /etc/ssh/ssh_known_hosts);
        [ -r /etc/ssh/ssh_known_hosts2 ] && kh=("${kh[@]}" /etc/ssh/ssh_known_hosts2);
        [ -r /etc/known_hosts ] && kh=("${kh[@]}" /etc/known_hosts);
        [ -r /etc/known_hosts2 ] && kh=("${kh[@]}" /etc/known_hosts2);
        [ -d /etc/ssh2/knownhosts ] && khd=("${khd[@]}" /etc/ssh2/knownhosts/*pub);
    fi;
    if [ -z "$configfile" ]; then
        [ -r ~/.ssh/known_hosts ] && kh=("${kh[@]}" ~/.ssh/known_hosts);
        [ -r ~/.ssh/known_hosts2 ] && kh=("${kh[@]}" ~/.ssh/known_hosts2);
        [ -d ~/.ssh2/hostkeys ] && khd=("${khd[@]}" ~/.ssh2/hostkeys/*pub);
    fi;
    if [[ ${#kh[@]} -gt 0 || ${#khd[@]} -gt 0 ]]; then
        awkcur=${cur//\//\\\/};
        awkcur=${awkcur//\./\\\.};
        curd=$awkcur;
        if [[ "$awkcur" == [0-9]*.* ]]; then
            awkcur="^$awkcur.*";
        else
            if [[ "$awkcur" == [0-9]* ]]; then
                awkcur="^$awkcur.*\.";
            else
                if [ -z "$awkcur" ]; then
                    awkcur="[a-z.]";
                else
                    awkcur="^$awkcur";
                fi;
            fi;
        fi;
        if [ ${#kh[@]} -gt 0 ]; then
            COMPREPLY=("${COMPREPLY[@]}" $( awk 'BEGIN {FS=","}
            /^\s*[^|\#]/ {for (i=1; i<=2; ++i) { \
            gsub(" .*$", "", $i); \
            gsub("[\\[\\]]", "", $i); \
            gsub(":[0-9]+$", "", $i); \
            if ($i ~ /'"$awkcur"'/) {print $i} \
            }}' "${kh[@]}" 2>/dev/null ));
        fi;
        if [ ${#khd[@]} -gt 0 ]; then
            for i in "${khd[@]}";
            do
                if [[ "$i" == *key_22_$curd*.pub && -r "$i" ]]; then
                    host=${i/#*key_22_/};
                    host=${host/%.pub/};
                    COMPREPLY=("${COMPREPLY[@]}" $host);
                fi;
            done;
        fi;
        for ((i=0; i < ${#COMPREPLY[@]}; i++ ))
        do
            COMPREPLY[i]=$prefix$user${COMPREPLY[i]}$suffix;
        done;
    fi;
    if [[ ${#config[@]} -gt 0 && -n "$aliases" ]]; then
        local hosts=$( sed -ne 's/^[ \t]*[Hh][Oo][Ss][Tt]\([Nn][Aa][Mm][Ee]\)\{0,1\}['"$'\t '"']\{1,\}\([^#*?]*\)\(#.*\)\{0,1\}$/\2/p' "${config[@]}" );
        COMPREPLY=("${COMPREPLY[@]}" $( compgen  -P "$prefix$user"             -S "$suffix" -W "$hosts" -- "$cur" ));
    fi;
    if type avahi-browse &>/dev/null; then
        COMPREPLY=("${COMPREPLY[@]}" $(             compgen -P "$prefix$user" -S "$suffix" -W             "$( avahi-browse -cpr _workstation._tcp 2>/dev/null |                  awk -F';' '/^=/ { print $7 }' | sort -u )" -- "$cur" ));
    fi;
    if [ -n "${COMP_KNOWN_HOSTS_WITH_HOSTFILE-1}" ]; then
        COMPREPLY=("${COMPREPLY[@]}" $( compgen -A hostname -P "$prefix$user" -S "$suffix" -- "$cur" ));
    fi;
    __ltrim_colon_completions "$prefix$user$cur";
    return 0
}
_l2ping () 
{ 
    local cur prev;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    case $prev in 
        -i)
            _bluetooth_devices;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-i -s -c -t -f -r' -- "$cur" ));
    else
        _bluetooth_adresses;
    fi
}
_lastlog () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -b | --before | -h | --help | -t | --time)
            return 0
        ;;
        -u | --user)
            COMPREPLY=($( compgen -u -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--before --help --time --user' -- "$cur" ));
        return 0;
    fi
}
_linux_fstab () 
{ 
    COMPREPLY=();
    local fs_spec fs_file fs_other;
    local oldifs="$IFS";
    while read -r fs_spec fs_file fs_other; do
        if [[ $fs_spec = [#]* ]]; then
            continue;
        fi;
        if [[ $1 == -L ]]; then
            local fs_label=${fs_spec/#LABEL=};
            if [[ $fs_label != "$fs_spec" ]]; then
                __linux_fstab_unescape fs_label;
                IFS='';
                COMPREPLY+=("$fs_label");
                IFS=$oldifs;
            fi;
        else
            __linux_fstab_unescape fs_spec;
            __linux_fstab_unescape fs_file;
            IFS='';
            [[ $fs_spec = */* ]] && COMPREPLY+=("$fs_spec");
            [[ $fs_file = */* ]] && COMPREPLY+=("$fs_file");
            IFS=$oldifs;
        fi;
    done;
    _reply_compgen_array
}
_longopt () 
{ 
    local cur prev;
    _get_comp_words_by_ref cur prev;
    if _split_longopt; then
        case "$prev" in 
            *[Dd][Ii][Rr]*)
                _filedir -d
            ;;
            *[Ff][Ii][Ll][Ee]*)
                _filedir
            ;;
        esac;
        return 0;
    fi;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W "$( $1 --help 2>&1 |             sed -ne 's/.*\(--[-A-Za-z0-9]\{1,\}\).*/\1/p' | sort -u )"             -- "$cur" ));
    else
        if [[ "$1" == rmdir ]]; then
            _filedir -d;
        else
            _filedir;
        fi;
    fi
}
_look () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [ $COMP_CWORD = 1 ]; then
        COMPREPLY=($( compgen -W '$(look "$cur" 2>/dev/null)' -- "$cur" ));
    fi
}
_lzma () 
{ 
    local cur prev xspec;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-1 -2 -3 -4 -5 -6 -7 -8 -9 \
            --help --decompress --compress --keep --force --suffix \
            --test --stdout --quiet --verbose --license --list \
            --version --small --fast --best --text' -- "$cur" ));
        return 0;
    fi;
    local IFS='	
';
    xspec="*.@(lzma|tlz)";
    if [[ "$prev" == --* ]]; then
        [[ "$prev" == --decompress || "$prev" == --list || "$prev" == --test ]] && xspec="!"$xspec;
        [[ "$prev" == --compress ]] && xspec=;
    else
        if [[ "$prev" == -* ]]; then
            [[ "$prev" == -*[dt]* ]] && xspec="!"$xspec;
            [[ "$prev" == -*z* ]] && xspec=;
        fi;
    fi;
    _expand || return 0;
    COMPREPLY=($( compgen -f -X "$xspec" -- "$cur" ) $( compgen -d -- "$cur" ))
}
_mac_addresses () 
{ 
    local re='\([A-Fa-f0-9]\{2\}:\)\{5\}[A-Fa-f0-9]\{2\}';
    local PATH="$PATH:/sbin:/usr/sbin";
    COMPREPLY=("${COMPREPLY[@]}" $( ifconfig -a 2>/dev/null | sed -ne         "s/.*[[:space:]]HWaddr[[:space:]]\{1,\}\($re\)[[:space:]]*$/\1/p" ));
    COMPREPLY=("${COMPREPLY[@]}" $( arp -an 2>/dev/null | sed -ne         "s/.*[[:space:]]\($re\)[[:space:]].*/\1/p" -ne         "s/.*[[:space:]]\($re\)[[:space:]]*$/\1/p" ));
    COMPREPLY=($( compgen -W '${COMPREPLY[@]}' -- "$cur" ));
    __ltrim_colon_completions "$cur"
}
_make () 
{ 
    local file makef makef_dir="." makef_inc cur prev i split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -f | -o | -W | --file | --makefile | --old-file | --new-file | --assume-old | --assume-new | --what-if)
            _filedir;
            return 0
        ;;
        -I | -C | --directory | --include-dir)
            _filedir -d;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-b -m -B -C -d -e -f -h -i -I\
            -j -l -k -n -o -p -q -r -R - s -S -t -v -w -W \
            --always-make --directory --debug \
            --environment-overrides --file --makefile --help \
            --ignore-errors --include-dir --jobs --load-average \
            --max-load --keep-going --just-print --dry-run \
            --recon --old-file --assume-old --print-data-base \
            --question --no-builtin-rules --no-builtin-variables \
            --silent --quiet --no-keep-goind --stop --touch \
            --version --print-directory --no-print-directory \
            --what-if --new-file --assume-new \
            --warn-undefined-variables' -- "$cur" ));
    else
        for ((i=0; i < ${#COMP_WORDS[@]}; i++ ))
        do
            if [[ ${COMP_WORDS[i]} == -@(C|-directory) ]]; then
                eval makef_dir=${COMP_WORDS[i+1]};
                break;
            fi;
        done;
        for ((i=0; i < ${#COMP_WORDS[@]}; i++ ))
        do
            if [[ ${COMP_WORDS[i]} == -@(f|-?(make)file) ]]; then
                eval makef=${COMP_WORDS[i+1]};
                break;
            fi;
        done;
        [ -n "$makef" ] && makef="-f ${makef}";
        [ -n "$makef_dir" ] && makef_dir="-C ${makef_dir}";
        COMPREPLY=($( compgen -W "$( make -qp $makef $makef_dir 2>/dev/null |             awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ \
            {split($1,A,/ /);for(i in A)print A[i]}' )"             -- "$cur" ));
    fi
}
_man () 
{ 
    local cur i prev sect manpath manext mansect uname;
    manext="@([0-9lnp]|[0-9][px]|man|3pm)?(.@([gx]z|bz2|lzma|Z))";
    mansect="@([0-9lnp]|[0-9][px]|3pm)";
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [[ "$prev" == -l ]]; then
        _filedir $manext;
        return 0;
    fi;
    _expand || return 0;
    if [[ "$cur" == */* ]]; then
        _filedir $manext;
        return 0;
    fi;
    uname=$( uname -s );
    if [[ $uname == @(Linux|GNU|GNU/*|FreeBSD|Cygwin|CYGWIN_*) ]]; then
        manpath=$( manpath 2>/dev/null || command man --path );
    else
        manpath=$MANPATH;
    fi;
    if [ -z "$manpath" ]; then
        COMPREPLY=($( compgen -c -- "$cur" ));
        return 0;
    fi;
    [[ "$prev" == $mansect ]] && sect=$prev || sect='*';
    manpath=$manpath:;
    if [ -n "$cur" ]; then
        manpath="${manpath//://*man$sect/$cur* } ${manpath//://*cat$sect/$cur* }";
    else
        manpath="${manpath//://*man$sect/ } ${manpath//://*cat$sect/ }";
    fi;
    COMPREPLY=($( eval command ls "$manpath" 2>/dev/null ));
    COMPREPLY=(${COMPREPLY[@]##*/?(:)});
    COMPREPLY=(${COMPREPLY[@]%.@([gx]z|bz2|lzma|Z)});
    COMPREPLY=($( compgen -W '${COMPREPLY[@]%.*}' -- "${cur//\\\\/}" ));
    if [[ "$prev" != $mansect ]]; then
        local start=${#COMPREPLY[@]};
        _filedir $manext;
        for ((i=$start; i < ${#COMPREPLY[@]}; i++ ))
        do
            [[ ${COMPREPLY[i]} == */* ]] || COMPREPLY[i]=./${COMPREPLY[i]};
        done;
    fi;
    return 0
}
_mii_tool () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -F | --force)
            COMPREPLY=($( compgen -W '100baseTx-FD 100baseTx-HD \
                10baseT-FD 10baseT-HD' -- "$cur" ));
            return 0
        ;;
        -A | --advertise)
            COMPREPLY=($( compgen -W '100baseT4 100baseTx-FD 100baseTx-HD \
                10baseT-FD 10baseT-HD' -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--verbose --version --reset --restart \
            --watch --log --advertise --force' -- "$cur" ));
    else
        _available_interfaces -a;
    fi
}
_mkisofs () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -o | -abstract | -biblio | -check-session | -copyright | -log-file | -root-info | -prep-boot | -*-list)
            _filedir;
            return 0
        ;;
        -*-charset)
            COMPREPLY=($( compgen -W '$( mkisofs -input-charset \
                    help 2>&1 | tail -n +3 )' -- "$cur" ));
            return 0
        ;;
        -uid)
            _uids;
            return 0
        ;;
        -gid)
            _gids;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-abstract -appid -allow-lowercase \
            -allow-multidot -biblio -cache-inodes -no-cache-inodes \
            -eltorito-boot -eltorito-alt-boot -sparc-boot -generic-boot \
            -hard-disk-boot -no-emul-boot -no-boot -boot-load-seg \
            -boot-load-size -boot-info-table -cdrecord-params \
            -eltorito-catalog -check-oldname -check-session -copyright \
            -omit-period -disable-deep-relocation -dir-mode -dvd-video \
            -follow-links -file-mode -gid -gui -graft-points -hide -hide-list \
            -hidden -hidden-list -hide-joliet -hide-joliet-list \
            -hide-joliet-trans-tbl -hide-rr-moved -input-charset \
            -output-charset -iso-level -joliet -joliet-long -jcharset \
            -full-iso9660-filenames -allow-leading-dots -log-file -exclude \
            -exclude-list -max-iso9660-filenames -prev-session \
            -omit-version-number -new-dir-mode -nobak -no-bak -force-rr -no-rr \
            -no-split-symlink-components -no-split-symlink-fields -output -pad \
            -no-pad -path-list -publisher -preparer -print-size -quiet -rock
            -rational-rock -relaxed-filenames -sort -split-output \
            -stream-media-size -stream-file-name -sysid -translation-table \
            -table-name -ucs-level -udf -uid -use-fileversion \
            -untranslated-filenames -no-iso-translate -volid -volset \
            -volset-size -volset-seqno -verbose -old-exclude \
            -transparent-compression -hfs -apple -map -magic -hfs-creator \
            -hfs-type -probe -no-desktop -mac-name \
            -boot-hfs-file -part -auto -cluster-size \
            -hide-hfs -hide-hfs-list -hfs-volid \
            -icon-position -root-info -prep-boot \
            -input-hfs-charset -output-hfs-charset \
            -hfs-unlock -hfs-bless -hfs-parms --cap \
            --netatalk --double --ethershare --ushare \
            --exchange --sgi --xinet --macbin --single \
            --dave --sfm --osx-double --osx-hfs' -- "$cur" ));
    else
        _filedir;
    fi
}
_modules () 
{ 
    local modpath;
    modpath=/lib/modules/$1;
    COMPREPLY=($( compgen -W "$( command ls -R $modpath |         sed -ne 's/^\(.*\)\.k\{0,1\}o\(\.gz\)\{0,1\}$/\1/p' )" -- "$cur" ))
}
_mogrify () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-adaptive-blur -adaptive-resize \
            -adaptive-sharpen -adjoin -affine -alpha -annotate \
            -antialias -attenuate -authenticate -auto-orient \
            -background -bias -black-point-compensation \
            -black-threshold -blue-primary -blur -border \
            -bordercolor -caption -channel -charcoal -chop -clip \
            -clip-mask -clip-path -clut -colorize -colors \
            -colorspace -comment -compose -compress -contrast \
            -contrast-stretch -convolve -cycle -debug -decipher \
            -define -delay -density -depth -despeckle -display \
            -dispose -distort -dither -draw -edge -emboss \
            -encipher -encoding -endian -enhance -equalize \
            -evaluate -extent -extract -family -fill -filter \
            -flip -floodfill -flop -font -format -frame -fuzz \
            -gamma -gaussian-blur -geometry -gravity \
            -green-primary -help -identify -implode -intent \
            -interlace -interpolate -label -lat -layers -level \
            -limit -linear-stretch -liquid-rescale -list -log \
            -loop -mask -mattecolor -median -modulate -monitor \
            -monochrome -motion-blur -negate -noise -normalize \
            -opaque -ordered-dither -orient -page -paint -path \
            -ping -pointsize -polaroid -posterize -preview -print \
            -profile -quality -quantize -quiet -radial-blur \
            -raise -random-threshold -recolor -red-primary \
            -regard-warnings -region -render -repage -resample \
            -resize -roll -rotate -sample -sampling-factor -scale \
            -scene -seed -segment -sepia-tone -set -shade -shadow \
            -sharpen -shave -shear -sigmoidal-contrast -size \
            -sketch -solarize -splice -spread -stretch -strip \
            -stroke -strokewidth -style -swirl -taint -texture \
            -threshold -thumbnail -tile -tile-offset -tint \
            -transform -transparent -transparent-color -transpose \
            -transverse -treedepth -trim -type -undercolor \
            -unique-colors -units -unsharp -verbose -version \
            -view -vignette -virtual-pixel -wave -weight \
            -white-point -white-threshold' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+compress +contrast +debug +dither \
            +endian +gamma +label +map +mask +matte +negate +page \
            +raise' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_montage () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-adjoin -affine -alpha \
            -authenticate -background -blue-primary -blur -border \
            -bordercolor -borderwidth -caption -channel -clone \
            -coalesce -colors -colorspace -comment -compose \
            -compress -crop -debug -define -density -depth \
            -display -dispose -dither -draw -encoding -endian \
            -extract -fill -filter -flatten -flip -flop -font \
            -format -frame -gamma -geometry -gravity \
            -green-primary -help -identify -interlace \
            -interpolate -label -limit -list -log -mattecolor \
            -mode -monitor -monochrome -origin -page -pointsize \
            -polaroid -profile -quality -quantize -quiet \
            -red-primary -regard-warnings -repage -resize \
            -respect-parenthesis -rotate -sampling-factor -scenes \
            -seed -set -shadow -size -strip -stroke -texture \
            -thumbnail -tile -title -transform -transparent \
            -transparent-color -treedepth -trim -type -units \
            -verbose -version -virtual-pixel \
            -white-point' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+adjoin +compress +debug +dither \
            +endian +gamma +label +matte +page' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_mount () 
{ 
    local cur sm host prev;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    case $prev in 
        -t | --types)
            _fstypes;
            return 0
        ;;
    esac;
    [[ "$cur" == \\ ]] && cur="/";
    if [[ "$cur" == *:* ]]; then
        for sm in "$(type -P showmount)" {,/usr}/{,s}bin/showmount;
        do
            [ -x "$sm" ] || continue;
            COMPREPLY=($( compgen -W "$( "$sm" -e ${cur%%:*} |                 awk 'NR>1 {print $1}' )" -- "${cur#*:}" ));
            return 0;
        done;
    fi;
    if [[ "$cur" == //* ]]; then
        host=${cur#//};
        host=${host%%/*};
        if [ -n "$host" ]; then
            COMPREPLY=($( compgen -P "//$host" -W                 "$( smbclient -d 0 -NL $host 2>/dev/null |
                sed -ne '/^['"$'\t '"']*Sharename/,/^$/p' |
                sed -ne '3,$s|^[^A-Za-z]*\([^'"$'\t '"']*\).*$|/\1|p' )"                     -- "${cur#//$host}" ));
        fi;
    else
        if [ -r /etc/vfstab ]; then
            COMPREPLY=($( compgen -W "$( awk '! /^[ \t]*#/ {if ($3 ~ /\//) print $3}' /etc/vfstab )" -- "$cur" ));
        else
            if [ ! -e /etc/fstab ]; then
                COMPREPLY=($( compgen -W "$( mount | awk '! /^[ \t]*#/ {if ($3 ~ /\//) print $3}' )" -- "$cur" ));
            else
                if [ "$prev" = -L ]; then
                    _linux_fstab -L < /etc/fstab;
                else
                    if [ "$prev" = -U ]; then
                        COMPREPLY=($( compgen -W '$(sed -ne "s/^[[:space:]]*UUID=\([^[:space:]]*\).*/\1/p" /etc/fstab )' -- "$cur" ));
                    else
                        _linux_fstab < /etc/fstab;
                    fi;
                fi;
            fi;
        fi;
    fi;
    return 0
}
_mutt () 
{ 
    local cur prev;
    _get_comp_words_by_ref -n =+! cur prev;
    COMPREPLY=();
    case $cur in 
        -*)
            COMPREPLY=($( compgen -W '-A -a -b -c -e -f -F -H -i -m -n \
            -p -Q -R -s -v -x -y -z -Z -h' -- "$cur" ));
            return 0
        ;;
        *)
            case $prev in 
                -a | -f | -F | -H | -i)
                    _muttfiledir "$cur";
                    return 0
                ;;
                -A)
                    _muttaliases "$cur";
                    return 0
                ;;
                -e | -m | -Q | -s | -h | -p | -R | -v | -y | -z | -Z)
                    return 0
                ;;
                *)
                    _muttaddr "$cur";
                    return 0
                ;;
            esac
        ;;
    esac
}
_muttaddr () 
{ 
    _muttaliases "$1";
    _muttquery "$1";
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -u -- "$1" ));
    return 0
}
_muttaliases () 
{ 
    local cur=$1 muttrc muttcmd=${COMP_WORDS[0]};
    local -a conffiles aliases;
    muttrc=$(_muttrc);
    [ -z "$muttrc" ] && return 0;
    conffiles=($(eval _muttconffiles $muttrc $muttrc));
    aliases=($( sed -n 's|^alias[[:space:]]\{1,\}\([^[:space:]]\{1,\}\).*$|\1|p'         $(eval echo "${conffiles[@]}") ));
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W "${aliases[*]}" -- "$cur" ));
    return 0
}
_muttconffiles () 
{ 
    local file sofar;
    local -a newconffiles;
    sofar=" $1 ";
    shift;
    while [[ -n "$1" ]]; do
        newconffiles=($(sed -n 's|^source[[:space:]]\{1,\}\([^[:space:]]\{1,\}\).*$|\1|p' $(eval echo $1) ));
        for file in "${newconffiles[@]}";
        do
            [[ ! -n "$file" ]] || [[ "${sofar/ ${file} / }" != "$sofar" ]] && continue;
            sofar="$sofar $file";
            sofar=" $(eval _muttconffiles \"$sofar\" $file) ";
        done;
        shift;
    done;
    printf '%s\n' $sofar
}
_muttfiledir () 
{ 
    local cur=$1 folder muttrc spoolfile muttcmd=${COMP_WORDS[0]};
    muttrc=$(_muttrc);
    if [[ $cur == [=+]* ]]; then
        folder="$( $muttcmd -F "$muttrc" -Q folder | sed -e 's|^folder=\"\(.*\)\"$|\1|' )";
        : folder:=~/Mail;
        COMPREPLY=($( compgen -f -- "$folder/${cur:1}" ));
        COMPREPLY=(${COMPREPLY[@]#$folder/});
        return 0;
    else
        if [ "$cur" == !* ]; then
            spoolfile="$( $muttcmd -F "$muttrc" -Q spoolfile |             sed -e 's|^spoolfile=\"\(.*\)\"$|\1|' )";
            [ ! -z "$spoolfile" ] && eval cur="${cur/^!/$spoolfile}";
        fi;
    fi;
    _filedir;
    return 0
}
_muttquery () 
{ 
    local cur=$1 querycmd muttcmd=${COMP_WORDS[0]};
    local -a queryresults;
    querycmd="$( $muttcmd -Q query_command | sed -e 's|^query_command=\"\(.*\)\"$|\1|' -e 's|%s|'$cur'|' )";
    if [[ -z "$cur" || -z "$querycmd" ]]; then
        queryresults=();
    else
        queryresults=($( $querycmd |             sed -n '2,$s|^\([^[:space:]]\{1,\}\).*|\1|p' ));
    fi;
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W "${queryresults[*]}"         -- "$cur" ));
    return 0
}
_muttrc () 
{ 
    set -- "${COMP_WORDS[@]}";
    while [ $# -gt 0 ]; do
        if [ "${1:0:2}" = -F ]; then
            if [ ${#1} -gt 2 ]; then
                muttrc="$(dequote "${1:2}")";
            else
                shift;
                [ "$1" ] && muttrc="$(dequote "$1")";
            fi;
            break;
        fi;
        shift;
    done;
    if [ -z "$muttrc" ]; then
        [ -f ~/.${muttcmd}/${muttcmd}rc ] && muttrc="~/.${muttcmd}/${muttcmd}rc";
        [ -f ~/.${muttcmd}rc ] && muttrc="~/.${muttcmd}rc";
    fi;
    printf "%s" "$muttrc"
}
_newgrp () 
{ 
    COMPREPLY=();
    if [[ "`_get_cword`" == "-" ]]; then
        COMPREPLY=(-);
    fi
}
_newusers () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -c | --crypt)
            COMPREPLY=($( compgen -W 'DES MD5 NONE SHA256 SHA512'                 -- "$cur" ));
            return 0
        ;;
        -s | --sha-rounds)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--crypt-method --help --system \
            --sha-rounds' -- "$cur" ));
        return 0;
    fi;
    _filedir
}
_nslookup () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -P '-' -W 'all class= debug d2 domain= srchlist= \
        defname search port= querytype= type= recurse retry root timeout vc \
        ignoretc' -- "$cur" ))
}
_ooexp_ () 
{ 
    local c=${COMP_WORDS[COMP_CWORD]};
    local a="${COMP_LINE}";
    local e s g=0 cd dc t="";
    local IFS;
    shopt -q extglob && g=1;
    test $g -eq 0 && shopt -s extglob;
    cd='*-?(c)d*';
    dc='*-d?(c)*';
    case "${1##*/}" in 
        oobase)
            e='!*.+(odb|ODB)'
        ;;
        oofromtemplate)
            e='!*.+(stw|STW|dot|DOT|vor|VOR|stc|STC|xlt|XLT|sti|STI|pot|POT|std|STD|stw|STW|dotm|DOTM|dotx|DOTX|potm|POTM|potx|POTX|xltm|XLTM|xltx|XLTX)'
        ;;
        oodraw)
            e='!*.+(sxd|SXD|std|STD|dxf|DXF|emf|EMF|eps|EPS|met|MET|pct|PCT|sgf|SGF|sgv|SGV|sda|SDA|sdd|SDD|vor|VOR|svm|SVM|wmf|WMF|bmp|BMP|gif|GIF|jpg|JPG|jpeg|JPEG|jfif|JFIF|fif|FIF|jpe|JPE|pcd|PCD|pcx|PCX|pgm|PGM|png|PNG|ppm|PPM|psd|PSD|ras|RAS|tga|TGA|tif|TIF|tiff|TIFF|xbm|XBM|xpm|XPM|odg|ODG|otg|OTG|fodg|FODG|odc|ODC|odi|ODI|sds|SDS|wpg|WPG|svg|SVG)'
        ;;
        oocalc)
            e='!*.+(sxc|SXC|stc|STC|dif|DIF|dbf|DBF|xls|XLS|xlw|XLW|xlt|XLT|rtf|RTF|sdc|SDC|vor|VOR|slk|SLK|txt|TXT|htm|HTM|html|HTML|wk1|WK1|wks|WKS|123|123|xml|XML|ods|ODS|ots|OTS|fods|FODS|csv|CSV|xlsb|XLSB|xlsm|XLSM|xlsx|XLSX|xltm|XLTM|xltx|XLTX)'
        ;;
        oomath)
            e='!*.+(sxm|SXM|smf|SMF|mml|MML|odf|ODF)'
        ;;
        ooweb)
            e='!*.+(htm|HTM|html|HTML|stw|STW|txt|TXT|vor|VOR|oth|OTH)'
        ;;
        ooffice)
            e='!*.+(sxd|SXD|std|STD|dxf|DXF|emf|EMF|eps|EPS|met|MET|pct|PCT|sgf|SGF|sgv|SGV|sda|SDA|sdd|SDD|vor|VOR|svm|SVM|wmf|WMF|bmp|BMP|gif|GIF|jpg|JPG|jpeg|JPEG|jfif|JFIF|fif|FIF|jpe|JPE|pcd|PCD|pcx|PCX|pgm|PGM|png|PNG|ppm|PPM|psd|PSD|ras|RAS|tga|TGA|tif|TIF|tiff|TIFF|xbm|XBM|xpm|XPM|odg|ODG|otg|OTG|fodg|FODG|odc|ODC|odi|ODI|sds|SDS|wpg|WPG|svg|SVG|doc|DOC|dot|DOT|rtf|RTF|sxw|SXW|stw|STW|sdw|SDW|vor|VOR|txt|TXT|htm?|HTM?|xml|XML|wp|WP|wpd|WPD|wps|WPS|odt|ODT|ott|OTT|fodt|FODT|docm|DOCM|docx|DOCX|dotm|DOTM|dotx|DOTX|sxm|SXM|smf|SMF|mml|MML|odf|ODF|sxi|SXI|sti|STI|ppt|PPT|pps|PPS|pot|POT|sxd|SXD|sda|SDA|sdd|SDD|sdp|SDP|vor|VOR|cgm|CGM|odp|ODP|otp|OTP|fodp|FODP|ppsm|PPSM|ppsx|PPSX|pptm|PPTM|pptx|PPTX|potm|POTM|potx|POTX|odb|ODB|sxc|SXC|stc|STC|dif|DIF|dbf|DBF|xls|XLS|xlw|XLW|xlt|XLT|rtf|RTF|sdc|SDC|vor|VOR|slk|SLK|txt|TXT|htm|HTM|html|HTML|wk1|WK1|wks|WKS|123|123|xml|XML|ods|ODS|ots|OTS|fods|FODS|csv|CSV|xlsb|XLSB|xlsm|XLSM|xlsx|XLSX|xltm|XLTM|xltx|XLTX|sxg|SXG|odm|ODM|sgl|SGL|stw|STW|dot|DOT|vor|VOR|stc|STC|xlt|XLT|sti|STI|pot|POT|std|STD|stw|STW|dotm|DOTM|dotx|DOTX|potm|POTM|potx|POTX|xltm|XLTM|xltx|XLTX|htm|HTM|html|HTML|stw|STW|txt|TXT|vor|VOR|oth|OTH)'
        ;;
        oowriter)
            e='!*.+(doc|DOC|dot|DOT|rtf|RTF|sxw|SXW|stw|STW|sdw|SDW|vor|VOR|txt|TXT|htm?|HTM?|xml|XML|wp|WP|wpd|WPD|wps|WPS|odt|ODT|ott|OTT|fodt|FODT|docm|DOCM|docx|DOCX|dotm|DOTM|dotx|DOTX|sxg|SXG|odm|ODM|sgl|SGL)'
        ;;
        ooimpress)
            e='!*.+(sxi|SXI|sti|STI|ppt|PPT|pps|PPS|pot|POT|sxd|SXD|sda|SDA|sdd|SDD|sdp|SDP|vor|VOR|cgm|CGM|odp|ODP|otp|OTP|fodp|FODP|ppsm|PPSM|ppsx|PPSX|pptm|PPTM|pptx|PPTX|potm|POTM|potx|POTX)'
        ;;
        unopkg)
            e='!*.+(oxt|OXT)'
        ;;
        *)
            e='!*'
        ;;
    esac;
    case "$(complete -p ${1##*/} 2> /dev/null)" in 
        *-d*)

        ;;
        *)
            s="-S/"
        ;;
    esac;
    IFS='
';
    case "$c" in 
        \$\(*\))
            eval COMPREPLY=\(${c}\)
        ;;
        \$\(*)
            COMPREPLY=($(compgen -c -P '$(' -S ')'  -- ${c#??}))
        ;;
        \`*\`)
            eval COMPREPLY=\(${c}\)
        ;;
        \`*)
            COMPREPLY=($(compgen -c -P '\`' -S '\`' -- ${c#?}))
        ;;
        \$\{*\})
            eval COMPREPLY=\(${c}\)
        ;;
        \$\{*)
            COMPREPLY=($(compgen -v -P '${' -S '}'  -- ${c#??}))
        ;;
        \$*)
            COMPREPLY=($(compgen -v -P '$'          -- ${c#?}))
        ;;
        \~*/*)
            COMPREPLY=($(compgen -f -X "$e"         -- ${c}))
        ;;
        \~*)
            COMPREPLY=($(compgen -u ${s}	 	-- ${c}))
        ;;
        *@*)
            COMPREPLY=($(compgen -A hostname -P '@' -S ':' -- ${c#*@}))
        ;;
        *[*?[]*)
            COMPREPLY=($(compgen -G "${c}"))
        ;;
        *[?*+\!@]\(*\)*)
            if test $g -eq 0; then
                COMPREPLY=($(compgen -f -X "$e" -- $c));
                test $g -eq 0 && shopt -u extglob;
                return;
            fi;
            COMPREPLY=($(compgen -G "${c}"))
        ;;
        *)
            if test "$c" = ".."; then
                COMPREPLY=($(compgen -d -X "$e" -S / ${_nosp} -- $c));
            else
                for s in $(compgen -f -X "$e" -- $c);
                do
                    if test -d $s; then
                        COMPREPLY=(${COMPREPLY[@]} $(compgen -f -X "$e" -S / -- $s));
                    else
                        if test -z "$t"; then
                            COMPREPLY=(${COMPREPLY[@]} $s);
                        else
                            case "$(file -b $s 2> /dev/null)" in 
                                $t)
                                    COMPREPLY=(${COMPREPLY[@]} $s)
                                ;;
                            esac;
                        fi;
                    fi;
                done;
            fi
        ;;
    esac;
    test $g -eq 0 && shopt -u extglob
}
_openssl () 
{ 
    local cur prev commands command options formats;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    commands='asn1parse ca ciphers crl crl2pkcs7 dgst dh dhparam dsa \
        dsaparam ec ecparam enc engine errstr gendh gendsa genrsa \
        nseq ocsp passwd pkcs12 pkcs7 pkcs8 prime rand req rsa \
        rsautl s_client s_server s_time sess_id smime speed spkac \
        verify version x509 md2 md4 md5 rmd160 sha sha1 aes-128-cbc \
        aes-128-ecb aes-192-cbc aes-192-ecb aes-256-cbc aes-256-ecb \
        base64 bf bf-cbc bf-cfb bf-ecb bf-ofb camellia-128-cbc \
        camellia-128-ecb camellia-192-cbc camellia-192-ecb \
        camellia-256-cbc camellia-256-ecb cast cast-cbc cast5-cbc \
        cast5-cfb cast5-ecb cast5-ofb des des-cbc des-cfb des-ecb \
        des-ede des-ede-cbc des-ede-cfb des-ede-ofb des-ede3 \
        des-ede3-cbc des-ede3-cfb des-ede3-ofb des-ofb des3 desx rc2 \
        rc2-40-cbc rc2-64-cbc rc2-cbc rc2-cfb rc2-ecb rc2-ofb rc4 \
        rc4-40';
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($( compgen -W "$commands" -- "$cur" ));
    else
        command=${COMP_WORDS[1]};
        case $prev in 
            -CA | -CAfile | -CAkey | -CAserial | -cert | -certfile | -config | -content | -dcert | -dkey | -dhparam | -extfile | -in | -inkey | -kfile | -key | -keyout | -out | -oid | -prvrify | -rand | -recip | -revoke | -sess_in | -sess_out | -spkac | -sign | -signkey | -signer | -signature | -ss_cert | -untrusted | -verify)
                _filedir;
                return 0
            ;;
            -outdir | -CApath)
                _filedir -d;
                return 0
            ;;
            -name | -crlexts | -extensions)
                _openssl_sections;
                return 0
            ;;
            -inform | -outform | -keyform | -certform | -CAform | -CAkeyform | -dkeyform | -dcertform)
                formats='DER PEM';
                case $command in 
                    x509)
                        formats="$formats NET"
                    ;;
                    smime)
                        formats="$formats SMIME"
                    ;;
                esac;
                COMPREPLY=($( compgen -W "$formats" -- "$cur" ));
                return 0
            ;;
            -connect)
                _known_hosts_real "$cur";
                return 0
            ;;
            -starttls)
                COMPREPLY=($( compgen -W 'smtp pop3 imap ftp'                     -- "$cur" ));
                return 0
            ;;
            -cipher)
                COMPREPLY=($( compgen -W "$(openssl ciphers |                     tr ':' '\n')" -- "$cur" ));
                return 0
            ;;
        esac;
        if [[ "$cur" == -* ]]; then
            case $command in 
                asn1parse)
                    options='-inform -in -out -noout -offset -length -i -oid \
                        -strparse'
                ;;
                ca)
                    options='-verbose -config -name -gencrl -revoke \
                        -crl_reason -crl_hold -crl_compromise \
                        -crl_CA_compromise -crldays -crlhours -crlexts \
                        -startdate -enddate -days -md -policy -keyfile -key \
                        -passin -cert -selfsig -in -out -notext -outdir \
                        -infiles -spkac -ss_cert -preserveDN -noemailDN \
                        -batch -msie_hack -extensions -extfile -engine \
                        -subj -utf8 -multivalue-rdn'
                ;;
                ciphers)
                    options='-v -ssl2 -ssl3 -tls1'
                ;;
                crl)
                    options='-inform -outform -text -in -out -noout -hash \
                        -issuer -lastupdate -nextupdate -CAfile -CApath'
                ;;
                crl2pkcs7)
                    options='-inform -outform -in -out -print_certs'
                ;;
                dgst)
                    options='-md5 -md4 -md2 -sha1 -sha -mdc2 -ripemd160 -dss1 \
                        -c -d -hex -binary -out -sign -verify -prverify \
                        -signature'
                ;;
                dsa)
                    options='-inform -outform -in -passin -out -passout -des \
                        -des3 -idea -text -noout -modulus -pubin -pubout'
                ;;
                dsaparam)
                    options='-inform -outform -in -out -noout -text -C -rand \
                        -genkey'
                ;;
                enc)
                    options='-ciphername -in -out -pass -e -d -a -A -k -kfile \
                        -S -K -iv -p -P -bufsize -debug'
                ;;
                dhparam)
                    options='-inform -outform -in -out -dsaparam -noout -text \
                        -C -2 -5 -rand'
                ;;
                gendsa)
                    options='-out -des -des3 -idea -rand'
                ;;
                genrsa)
                    options='-out -passout -des -des3 -idea -f4 -3 -rand'
                ;;
                pkcs7)
                    options='-inform -outform -in -out -print_certs -text \
                        -noout'
                ;;
                rand)
                    options='-out -rand -base64'
                ;;
                req)
                    options='-inform -outform -in -passin -out -passout -text \
                        -noout -verify -modulus -new -rand -newkey -newkey \
                        -nodes -key -keyform -keyout -md5 -sha1 -md2 -mdc2 \
                        -config -x509 -days -asn1-kludge -newhdr -extensions \
                        -reqexts section'
                ;;
                rsa)
                    options='-inform -outform -in -passin -out -passout \
                        -sgckey -des -des3 -idea -text -noout -modulus -check \
                        -pubin -pubout -engine'
                ;;
                rsautl)
                    options='-in -out -inkey -pubin -certin -sign -verify \
                        -encrypt -decrypt -pkcs -ssl -raw -hexdump -asn1parse'
                ;;
                s_client)
                    options='-connect -verify -cert -certform -key -keyform \
                        -pass -CApath -CAfile -reconnect -pause -showcerts \
                        -debug -msg -nbio_test -state -nbio -crlf -ign_eof \
                        -quiet -ssl2 -ssl3 -tls1 -no_ssl2 -no_ssl3 -no_tls1 \
                        -bugs -cipher -starttls -engine -tlsextdebug \
                        -no_ticket -sess_out -sess_in -rand'
                ;;
                s_server)
                    options='-accept -context -verify -Verify -crl_check \
                        -crl_check_all -cert -certform -key -keyform -pass \
                        -dcert -dcertform -dkey -dkeyform -dpass -dhparam \
                        -nbio -nbio_test -crlf -debug -msg -state -CApath \
                        -CAfile -nocert -cipher -quiet -no_tmp_rsa -ssl2 \
                        -ssl3 -tls1 -no_ssl2 -no_ssl3 -no_tls1 -no_dhe \
                        -bugs -hack -www -WWW -HTTP -engine -tlsextdebug \
                        -no_ticket -id_prefix -rand'
                ;;
                s_time)
                    options='-connect -www -cert -key -CApath -CAfile -reuse \
                        -new -verify -nbio -time -ssl2 -ssl3 -bugs -cipher'
                ;;
                sess_id)
                    options='-inform -outform -in -out -text -noout -context \
                        ID'
                ;;
                smime)
                    options='-encrypt -decrypt -sign -verify -pk7out -des \
                        -des3 -rc2-40 -rc2-64 -rc2-128 -aes128 -aes192 -aes256 \
                        -in -certfile -signer -recip -inform -passin -inkey \
                        -out -outform -content -to -from -subject -text -rand'
                ;;
                speed)
                    options='-engine'
                ;;
                verify)
                    options='-CApath -CAfile -purpose -untrusted -help \
                        -issuer_checks -verbose -certificates'
                ;;
                x509)
                    options='-inform -outform -keyform -CAform -CAkeyform -in \
                        -out -serial -hash -subject-hash -issuer_hash -subject \
                        -issuer -nameopt -email -startdate -enddate -purpose \
                        -dates -modulus -fingerprint -alias -noout -trustout \
                        -clrtrust -clrreject -addtrust -addreject -setalias \
                        -days -set_serial -signkey -x509toreq -req -CA -CAkey \
                        -CAcreateserial -CAserial -text -C -md2 -md5 -sha1 \
                        -mdc2 -clrext -extfile -extensions -engine'
                ;;
                md5 | md4 | md2 | sha1 | sha | mdc2 | ripemd160)
                    options='-c -d'
                ;;
            esac;
            COMPREPLY=($( compgen -W "$options" -- "$cur" ));
        else
            if [[ "$command" == speed ]]; then
                COMPREPLY=($( compgen -W 'md2 mdc2 md5 hmac sha1 rmd160 \
                    idea-cbc rc2-cbc rc5-cbc bf-cbc des-cbc des-ede3 rc4 \
                    rsa512 rsa1024 rsa2048 rsa4096 dsa512 dsa1024 dsa2048 idea \
                    rc2 des rsa blowfish' -- "$cur" ));
            else
                _filedir;
            fi;
        fi;
    fi
}
_openssl_sections () 
{ 
    local config f;
    for ((i=2; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" == -config ]]; then
            config=${COMP_WORDS[i+1]};
            break;
        fi;
    done;
    if [ -z "$config" ]; then
        for f in /etc/ssl/openssl.cnf /etc/pki/tls/openssl.cnf /usr/share/ssl/openssl.cnf;
        do
            [ -f $f ] && config=$f && break;
        done;
    fi;
    [ ! -f "$config" ] && return 0;
    COMPREPLY=($( compgen -W "$( awk '/\[.*\]/ {print $2}' $config )"         -- "$cur" ))
}
_pack200 () 
{ 
    COMPREPLY=();
    local cur prev;
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -S | --segment-limit | -P | --pass-file | -C | --class-attribute | -F | --field-attribute | -M | --method-attribute | -D | --code-attribute | '-?' | -h | --help | -V | --version | -J)
            return 0
        ;;
        -E | --effort)
            COMPREPLY=($( compgen -W '0 1 2 3 4 5 6 7 8 9' -- "$cur" ));
            return 0
        ;;
        -H | --deflate-hint)
            COMPREPLY=($( compgen -W 'true false keep' -- "$cur" ));
            return 0
        ;;
        -m | --modification-time)
            COMPREPLY=($( compgen -W 'latest keep' -- "$cur" ));
            return 0
        ;;
        -U | --unknown-attribute)
            COMPREPLY=($( compgen -W 'error strip pass' -- "$cur" ));
            return 0
        ;;
        -f | --config-file)
            _filedir properties;
            return 0
        ;;
        -l | --log-file)
            COMPREPLY=($( compgen -W '-' -- "$cur" ));
            _filedir log;
            return 0
        ;;
    esac;
    local i pack=false jar=false;
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        case ${COMP_WORDS[i]} in 
            *.pack | *.pack.gz)
                pack=true
            ;;
            *.jar)
                jar=true
            ;;
        esac;
    done;
    if ! $pack; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '--no-gzip --gzip --strip-debug \
                --no-keep-file-order --segment-limit= --effort= \
                --deflate-hint= --modification-time= --pass-file= \
                --unknown-attribute= --class-attribute= --field-attribute= \
                --method-attribute= --code-attribute= --config-file= \
                --verbose --quiet --log-file= --help --version -J' -- "$cur" ));
            [[ ${#COMPREPLY[@]} -eq 1 && ${COMPREPLY[0]} == *= ]] && type compopt &>/dev/null && compopt -o nospace;
        else
            _filedir 'pack?(.gz)';
        fi;
    else
        if ! $jar; then
            _filedir jar;
        fi;
    fi
}
_parse_help () 
{ 
    local cmd;
    cmd=$1;
    $cmd --help 2>&1 | command grep -- "^[[:space:]]*-" | tr "," " " | awk '{print $1; if ($2 ~ /-.*/) { print $2 } }' | sed -e "s:=.*::g"
}
_passwd () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -n | -x | -w | -i | -\? | --help | --usage)
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-k -l --stdin -u -d -n -x -w -i -S \
            -? --help --usage' -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -u -- "$cur" ))
}
_pci_ids () 
{ 
    COMPREPLY=(${COMPREPLY[@]:-} $( compgen -W         "$( PATH="$PATH:/sbin" lspci -n | awk '{print $3}')" -- "$cur" ))
}
_perl () 
{ 
    local cur prev prefix temp;
    local optPrefix optSuffix;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    prefix="";
    if [[ "$cur" == -?* ]]; then
        temp=$cur;
        prev=${temp:0:2};
        cur=${temp:2};
        optPrefix=-P$prev;
        optSuffix=-S/;
        prefix=$prev;
    fi;
    case $prev in 
        -I | -x)
            local IFS='	
';
            COMPREPLY=($( compgen -d $optPrefix $optSuffix -- "$cur" ));
            return 0
        ;;
        -m | -M)
            _perlmodules "$cur";
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-C -s -T -u -U -W -X -h -v -V -c -w -d \
            -D -p -n -a -F -l -0 -I -m -M -P -S -x -i -e ' -- "$cur" ));
    else
        _filedir;
    fi
}
_perldoc () 
{ 
    local cur prev prefix temp;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    prefix="";
    if [[ "$cur" == -?* ]]; then
        temp=$cur;
        prev=${temp:0:2};
        cur=${temp:2};
        prefix=$prev;
    fi;
    case $prev in 
        -f)
            COMPREPLY=($( compgen -W 'chomp chop chr crypt hex index lc \
                lcfirst length oct ord pack q qq reverse rindex sprintf \
                substr tr uc ucfirst y m pos quotemeta s split study qr abs \
                atan2 cos exp hex int log oct rand sin sqrt srand pop push \
                shift splice unshift grep join map qw reverse sort unpack \
                delete each exists keys values binmode close closedir \
                dbmclose dbmopen die eof fileno flock format getc print \
                printf read readdir rewinddir say seek seekdir select syscall \
                sysread sysseek syswrite tell telldir truncate warn write \
                pack read syscall sysread syswrite unpack vec -X chdir chmod \
                chown chroot fcntl glob ioctl link lstat mkdir open opendir \
                readlink rename rmdir stat symlink umask unlink utime caller \
                continue do dump eval exit goto last next redo return \
                sub wantarray break caller import local my our state package \
                use defined formline reset scalar undef \
                alarm exec fork getpgrp getppid getpriority kill pipe qx \
                setpgrp setpriority sleep system times wait waitpid \
                import no package require use bless dbmclose dbmopen package \
                ref tie tied untie use accept bind connect getpeername \
                getsockname getsockopt listen recv send setsockopt shutdown \
                socket socketpair msgctl msgget msgrcv msgsnd semctl semget \
                semop shmctl shmget shmread shmwrite endgrent endhostent \
                endnetent endpwent getgrent getgrgid getgrnam getlogin \
                getpwent getpwnam getpwuid setgrent setpwent endprotoent \
                endservent gethostbyaddr gethostbyname gethostent \
                getnetbyaddr getnetbyname getnetent getprotobyname \
                getprotobynumber getprotoent getservbyname getservbyport \
                getservent sethostent setnetent setprotoent setservent \
                gmtime localtime time times lock' -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-h -v -t -u -m -l -F -X -f -q' -- "$cur" ));
    else
        if [[ "$cur" != */* ]]; then
            _perlmodules "$cur";
            COMPREPLY=("${COMPREPLY[@]}" $( compgen -W                 '$( PAGER=/bin/cat man perl |  \
                sed -ne "/perl.*Perl overview/,/perlwin32/p" | \
                awk "\$NF=2 { print \$1}" | command grep perl )' -- "$cur" ));
        fi;
        _filedir '@(pl|PL|pm|PM|pod|POD)';
    fi
}
_perlmodules () 
{ 
    COMPREPLY=($( compgen -P "$prefix" -W "$( perl -e 'sub mods { my ($base,$dir)=@_; return if  $base !~ /^\Q$ENV{cur}/; chdir($dir) or return; for (glob(q[*.pm])) {s/\.pm$//; print qq[$base$_\n]}; mods(/^(?:[.\d]+|$Config{archname}-$Config{osname}|auto)$/ ? undef : qq[${base}${_}::],qq[$dir/$_]) for grep {-d} glob(q[*]); } mods(undef,$_) for @INC;' )" -- "$cur" ));
    __ltrim_colon_completions "$1"
}
_pgids () 
{ 
    COMPREPLY=($( compgen -W '$( command ps axo pgid= )' -- "$cur" ))
}
_pgrep () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _pnames;
    return 0
}
_pids () 
{ 
    COMPREPLY=($( compgen -W '$( command ps axo pid= )' -- "$cur" ))
}
_pkg_config () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        --variable | --define-variable | --atleast-version | --atleast-pkgconfig-version | --exact-version | --max-version)
            return 0
        ;;
        -\? | --help | --version | --usage)
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--version --modversion \
            --atleast-pkgconfig-version --libs --static \
            --short-errors --libs-only-l --libs-only-other \
            --libs-only-L --cflags --cflags-only-I \
            --cflags-only-other --variable --define-variable \
            --exists --uninstalled --atleast-version \
            --exact-version --max-version --list-all --debug \
            --print-errors --silence-errors --errors-to-stdout \
            --print-provides --print-requires --help --usage' -- "$cur"));
    else
        COMPREPLY=($( compgen -W "$( pkg-config --list-all             2>/dev/null | awk '{print $1}' )" -- "$cur" ));
    fi
}
_pm_action () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -W "--help $( _parse_help "$1" )" -- "$cur" ))
}
_pm_is_supported () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -W '--help --suspend --hibernate --suspend-hybrid'         -- "$cur" ))
}
_pm_powersave () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -W "true false" -- "$cur" ))
}
_pnames () 
{ 
    COMPREPLY=($( compgen -X '<defunct>' -W '$( command ps axo command= | \
        sed -e "s/ .*//" -e "s:.*/::" -e "s/:$//" -e "s/^[[(-]//" \
            -e "s/[])]$//" | sort -u )' -- "$cur" ))
}
_poff () 
{ 
    local prev cur conns;
    [ -r /etc/ppp/peers/ ] || return 0;
    COMPREPLY=();
    prev=${COMP_WORDS[COMP_CWORD-1]};
    cur=${COMP_WORDS[COMP_CWORD]};
    conns=$(\ls --color=none /etc/ppp/peers | egrep -v '(\.bak|~)$');
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($(compgen -W '-r -d -c -a -h -v' -- $cur));
        return 0;
    fi;
    if [ $COMP_CWORD -eq 1 ] && [[ "$cur" != -* ]] || [[ "$prev" == -* ]]; then
        COMPREPLY=($(compgen -o filenames -W "$conns" $cur));
    fi;
    return 0
}
_pon () 
{ 
    local cur conns;
    [ -r /etc/ppp/peers/ ] || return 0;
    COMPREPLY=();
    cur=${COMP_WORDS[COMP_CWORD]};
    conns=$(\ls --color=none /etc/ppp/peers | egrep -v '(\.bak|~)$');
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($(compgen -o filenames -W "$conns" $cur));
    fi;
    return 0
}
_pwck () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-q -r -s' -- "$cur" ));
        return 0;
    fi;
    _filedir
}
_python () 
{ 
    local prev cur i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -Q)
            COMPREPLY=($( compgen -W "old new warn warnall" -- "$cur" ));
            return 0
        ;;
        -W)
            COMPREPLY=($( compgen -W "ignore default all module once error"             -- "$cur" ));
            return 0
        ;;
        -c)
            _filedir '@(py|pyc|pyo)';
            return 0
        ;;
        !(python|-?))
            [[ ${COMP_WORDS[COMP_CWORD-2]} != -@(Q|W) ]] && _filedir
        ;;
    esac;
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        if [[ ${COMP_WORDS[i]} == -c ]]; then
            _filedir;
        fi;
    done;
    if [[ "$cur" != -* ]]; then
        _filedir '@(py|pyc|pyo)';
    else
        COMPREPLY=($( compgen -W "- -d -E -h -i -O -Q -S -t -u             -U -v -V -W -x -c" -- "$cur" ));
    fi;
    return 0
}
_querybts () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -B | --bts)
            COMPREPLY=($( compgen -W "debian guug kde mandrake help"                 -- "$cur" ));
            return 0
        ;;
        -u | --ui | --interface)
            COMPREPLY=($( compgen -W "newt text gnome" -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    COMPREPLY=($( compgen -W '-h --help -v --version -A --archive \
        -B --bts -l --ldap --no-ldap --proxy --http_proxy \
        -s --source -w --web -u --ui --interface \
        wnpp boot-floppies kernel bugs.debian.org \
        cdimage.debian.org general installation-reports \
        listarchives lists.debian.org mirrors nm.debian.org \
        press project qa.debian.org release-notes \
        security.debian.org tech-ctte upgrade-reports \
        www.debian.org' -- "$cur" ) $( apt-cache pkgnames -- "$cur" 2> /dev/null))
}
_quote_readline_by_ref () 
{ 
    if [[ ${1:0:1} == "'" ]]; then
        printf -v $2 %q "${1:1}";
        if [[ ${BASH_VERSINFO[0]} -le 3 ]]; then
            printf -v $2 %q ${!2};
        fi;
    else
        if [[ ${BASH_VERSINFO[0]} -le 3 && ${1:0:1} == '"' ]]; then
            printf -v $2 %q "${1:1}";
        else
            printf -v $2 %q "$1";
        fi;
    fi;
    [[ ${!2:0:1} == '$' ]] && eval $2=${!2}
}
_rdesktop () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -k)
            COMPREPLY=($( command ls                 /usr/share/rdesktop/keymaps 2>/dev/null |                 command grep -E -v '(common|modifiers)' ));
            COMPREPLY=(${COMPREPLY[@]:-} $( command ls                 $HOME/.rdesktop/keymaps 2>/dev/null ));
            COMPREPLY=(${COMPREPLY[@]:-} $( command ls                 ./keymaps 2>/dev/null ));
            COMPREPLY=($( compgen -W '${COMPREPLY[@]}' -- "$cur" ));
            return 0
        ;;
        -a)
            COMPREPLY=($( compgen -W '8 15 16 24' -- "$cur" ));
            return 0
        ;;
        -x)
            COMPREPLY=($( compgen -W 'b broadband m modem l lan'                 -- $cur ));
            return 0
        ;;
        -r)
            COMPREPLY=($( compgen -W 'comport: disk: lptport: \
                printer: sound: lspci scard' -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-u -d -s -c -p -n -k -g -f -b -L \
            -A -B -e -E -m -C -D -K -S -T -N -X -a -z -x -P -r \
            -0 -4 -5' -- "$cur" ));
    else
        _known_hosts_real "$cur";
    fi
}
_realcommand () 
{ 
    type -P "$1" > /dev/null && { 
        if type -p realpath > /dev/null; then
            realpath "$(type -P "$1")";
        else
            if type -p readlink > /dev/null; then
                readlink -f "$(type -P "$1")";
            else
                type -P "$1";
            fi;
        fi
    }
}
_renice () 
{ 
    local command cur curopt i;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    command=$1;
    i=0;
    while [[ $i -le $COMP_CWORD && ${#COMPREPLY[@]} -eq 0 ]]; do
        curopt=${COMP_WORDS[COMP_CWORD-$i]};
        case "$curopt" in 
            -u)
                COMPREPLY=($( compgen -u -- "$cur" ))
            ;;
            -g)
                _pgids
            ;;
            -p | $command)
                _pids
            ;;
        esac;
        i=$(( ++i ));
    done
}
_reply_compgen_array () 
{ 
    local i wlist;
    for i in ${!COMPREPLY[*]};
    do
        local q=$(quote "$(printf %q "${COMPREPLY[$i]}")");
        wlist+=$q'
';
    done;
    local ecur="$cur";
    ecur="${ecur//\\/\\\\}";
    ecur="${ecur//\'/\'}";
    local oldifs=$IFS;
    IFS='
' eval 'COMPREPLY=(`compgen -W "$wlist" -- "${ecur}"`)';
    IFS=$oldifs
}
_reportbug () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -f | --filename | -i | --include | --mta | -o | --output)
            _filedir;
            return 0
        ;;
        -B | --bts)
            COMPREPLY=($( compgen -W "debian guug kde mandrake help" --                 "$cur" ));
            return 0
        ;;
        -e | --editor | --mua)
            COMP_WORDS=(COMP_WORDS[0] "$cur");
            COMP_CWORD=1;
            _command;
            return 0
        ;;
        --mode)
            COMPREPLY=($( compgen -W "novice standard expert" -- "$cur" ));
            return 0
        ;;
        -S | --severity)
            COMPREPLY=($( compgen -W "grave serious important normal                 minor wishlist" -- "$cur" ));
            return 0
        ;;
        -u | --ui | --interface)
            COMPREPLY=($( compgen -W "newt text gnome" -- "$cur" ));
            return 0
        ;;
        -t | --type)
            COMPREPLY=($( compgen -W "gnats debbugs" -- "$cur" ));
            return 0
        ;;
        -T | --tag)
            COMPREPLY=($( compgen -W "none                 woody potato sarge sarge-ignore etch etch-ignore                 lenny lenny-ignore sid experimental confirmed                 d-i fixed fixed-in-experimental fixed-upstream                 help l10n moreinfo patch pending security                 unreproducible upstream wontfix ipv6 lfs" -- "$cur" ));
            return 0
        ;;
        --from-buildd)
            COMPREPLY=($( compgen -S "_" -W '$( apt-cache dumpavail | \
                command grep "^Source: $cur" | sort -u | cut -f2 -d" " )' ));
            return 0
        ;;
        *)

        ;;
    esac;
    COMPREPLY=($( compgen -W '--help --version --attach \
        --no-query-bts --query-bts --bts --body --body-file --bodyfile \
        --no-config-files --class --configure --check-available --debug \
        --no-check-available --debconf --test --draftpath --editor --email \
        --exit-prompt --filename --from-buildd --gnupg --gpg --path --gnus \
        --header --include --no-check-installed --check-installed \
        --justification --kudos --keyid --license --list-cc \
        --maintonly --mirror --mode --mua --mta --mutt --mh --nmh \
        --bugnumber --no-bug-script --no-cc-menu --output --offline \
        --print --paranoid --no-paranoid --pgp --proxy --http_proxy \
        --pseudo-header --quiet --query-only --query-source --no-query-source \
        --realname --report-quiet --reply-to --replyto --subject --severity \
        --smtphost --timeout --tls --smtpuser --smtppasswd --src --source --type \
        --tag --template --verify --no-verify --no-cc --package-version \
        --no-compress --ui --interface \
        wnpp boot-floppies kernel bugs.debian.org \
        cdimage.debian.org general installation-reports \
        listarchives lists.debian.org mirrors nm.debian.org \
        press project qa.debian.org release-notes \
        security.debian.org tech-ctte upgrade-reports \
        www.debian.org' -- "$cur" ) $( apt-cache pkgnames -- "$cur" 2> /dev/null));
    _filedir;
    return 0
}
_rfcomm () 
{ 
    local cur prev;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    case $prev in 
        -@(f|-config))
            _filedir;
            return 0
        ;;
        -i)
            _bluetooth_devices;
            _bluetooth_adresses;
            return 0
        ;;
    esac;
    _get_command;
    if [ -z $command ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-h --help -a -r --raw -f \
                --config -i -A --auth -E --encrypt -S --secure \
                -M --master' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'show connect listen watch \
                bind release' -- "$cur" ));
        fi;
    else
        _count_args;
        if [ $args -eq 2 ]; then
            _bluetooth_devices;
        else
            case $command in 
                @(connect|bind))
                    if [ $args -eq 3 ]; then
                        _bluetooth_adresses;
                    fi
                ;;
            esac;
        fi;
    fi
}
_rl_enabled () 
{ 
    [[ "$( bind -v )" = *$1+([[:space:]])on* ]]
}
_rmmod () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _installed_modules "$cur";
    return 0
}
_root_command () 
{ 
    local PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin;
    _command $1 $2 $3
}
_route () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    if [ "$prev" = dev ]; then
        _available_interfaces;
        return 0;
    fi;
    local i found;
    for opt in add del -host -net netmask metric mss window irtt reject mod dyn reinstate dev default gw;
    do
        found=false;
        for ((i=1; i < ${#COMP_WORDS[@]}-1; i++ ))
        do
            [ "${COMP_WORDS[i]}" = "$opt" ] && found=true && break;
        done;
        $found || COMPREPLY[${#COMPREPLY[@]}]="$opt";
    done;
    COMPREPLY=($( compgen -W '"${COMPREPLY[@]}"' -- "$cur" ))
}
_rpcdebug () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -s)
            _rpcdebug_flags;
            return 0
        ;;
        -c)
            _rpcdebug_flags;
            return 0
        ;;
        -m)
            COMPREPLY=($( compgen -W 'rpc nfs nfsd nlm' -- "$cur" ));
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-v -h -m -s -c' -- "$cur" ));
    fi
}
_rpcdebug_flags () 
{ 
    local i module;
    for ((i=0; i < ${#COMP_WORDS[@]}; i++ ))
    do
        if [[ ${COMP_WORDS[i]} == -m ]]; then
            module=${COMP_WORDS[i+1]};
        fi;
    done;
    if [ -n "$module" ]; then
        COMPREPLY=($( compgen -W "$(rpcdebug -vh 2>&1             | command grep '^'$module' '            | awk '{$1 = ""; print $0}')" -- "$cur" ));
    fi
}
_rsync () 
{ 
    COMPREPLY=();
    local cur prev;
    _get_comp_words_by_ref -n : cur prev;
    _expand || return 0;
    case $prev in 
        --config | --password-file | --include-from | --exclude-from)
            _filedir;
            return 0
        ;;
        -T | --temp-dir | --compare-dest)
            _filedir -d;
            return 0
        ;;
        -e | --rsh)
            COMPREPLY=($( compgen -W 'rsh ssh' -- "$cur" ));
            return 0
        ;;
        --compress-level)
            COMPREPLY=($( compgen -W '1 2 3 4 5 6 7 8 9' -- "$cur" ));
            return 0
        ;;
    esac;
    case $cur in 
        -*)
            COMPREPLY=($( compgen -W '--verbose --quiet --no-motd --checksum \
                --archive --recursive --relative --no-implied-dirs \
                --backup --backup-dir --suffix= --update --inplace --append \
                --append-verify --dirs --old-dirs --links --copy-links \
                --copy-unsafe-links --safe-links --copy-dirlinks \
                --keep-dirlinks --hard-links --perms --executability --chmod= \
                --acls --xattrs --owner --group --devices --copy-devices \
                --specials --times --omit-dir-times --super --fake-super \
                --sparse --dry-run --whole-file --no-whole-file \
                --one-file-system --block-size= --rsh= --rsync-path= \
                --existing --ignore-existing --remove-source-files --delete \
                --delete-before --delete-during --delete-delay --delete-after \
                --delete-excluded --ignore-errors --force --max-delete= \
                --max-size= --min-size= --partial --partial-dir= \
                --delay-updates --prune-empty-dirs --numeric-ids --timeout= \
                --contimeout= --ignore-times --size-only --modify-window= \
                --temp-dir= --fuzzy --compare-dest= --copy-dest= --link-dest= \
                --compress --compress-level= --skip-compress= --cvs-exclude \
                --filter= --exclude= --exclude-from= --include= \
                --include-from= --files-from= --from0 --protect-args \
                --address= --port= --sockopts= --blocking-io --no-blocking-io \
                --stats --8-bit-output --human-readable --progress \
                --itemize-changes --out-format= --log-file= \
                --log-file-format= --password-file= --list-only --bwlimit= \
                --write-batch= --only-write-batch= --read-batch= --protocol= \
                --iconv= --ipv4 --ipv6 --version --help --daemon --config= \
                --no-detach' -- "$cur" ))
        ;;
        *:*)
            if type _scp_remote_files &>/dev/null; then
                local i shell=ssh;
                for ((i=1; i < COMP_CWORD; i++ ))
                do
                    if [[ "${COMP_WORDS[i]}" == -@(e|-rsh) ]]; then
                        shell=${COMP_WORDS[i+1]};
                        break;
                    fi;
                done;
                [ "$shell" = ssh ] && _scp_remote_files;
            fi
        ;;
        *)
            _known_hosts_real -c -a "$cur";
            type _scp_local_files &>/dev/null && _scp_local_files || _filedir
        ;;
    esac;
    return 0
}
_rtcwake () 
{ 
    COMPREPLY=();
    local cur prev split=false;
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case "$prev" in 
        --help | -h | --version | -V | --seconds | -s | --time | -t)
            return 0
        ;;
        --mode | -m)
            COMPREPLY=($( compgen -W 'standby mem disk on no off' -- "$cur" ));
            return 0
        ;;
        --device | -d)
            COMPREPLY=($( command ls -d /dev/rtc?* 2>/dev/null ));
            COMPREPLY=($( compgen -W '${COMPREPLY[@]#/dev/}' -- "$cur" ));
            return 0
        ;;
    esac;
    $split && return 0;
    COMPREPLY=($( compgen -W '--device --local --mode --seconds --time --utc \
        --verbose --version --help' -- "$cur" ))
}
_scp () 
{ 
    local configfile cur prev prefix;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    _ssh_suboption_check && { 
        COMPREPLY=("${COMPREPLY[@]/%/ }");
        return 0
    };
    case $prev in 
        -l | -P)
            return 0
        ;;
        -F | -i | -S)
            _filedir;
            type compopt &>/dev/null && compopt +o nospace;
            return 0
        ;;
        -c)
            _ssh_ciphers;
            COMPREPLY=("${COMPREPLY[@]/%/ }");
            return 0
        ;;
        -o)
            _ssh_options;
            return 0
        ;;
    esac;
    _expand || return 0;
    if [[ "$cur" == *:* ]]; then
        _scp_remote_files;
        return 0;
    fi;
    if [[ "$cur" == -F* ]]; then
        cur=${cur#-F};
        prefix=-F;
    else
        set -- "${COMP_WORDS[@]}";
        while [ $# -gt 0 ]; do
            if [ "${1:0:2}" = -F ]; then
                if [ ${#1} -gt 2 ]; then
                    configfile="$(dequote "${1:2}")";
                else
                    shift;
                    [ "$1" ] && configfile="$(dequote "$1")";
                fi;
                break;
            fi;
            shift;
        done;
        case $cur in 
            -*)
                COMPREPLY=($( compgen -W '-1 -2 -4 -6 -B -C -c -F -i -l -o \
                    -P -p -q -r -S -v' -- "$cur" ));
                COMPREPLY=("${COMPREPLY[@]/%/ }");
                return 0
            ;;
            */*)

            ;;
            *)
                _known_hosts_real -c -a -F "$configfile" "$cur"
            ;;
        esac;
    fi;
    _scp_local_files "$prefix";
    return 0
}
_scp_local_files () 
{ 
    local IFS='	
';
    local dirsonly=false;
    if [ "$1" = -d ]; then
        dirsonly=true;
        shift;
    fi;
    if $dirsonly; then
        COMPREPLY=("${COMPREPLY[@]}" $( command ls -aF1d $cur* 2>/dev/null |             sed -e "s/$_scp_path_esc/\\\\&/g" -e '/[^\/]$/d' -e "s/^/$1/"));
    else
        COMPREPLY=("${COMPREPLY[@]}" $( command ls -aF1d $cur* 2>/dev/null |             sed -e "s/$_scp_path_esc/\\\\&/g" -e 's/[*@|=]$//g'             -e 's/[^\/]$/& /g' -e "s/^/$1/"));
    fi
}
_scp_remote_files () 
{ 
    local IFS='	
';
    cur=${cur/\\:/:};
    local userhost=${cur%%?(\\):*};
    local path=${cur#*:};
    path=$( sed -e 's/\\\\\\\('$_scp_path_esc'\)/\\\1/g' <<<"$path" );
    if [ -z "$path" ]; then
        path=$(ssh -o 'Batchmode yes' $userhost pwd 2>/dev/null);
    fi;
    local files;
    if [ "$1" = -d ]; then
        files=$( ssh -o 'Batchmode yes' $userhost             command ls -aF1d "$path*" 2>/dev/null |             sed -e 's/'$_scp_path_esc'/\\\\\\&/g' -e '/[^\/]$/d' );
    else
        files=$( ssh -o 'Batchmode yes' $userhost             command ls -aF1d "$path*" 2>/dev/null |             sed -e 's/'$_scp_path_esc'/\\\\\\&/g' -e 's/[*@|=]$//g'             -e 's/[^\/]$/& /g' );
    fi;
    COMPREPLY=("${COMPREPLY[@]}" $files)
}
_sdptool () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    cur=`_get_cword`;
    prev=${COMP_WORDS[COMP_CWORD-1]};
    _split_longopt && split=true;
    case $prev in 
        --bdaddr)
            _bluetooth_adresses;
            return 0
        ;;
    esac;
    $split && return 0;
    _get_command;
    if [ -z $command ]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '--help' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W 'search browse records add \
                del get setattr setseq' -- "$cur" ));
        fi;
    else
        case $command in 
            search)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '--bdaddr \
                        --tree --raw --xml' -- "$cur" ));
                else
                    _bluetooth_services;
                fi
            ;;
            @(browse|records))
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '--tree --raw --xml' -- "$cur" ));
                else
                    _bluetooth_adresses;
                fi
            ;;
            add)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '--handle --channel' -- "$cur" ));
                else
                    _bluetooth_services;
                fi
            ;;
            get)
                if [[ "$cur" == -* ]]; then
                    COMPREPLY=($( compgen -W '--bdaddr \
                        --tree --raw --xml' -- "$cur" ));
                fi
            ;;
        esac;
    fi
}
_service () 
{ 
    local cur prev sysvdir;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    [[ ${COMP_WORDS[0]} != @(*init.d/!(functions|~)|service) ]] && return 0;
    [ $COMP_CWORD -gt 2 ] && return 0;
    [ -d /etc/rc.d/init.d ] && sysvdir=/etc/rc.d/init.d || sysvdir=/etc/init.d;
    if [[ $COMP_CWORD -eq 1 ]] && [[ $prev == "service" ]]; then
        _services;
    else
        COMPREPLY=($( compgen -W '`sed -e "y/|/ /" \
            -ne "s/^.*\(U\|msg_u\)sage.*{\(.*\)}.*$/\2/p" \
            $sysvdir/${prev##*/} 2>/dev/null`' -- "$cur" ));
    fi;
    return 0
}
_services () 
{ 
    local sysvdir famdir;
    [ -d /etc/rc.d/init.d ] && sysvdir=/etc/rc.d/init.d || sysvdir=/etc/init.d;
    famdir=/etc/xinetd.d;
    COMPREPLY=($( printf '%s\n'         $sysvdir/!(*.rpm@(orig|new|save)|*~|functions) ));
    if [ -d $famdir ]; then
        COMPREPLY=("${COMPREPLY[@]}" $( printf '%s\n'             $famdir/!(*.rpm@(orig|new|save)|*~) ));
    fi;
    COMPREPLY=($( compgen -W '${COMPREPLY[@]#@($sysvdir|$famdir)/}' -- "$cur" ))
}
_sftp () 
{ 
    local cur prev configfile;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _ssh_suboption_check && return 0;
    case $prev in 
        -b | -F | -P)
            _filedir;
            return 0
        ;;
        -o)
            _ssh_options;
            return 0
        ;;
    esac;
    if [[ "$cur" == -F* ]]; then
        cur=${cur#-F};
        _filedir;
        COMPREPLY=("${COMPREPLY[@]/#/-F}");
        cur=-F$cur;
    else
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-1 -C -v -B -b -F -o -P -R -S -s'             -- "$cur" ));
        else
            set -- "${COMP_WORDS[@]}";
            while [ $# -gt 0 ]; do
                if [ "${1:0:2}" = -F ]; then
                    if [ ${#1} -gt 2 ]; then
                        configfile="$(dequote "${1:2}")";
                    else
                        shift;
                        [ "$1" ] && configfile="$(dequote "$1")";
                    fi;
                    break;
                fi;
                shift;
            done;
            _known_hosts_real -a -F "$configfile" "$cur";
        fi;
    fi;
    return 0
}
_shells () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W         '$( command grep "^[[:space:]]*/" /etc/shells 2>/dev/null )'         -- "$cur" ))
}
_signals () 
{ 
    local i;
    COMPREPLY=($( compgen -A signal SIG${cur#-} ));
    for ((i=0; i < ${#COMPREPLY[@]}; i++ ))
    do
        COMPREPLY[i]=-${COMPREPLY[i]#SIG};
    done
}
_split_longopt () 
{ 
    if [[ "$cur" == --?*=* ]]; then
        prev="${cur%%?(\\)=*}";
        cur="${cur#*=}";
        return 0;
    fi;
    return 1
}
_ssh () 
{ 
    local cur prev configfile;
    local -a config;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur prev;
    _ssh_suboption_check && return 0;
    case $prev in 
        -F | -i | -S)
            _filedir;
            return 0
        ;;
        -c)
            _ssh_ciphers;
            return 0
        ;;
        -m)
            _ssh_macs;
            return 0
        ;;
        -l)
            COMPREPLY=($( compgen -u -- "$cur" ));
            return 0
        ;;
        -o)
            _ssh_options;
            return 0
        ;;
        -w)
            _available_interfaces;
            return 0
        ;;
        -b)
            _ssh_bindaddress;
            return 0
        ;;
    esac;
    if [[ "$cur" == -F* ]]; then
        cur=${cur#-F};
        _filedir;
        COMPREPLY=("${COMPREPLY[@]/#/-F}");
        cur=-F$cur;
    else
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '-1 -2 -4 -6 -A -a -C -f -g -K -k -M \
            -N -n -q -s -T -t -V -v -X -v -Y -y -b -b -c -D -e -F \
            -i -L -l -m -O -o -p -R -S -w' -- "$cur" ));
        else
            set -- "${COMP_WORDS[@]}";
            while [ $# -gt 0 ]; do
                if [ "${1:0:2}" = -F ]; then
                    if [ ${#1} -gt 2 ]; then
                        configfile="$(dequote "${1:2}")";
                    else
                        shift;
                        [ "$1" ] && configfile="$(dequote "$1")";
                    fi;
                    break;
                fi;
                shift;
            done;
            _known_hosts_real -a -F "$configfile" "$cur";
            if [ $COMP_CWORD -ne 1 ]; then
                COMPREPLY=("${COMPREPLY[@]}" $( compgen -c -- "$cur" ));
            fi;
        fi;
    fi;
    return 0
}
_ssh_bindaddress () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W         "$( PATH="$PATH:/sbin" ifconfig -a |         sed -ne 's/.*addr:\([^[:space:]]*\).*/\1/p'             -ne 's/.*inet[[:space:]]\{1,\}\([^[:space:]]*\).*/\1/p' )"         -- "$cur" ))
}
_ssh_ciphers () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W '3des-cbc aes128-cbc \
        aes192-cbc aes256-cbc aes128-ctr aes192-ctr aes256-ctr arcfour128 \
        arcfour256 arcfour blowfish-cbc cast128-cbc' -- "$cur" ))
}
_ssh_copy_id () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -i)
            _filedir;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-i' -- "$cur" ));
    else
        _known_hosts_real -a "$cur";
    fi;
    return 0
}
_ssh_macs () 
{ 
    COMPREPLY=("${COMPREPLY[@]}" $( compgen -W 'hmac-md5 hmac-sha1 \
        umac-64@openssh.com hmac-ripemd160 hmac-sha1-96 hmac-md5-96'         -- "$cur" ))
}
_ssh_options () 
{ 
    type compopt &>/dev/null && compopt -o nospace;
    COMPREPLY=($( compgen -S = -W 'AddressFamily BatchMode BindAddress \
        ChallengeResponseAuthentication CheckHostIP Cipher Ciphers \
        ClearAllForwardings Compression CompressionLevel ConnectionAttempts \
        ConnectTimeout ControlMaster ControlPath DynamicForward EscapeChar \
        ExitOnForwardFailure ForwardAgent ForwardX11 ForwardX11Trusted \
        GatewayPorts GlobalKnownHostsFile GSSAPIAuthentication \
        GSSAPIDelegateCredentials HashKnownHosts Host HostbasedAuthentication \
        HostKeyAlgorithms HostKeyAlias HostName IdentityFile IdentitiesOnly \
        KbdInteractiveDevices LocalCommand LocalForward LogLevel MACs \
        NoHostAuthenticationForLocalhost NumberOfPasswordPrompts \
        PasswordAuthentication PermitLocalCommand Port \
        PreferredAuthentications Protocol ProxyCommand PubkeyAuthentication \
        RekeyLimit RemoteForward RhostsRSAAuthentication RSAAuthentication \
        SendEnv ServerAliveInterval ServerAliveCountMax SmartcardDevice \
        StrictHostKeyChecking TCPKeepAlive Tunnel TunnelDevice \
        UsePrivilegedPort User UserKnownHostsFile VerifyHostKeyDNS \
        VisualHostKey XAuthLocation' -- "$cur" ))
}
_ssh_suboption () 
{ 
    local prev=${1%%=*} cur=${1#*=};
    case $prev in 
        BatchMode | ChallengeResponseAuthentication | CheckHostIP | ClearAllForwardings | Compression | ExitOnForwardFailure | ForwardAgent | ForwardX11 | ForwardX11Trusted | GatewayPorts | GSSAPIAuthentication | GSSAPIKeyExchange | GSSAPIDelegateCredentials | GSSAPITrustDns | HashKnownHosts | HostbasedAuthentication | IdentitiesOnly | KbdInteractiveAuthentication | KbdInteractiveDevices | NoHostAuthenticationForLocalhost | PasswordAuthentication | PubkeyAuthentication | RhostsRSAAuthentication | RSAAuthentication | StrictHostKeyChecking | TCPKeepAlive | UsePrivilegedPort | VerifyHostKeyDNS | VisualHostKey)
            COMPREPLY=($( compgen -W 'yes no' -- "$cur"))
        ;;
        AddressFamily)
            COMPREPLY=($( compgen -W 'any inet inet6' -- "$cur" ))
        ;;
        BindAddress)
            _ssh_bindaddress
        ;;
        Cipher)
            COMPREPLY=($( compgen -W 'blowfish des 3des' -- "$cur" ))
        ;;
        Protocol)
            COMPREPLY=($( compgen -W '1 2 1,2 2,1' -- "$cur" ))
        ;;
        Tunnel)
            COMPREPLY=($( compgen -W 'yes no point-to-point ethernet'                     -- "$cur" ))
        ;;
        PreferredAuthentications)
            COMPREPLY=($( compgen -W 'gssapi-with-mic host-based \
                    publickey keyboard-interactive password' -- "$cur" ))
        ;;
        MACs)
            _ssh_macs
        ;;
        Ciphers)
            _ssh_ciphers
        ;;
    esac;
    return 0
}
_ssh_suboption_check () 
{ 
    local cureq=`_get_cword :=` preveq=`_get_pword :=`;
    if [[ $cureq == *=* && $preveq == -o ]]; then
        _ssh_suboption $cureq;
        return $?;
    fi;
    return 1
}
_stream () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    _ImageMagick;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-authenticate -channel -colorspace \
            -compress -debug -define -density -depth -extract \
            -help -identify -interlace -interpolate -limit -list \
            -log -map -monitor -quantize -quiet -regard-warnings \
            -respect-parenthesis -sampling-factor -seed -set \
            -size -storage-type -transparent-color -verbose \
            -version -virtual-pixel' -- "$cur" ));
    else
        if [[ "$cur" == +* ]]; then
            COMPREPLY=($( compgen -W '+debug' -- "$cur" ));
        else
            _filedir;
        fi;
    fi
}
_sysctl () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    COMPREPLY=($( compgen -W "$(sysctl -N -a 2>/dev/null)" -- "$cur" ));
    return 0
}
_tar () 
{ 
    local cur ext regex tar untar;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=($( compgen -W 'c t x u r d A' -- "$cur" ));
        return 0;
    fi;
    case ${COMP_WORDS[1]} in 
        ?(-)[cr]*f)
            _filedir;
            return 0
        ;;
        +([^IZzJjy])f)
            ext='t@(ar?(.@(Z|gz|bz?(2)|lz?(ma)|xz))|gz|bz?(2)|lz?(ma)|xz)';
            regex='t\(ar\(\.\(Z\|gz\|bz2\?\|lzma\|xz\)\)\?\|gz\|bz2\?\|lzma\|xz\)'
        ;;
        *[Zz]*f)
            ext='t?(ar.)@(gz|Z)';
            regex='t\(ar\.\)\?\(gz\|Z\)'
        ;;
        *[Ijy]*f)
            ext='t?(ar.)bz?(2)';
            regex='t\(ar\.\)\?bz2\?'
        ;;
        *[J]*f)
            ext='t?(ar.)@(lz?(ma)|xz)';
            regex='t\(ar\.\)\?\(lzma\|xz\)\?'
        ;;
        *)
            _filedir;
            return 0
        ;;
    esac;
    if [[ "$COMP_LINE" == *$ext' ' ]]; then
        tar=$( sed -e 's/^.* \([^ ]*'$regex'\) .*$/\1/' <<<"$COMP_LINE" );
        untar=t${COMP_WORDS[1]//[^Izjyf]/};
        COMPREPLY=($( compgen -W "$( printf '%s ' $( tar $untar $tar             2>/dev/null ) )" -- "$cur" ));
        return 0;
    fi;
    _filedir "$ext";
    return 0
}
_uids () 
{ 
    if type getent &>/dev/null; then
        COMPREPLY=($( compgen -W '$( getent passwd | cut -d: -f3 )' -- "$cur" ));
    else
        if type perl &>/dev/null; then
            COMPREPLY=($( compgen -W '$( perl -e '"'"'while (($uid) = (getpwent)[2]) { print $uid . "\n" }'"'"' )' -- "$cur" ));
        else
            COMPREPLY=($( compgen -W '$( cut -d: -f3 /etc/passwd )' -- "$cur" ));
        fi;
    fi
}
_umount () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    COMPREPLY=();
    if [[ $(uname -s) = Linux && -r /proc/mounts ]]; then
        _linux_fstab < /proc/mounts;
    else
        local IFS='
';
        COMPREPLY=($( compgen -W '$( mount | cut -d" " -f 3 )' -- "$cur" ));
    fi;
    return 0
}
_unpack200 () 
{ 
    COMPREPLY=();
    local cur prev;
    _get_comp_words_by_ref cur prev;
    case $prev in 
        '-?' | -h | --help | -V | --version | -J)
            return 0
        ;;
        -H | --deflate-hint)
            COMPREPLY=($( compgen -W 'true false keep' -- "$cur" ));
            return 0
        ;;
        -l | --log-file)
            COMPREPLY=($( compgen -W '-' -- "$cur" ));
            _filedir log;
            return 0
        ;;
    esac;
    local i pack=false jar=false;
    for ((i=0; i < ${#COMP_WORDS[@]}-1; i++ ))
    do
        case ${COMP_WORDS[i]} in 
            *.pack | *.pack.gz)
                pack=true
            ;;
            *.jar)
                jar=true
            ;;
        esac;
    done;
    if ! $pack; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=($( compgen -W '--deflate-hint= --remove-pack-file \
                --verbose --quiet --log-file= --help --version' -- "$cur" ));
            [[ ${#COMPREPLY[@]} -eq 1 && ${COMPREPLY[0]} == *= ]] && type compopt &>/dev/null && compopt -o nospace;
        else
            _filedir 'pack?(.gz)';
        fi;
    else
        if ! $jar; then
            _filedir jar;
        fi;
    fi
}
_unrar () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '-ad -ap -av- -c- -cfg- -cl -cu \
            -dh -ep -f -idp -ierr -inul -kb -o+ -o- -ow -p -p- -r -ta \
            -tb -tn -to -u -v -ver -vp -x -x@ -y' -- "$cur" ));
    else
        if [ $COMP_CWORD -eq 1 ]; then
            COMPREPLY=($( compgen -W 'e l lb lt p t v vb vt x' -- "$cur" ));
        else
            _filedir '@(rar|RAR)';
        fi;
    fi
}
_update_alternatives () 
{ 
    local cur prev mode args i;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        --altdir | --admindir)
            _filedir -d;
            return 0
        ;;
        --help | --usage | --version)
            return 0
        ;;
    esac;
    for ((i=1; i < COMP_CWORD; i++ ))
    do
        if [[ "${COMP_WORDS[i]}" == --@(install|remove|auto|display|config|remove-all|set) ]]; then
            mode=${COMP_WORDS[i]};
            args=$(($COMP_CWORD - i));
            break;
        fi;
    done;
    case $mode in 
        --install)
            case $args in 
                1 | 3)
                    _filedir
                ;;
                2)
                    _installed_alternatives
                ;;
                4)

                ;;
                *)
                    case $((args % 4)) in 
                        0 | 2)
                            _filedir
                        ;;
                        1)
                            COMPREPLY=($( compgen -W '--slave' -- "$cur" ))
                        ;;
                        3)
                            _installed_alternatives
                        ;;
                    esac
                ;;
            esac
        ;;
        --remove | --set)
            case $args in 
                1)
                    _installed_alternatives
                ;;
                2)
                    _filedir
                ;;
            esac
        ;;
        --auto | --remove-all | --display | --config)
            _installed_alternatives
        ;;
        *)
            COMPREPLY=($( compgen -W '--verbose --quiet --help --version \
                --altdir --admindir --install --remove --auto --display \
                --config --set' -- "$cur" ))
        ;;
    esac
}
_update_initramfs () 
{ 
    local cur prev valid_options;
    cur=$(_get_cword);
    prev=${COMP_WORDS[COMP_CWORD-1]};
    if [[ "$prev" == '-k' ]]; then
        _kernel_versions;
        COMPREPLY=($( compgen -W '${COMPREPLY[@]} all' -- "$cur" ));
        return;
    fi;
    valid_options=$( update-initramfs -h 2>&1 | 		sed -e '/^ -/!d;s/^ \(-\w\+\).*/\1/' );
    COMPREPLY=($( compgen -W "$valid_options" -- $cur ))
}
_update_rc_d () 
{ 
    local cur prev sysvdir services options valid_options;
    _get_comp_words_by_ref cur prev;
    [ -d /etc/rc.d/init.d ] && sysvdir=/etc/rc.d/init.d || sysvdir=/etc/init.d;
    services=($(printf '%s ' $sysvdir/!(README*|*.sh|*.dpkg*|*.rpm@(orig|new|save))));
    services=(${services[@]#$sysvdir/});
    options=(-f -n);
    if [[ $COMP_CWORD -eq 1 || "$prev" == -* ]]; then
        valid_options=($(         tr " " "\n" <<<"${COMP_WORDS[@]} ${options[@]}"         | sed -ne "/$( sed "s/ /\\|/g" <<<"${options[@]}" )/p"         | sort | uniq -u         ));
        COMPREPLY=($( compgen -W '${options[@]} ${services[@]}'         -X '$( tr " " "|" <<<${COMP_WORDS[@]} )' -- "$cur" ));
    else
        if [[ "$prev" == ?($( tr " " "|" <<<${services[@]} )) ]]; then
            COMPREPLY=($( compgen -W 'remove defaults start stop' -- "$cur" ));
        else
            if [[ "$prev" == defaults && "$cur" == [0-9] ]]; then
                COMPREPLY=(0 1 2 3 4 5 6 7 8 9);
            else
                if [[ "$prev" == defaults && "$cur" == [sk]?([0-9]) ]]; then
                    COMPREPLY=(0 1 2 3 4 5 6 7 8 9);
                else
                    if [[ "$prev" == defaults && -z "$cur" ]]; then
                        COMPREPLY=(0 1 2 3 4 5 6 7 8 9 s k);
                    else
                        if [[ "$prev" == ?(start|stop) ]]; then
                            if [[ "$cur" == [0-9] || -z "$cur" ]]; then
                                COMPREPLY=(0 1 2 3 4 5 6 7 8 9);
                            else
                                if [[ "$cur" == [0-9][0-9] ]]; then
                                    COMPREPLY=($cur);
                                else
                                    COMPREPLY=();
                                fi;
                            fi;
                        else
                            if [[ "$prev" == ?([0-9][0-9]|[0-6S]) ]]; then
                                if [[ -z "$cur" ]]; then
                                    if [[ $prev == [0-9][0-9] ]]; then
                                        COMPREPLY=(0 1 2 3 4 5 6 S);
                                    else
                                        COMPREPLY=(0 1 2 3 4 5 6 S .);
                                    fi;
                                else
                                    if [[ "$cur" == [0-6S.] ]]; then
                                        COMPREPLY=($cur);
                                    else
                                        COMPREPLY=();
                                    fi;
                                fi;
                            else
                                if [[ "$prev" == "." ]]; then
                                    COMPREPLY=($(compgen -W "start stop" -- "$cur"));
                                else
                                    COMPREPLY=();
                                fi;
                            fi;
                        fi;
                    fi;
                fi;
            fi;
        fi;
    fi;
    return 0
}
_upvar () 
{ 
    if unset -v "$1"; then
        if (( $# == 2 )); then
            eval $1=\"\$2\";
        else
            eval $1=\(\"\${@:2}\"\);
        fi;
    fi
}
_upvars () 
{ 
    if ! (( $# )); then
        echo "${FUNCNAME[0]}: usage: ${FUNCNAME[0]} [-v varname" "value] | [-aN varname [value ...]] ..." 1>&2;
        return 2;
    fi;
    while (( $# )); do
        case $1 in 
            -a*)
                [[ -n ${1#-a} ]] || { 
                    echo "bash: ${FUNCNAME[0]}: \`$1': missing" "number specifier" 1>&2;
                    return 1
                };
                printf %d "${1#-a}" &>/dev/null || { 
                    echo "bash:" "${FUNCNAME[0]}: \`$1': invalid number specifier" 1>&2;
                    return 1
                };
                [[ -n "$2" ]] && unset -v "$2" && eval $2=\(\"\${@:3:${1#-a}}\"\) && shift $((${1#-a} + 2)) || { 
                    echo "bash: ${FUNCNAME[0]}:" "\`$1${2+ }$2': missing argument(s)" 1>&2;
                    return 1
                }
            ;;
            -v)
                [[ -n "$2" ]] && unset -v "$2" && eval $2=\"\$3\" && shift 3 || { 
                    echo "bash: ${FUNCNAME[0]}: $1: missing" "argument(s)" 1>&2;
                    return 1
                }
            ;;
            *)
                echo "bash: ${FUNCNAME[0]}: $1: invalid option" 1>&2;
                return 1
            ;;
        esac;
    done
}
_usb_ids () 
{ 
    COMPREPLY=(${COMPREPLY[@]:-} $( compgen -W         "$( PATH="$PATH:/sbin" lsusb | awk '{print $6}' )" -- "$cur" ))
}
_user_at_host () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref -n : cur;
    if [[ $cur == *@* ]]; then
        _known_hosts_real "$cur";
    else
        COMPREPLY=($( compgen -u -- "$cur" ));
    fi;
    return 0
}
_useradd () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -c | --comment | -h | --help | -e | --expiredate | -f | --inactive | -k | --key | -p | --password | -u | --uid | -Z | --selinux-user)
            return 0
        ;;
        -b | --base-dir | -d | --home | -k | --skel)
            _filedir -d;
            return 0
        ;;
        -g | --gid)
            _gids;
            COMPREPLY=($( compgen -W '${COMPREPLY[@]} $( compgen -g )'                 -- "$cur" ));
            return 0
        ;;
        -G | --groups)
            COMPREPLY=($( compgen -g -- "$cur" ));
            return 0
        ;;
        -s | --shell)
            _shells;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--base-dir --comment --home-dir --defaults \
            --expiredate --inactive --gid --groups --help --skel --key \
            --no-log-init --create-home --no-create-home --no-user-group \
            --non-unique --password --system --shell --uid --user-group \
            --selinux-user' -- "$cur" ));
        return 0;
    fi
}
_userdel () 
{ 
    local cur;
    COMPREPLY=();
    _get_comp_words_by_ref cur;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--force --help --remove' -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -u -- "$cur" ))
}
_usergroup () 
{ 
    local IFS='
';
    if [[ $cur = *\\\\* || $cur = *:*:* ]]; then
        return;
    else
        if [[ $cur = *\\:* ]]; then
            local prefix;
            prefix=${cur%%*([^:])};
            prefix=${prefix//\\};
            COMPREPLY=($( compgen -P "$prefix" -g -- "${cur#*[:]}" ));
        else
            if [[ $cur = *:* ]]; then
                COMPREPLY=($( compgen -g -- "${cur#*:}" ));
            else
                COMPREPLY=($( compgen -u -- "$cur" ));
            fi;
        fi;
    fi
}
_usermod () 
{ 
    local cur prev split=false;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    _split_longopt && split=true;
    case $prev in 
        -c | --comment | -d | --home | -e | --expiredate | -f | --inactive | -h | --help | -l | --login | -p | --password | -u | --uid | -Z | --selinux-user)
            return 0
        ;;
        -g | --gid)
            _gids;
            COMPREPLY=($( compgen -W '${COMPREPLY[@]} $( compgen -g )'                 -- "$cur" ));
            return 0
        ;;
        -G | --groups)
            COMPREPLY=($( compgen -g -- "$cur" ));
            return 0
        ;;
        -s | --shell)
            _shells;
            return 0
        ;;
    esac;
    $split && return 0;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--append --comment --home --expiredate \
            --inactive --gid --groups --help --login --lock --move-home \
            --non-unique --password --shell --uid --unlock --selinux-user'             -- "$cur" ));
        return 0;
    fi;
    COMPREPLY=($( compgen -u -- "$cur" ))
}
_vipw () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -h | --help)
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--group --help --passwd \
            --quiet --shadow' -- "$cur" ));
        return 0;
    fi
}
_xhost () 
{ 
    local cur;
    _get_comp_words_by_ref cur;
    case $cur in 
        +*)
            _known_hosts_real -p+ "${cur:1}"
        ;;
        -*)
            _known_hosts_real -p- "${cur:1}"
        ;;
        *)
            _known_hosts_real "$cur"
        ;;
    esac;
    return 0
}
_xmllint () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        -o | --output)
            _filedir;
            return 0
        ;;
        --path | --dtdvalidfpi | --maxmem | --encode | --pattern)
            return 0
        ;;
        --dtdvalid)
            _filedir dtd;
            return 0
        ;;
        --relaxng)
            _filedir rng;
            return 0
        ;;
        --schema)
            _filedir xsd;
            return 0
        ;;
        --schematron)
            _filedir sch;
            return 0
        ;;
    esac;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '$( xmllint --help 2>&1 | \
            sed -ne "s/^[[:space:]]*\(--[^[:space:]:]*\).*/\1/p" ) \
            -o' -- "$cur" ));
        return 0;
    fi;
    _filedir '@(*ml|htm|svg)'
}
_xrandr () 
{ 
    local cur prev output modes;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        --output)
            local outputs=$(xrandr|awk '/connected/ {print $1}');
            COMPREPLY=($(compgen -W "$outputs" -- "$cur"));
            return 0
        ;;
        --mode)
            for ((i = 1; i < COMP_CWORD; i++ ))
            do
                if [[ "${COMP_WORDS[i]}" == "--output" ]]; then
                    output=${COMP_WORDS[i+1]};
                    break;
                fi;
            done;
            modes=$(xrandr|sed -e "1,/$output/ d"                 -e "/connected/,$ d"|awk '{print $1}');
            COMPREPLY=($( compgen -W "$modes" -- "$cur"));
            return 0
        ;;
    esac;
    case $cur in 
        *)
            COMPREPLY=($(compgen -W '-display -help --orientation --query \
                --size --rate --version -x -y --screen --verbose --dryrun \
                --prop --fb --fbmm --dpi --output --auto --mode --preferred \
                --pos --reflect --rotate --left-of --right-of --above --below \
                --same-as --set --off --crtc --newmode --rmmode --addmode \
                --delmode' -- "$cur"));
            return 0
        ;;
    esac;
    return 0
}
_xsltproc () 
{ 
    local cur prev;
    COMPREPLY=();
    _get_comp_words_by_ref cur prev;
    case $prev in 
        --output | -o)
            _filedir;
            return 0
        ;;
        --maxdepth)
            return 0
        ;;
        --encoding)
            COMPREPLY=($( compgen -W "$( iconv -l | sed -e '/^UTF[1378]/d'                 -e '/^ISO[0-9_]/d' -e '/^8859/d' -e 's/\/.*//')" -- "$cur" ));
            return 0
        ;;
        --param | --stringparam)
            return 0
        ;;
        --path)
            _filedir -d;
            return 0
        ;;
        --writesubtree)
            _filedir -d;
            return 0
        ;;
    esac;
    [[ $COMP_CWORD -gt 2 && `_get_cword '' 2` == --?(string)param ]] && return 0;
    if [[ "$cur" == -* ]]; then
        _longopt xsltproc;
    else
        _filedir '@(xsl|xslt|xml)';
    fi
}
_xz () 
{ 
    COMPREPLY=();
    local cur prev;
    _get_comp_words_by_ref cur prev;
    if [[ "$cur" == -* ]]; then
        COMPREPLY=($( compgen -W '--compress --decompress --test --list \
            --keep --force --stdout --suffix --files --files0 --format --check \
            -0 -1 -2 -3 -4 -5 -6 -7 -8 -9 --fast --best --extreme --memory \
            --lzma1 --lzma2 --x86 --powerpc --ia64 --arm --armthumb --sparc \
            --delta --quiet --verbose --no-warn --help --long-help --version'             -- "$cur" ));
        return 0;
    fi;
    local split=false;
    _split_longopt && split=true;
    local xspec="*.@(xz|lzma|txz|tlz)";
    case $prev in 
        --decompress | --list | --test | -!(-*)[dlt]*)
            xspec="!"$xspec
        ;;
        --files | --files0)
            _filedir;
            return 0
        ;;
        -C | --check)
            COMPREPLY=($( compgen -W 'crc32 crc64 sha256' -- "$cur" ));
            return 0
        ;;
        -F | --format)
            COMPREPLY=($( compgen -W 'auto xz lzma raw' -- "$cur" ));
            return 0
        ;;
        -M | --memory | -S | --suffix | --delta | --lzma1 | --lzma2)
            return 0
        ;;
        -h | --help | -H | --long-help | -V | --version)
            return 0
        ;;
    esac;
    $split && return 0;
    _expand || return 0;
    local IFS='	
';
    COMPREPLY=($( compgen -f -X "$xspec" -- "$cur" ) $( compgen -d -- "$cur" ))
}
dequote () 
{ 
    eval echo "$1" 2> /dev/null
}
quote () 
{ 
    echo \'${1//\'/\'\\\'\'}\'
}
quote_readline () 
{ 
    local quoted;
    _quote_readline_by_ref "$1" ret;
    printf %s "$ret"
}
