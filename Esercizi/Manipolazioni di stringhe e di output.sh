#!/bin/bash

### ESEMPI DI TAGLIO DELLE STRINGHE CON LE ESPRESSIONI REGOLARI [REGEXP] (manipolazione di stringhe) ###
# Il taglio delle stringhe con queste espressioni regolari si usa all'interno delle variabili'

  VAR="questa.sarebbe.una.stringa.di.esempio"
echo "------------------------------------------------------------------"
echo -e "          TAGLIO di stringhe con le espressioni regolari\n"
  # Tagliare la parte iniziale di una stringa
  echo ${VAR#*.}      # --> sarebbe.una.stringa.di.esempio
  echo ${VAR##*.}     # --> esempio
  # Rimuovere sottostringhe dall'inizio
  echo ${VAR##questa} # --> .sarebbe.una.stringa.di.esempio

  # Tagliare la parte finale di una stringa
  echo ${VAR%.*}      # --> questa.sarebbe.una.stringa.di
  echo ${VAR%%.*}     # --> questa
  # Rimuovere sottostringhe dalla fine
  echo ${VAR%%esempio} # --> questa.sarebbe.una.stringa.di.

  # Selezionare una sottostringa specifica
  echo ${VAR:7:37}    # --> sarebbe.una.stringa.di.esempio

  # Sostituire una sottostringa con un'altra (solo la prima volta che viene incontrata)
  echo ${VAR/st/ST}   # --> queSTa.sarebbe.una.stringa.di.esempio
  # Sostituire una sottostringa con un'altra (tutte le volte che viene incontrata)
  echo ${VAR//st/ST}  # --> queSTa.sarebbe.una.STringa.di.esempio
echo "-------------------------------------------------------------------"

### ESEMPI DI TAGLIO DELLE STRINGHE CON L'UTILIZZO DI COMANDI SPECIFICI (manipolazione di stringhe) ###
# Questa forma di manipolazione di stringhe può essere utile senza usare alcuna variabile 

echo -e "             TAGLIO di stringhe con comandi e regexp\n"
  CPU_TYPE_a=`grep model\ name /proc/cpuinfo|sort -u` # comando di partenza
  CPU_TYPE_b=`grep model\ name /proc/cpuinfo | cut -d":" -f2 |sort -u`
 
  echo "Output da manipolare: $CPU_TYPE_a"
  echo "[REGEXP]Questo è il mio processore: ${CPU_TYPE_a#*:}"
  echo "[COMANDO]Questo è il mio processore: $CPU_TYPE_b"
echo "-------------------------------------------------------------------"

### USO DI "basename" E "dirname" PER LA MANIPOLAZIONE DEL PATHNAME ###
echo -e "                   USO DI basename E dirname\n"
# basename, toglie la stringa del percorso e lascia solo il nome del file
  dir1=`basename "$0"` # $0 è questo stesso script
  echo "[basename]: il nome dello script è: $dir1"

# dirname, toglie il nome del file e lascia la stringa del percorso
  dir2=`dirname "$0"`
  echo "[dirname]: il nome del percorso è: $dir2"
echo "-------------------------------------------------------------------"

### ALTERNATIVA A basename CON LE ESPRESSIONI REGOLARI  ###
echo -e "          ALTERNATIVA A basename con le espressioni regolari\n"
echo -e "Quando in uno script ci si deve riferire al nome dello script stesso\nè usuale utilizzare il comando (esterno a bash) basename.\nTuttavia, tramite i modificatori delle espressioni regolari, Bash stessa\nè in grado di fornire questa funzionalità. Basta usare l'espressione: $\{0##*/}.\n"
echo "Esempio:"

echo "${0##*/}"
echo "-------------------------------------------------------------------"

### COME ESTRARRE NOME FILE ED ESTENSIONE DA UN PATH

# Continuano le piccole note di bash. Stavolta vediamo un paio di facili comandi per estrarre nome file ed estensione da un path. Inserisco il tutto in un piccolo script che analizza tutti i file nella directory corrente e ne stampa prima il nome e poi l’estensione.

for file in $(ls) ; do
  filename=${file##*/}
  basename=${filename%\.*}
  extension=${filename##*.}
  echo Nome: ${basename}, estensione: ${extension}
done
