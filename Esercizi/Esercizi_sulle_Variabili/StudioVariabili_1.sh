#!/bin/bash
#
# by Gianluca Pernigotto

# CHE COSA SONO LE VARIABILI?
# Una variabile � una porzione di memoria e il suo nome � il contenitore del suo valore;
# quindi un contenitore contenente dei dati di qualsiasi tipo: numeri, stringhe, comandi
# anche molto lunghi e possono contenere altre variabili.
# Questi valori diventano la definizione stessa delle variabili, e siccome i valori possono
# essere tanti e diversi, allora la stessa definizione di variabile � appunto "variabile".

# Per definire una variabile � necessario prima dichiararla attraverso un identificatore (nome),
# il quale � costituito da caratteri alfanumerici. Pu� essere incluso il simbolo underscore (_) l'unico ammesso. 
# Il primo carattere della dichiarazione pu� essere soltanto una lettera o l'underscore. 
# Poi attraverso l'operatore di assegnamento (=), si procede a inizializzarla con un valore,
# ma in certi casi si pu� definire una variabile anche senza un valore, basta creare solo il nome: variabile
# Dopo averla definita la si usa con una chiamata nella procedura di uno script anteponendogli
# il simbolo del dollaro $:

# esempi: definisco una variabile, la dichiaro con MioIndirizzo e la inizializzo assegnandogli una stringa: 
MioIndirizzo="San Bonifacio, via dei pitti N�123 37047 VR"


# chiamata alla variabile
echo $MioIndirizzo # qui serve echo perch� abbiamo usato stringhe per creare la variabile.

#
# Ci sono vari tipi di variabili, ma qui ne vediamo due. Le altre le vedremo pi� avanti:
#
# VARIABILI LOCALI: sono delle variabili definite momentaneamente che possono essere create
#                   sugli script per esempio. Possono essere richiamate solo dove vengono definite (create).
#                   Ma una volta che si chiude lo script o il terminale poi vengono eliminate.
#                   la variabile MioIndirizzo � una variabile locale.
#                   
# VARIABILI DI AMBIENTE: Sono delle variabili impostate in modo predefinito e analoghe su
#                        tutti i sistemi operativi UNIX-LIKE: Slackware, Debian, MacOSX, BSD etc.
#                        Queste variabili sono scritte solitamente in maiuscolo tipo $USER, $HOME,
#                        $LANG, $UID etc. Per una lista completa digita sul terminale: env
#                        Sono variabili gi� pronte per l'uso.
#                        Qui sotto qualche esempio di uso delle variabili di ambiente:

echo $HOME
echo $LANG
echo $USER
#oppure
echo "La mia casa �:$HOME. La mia lingua �:$LANG. Il mio user �:$USER."

# I prossimi studi interesseranno altri tipi di variabili: SOSTITUZIONE DI COMANDO, ESPANSIONE DI VARIABILI, VARIABILI SPECIALI.
# sederino rosso eh !
