#!/bin/bash

# AUTORE: JeanLuc Pernig� 
#
# Script usato come test per verificare l'utente di un computer.
# questi che stai leggendo sono solo dei commenti descrittivi.
# tutto quello che viene scritto dopo il cancelletto '#', � un commento
# e non influenza il codice reale di qualsiasi tipo di linguaggio di progr.

echo "Ciao, io so qual'� il tuo nome, ma prova a dirmelo tu..." # echo � uno stampatore, enunciatore
echo "...Quale � il tuo nome di login ?"

read rispondi   #read aspetta un input di immissione; definisco la variabile 
#rispondi ma non le assegno alcun valore. .

if [ $rispondi = $USER ] #costrutto condizionale if, le parentesi quadre sono dei test,
# inizializzo la variabile rispondi, = � un confronto con la variabile di ambiente $USER

then # quindi
      echo "Si, � esatto !!"

else # altrimenti
      echo "No, sei un bugiardo/a, $rispondi non � il tuo nome di login"
      
fi # concludo il costrutto condizionale
      
exit 0  #esce dall'esecuzione dello script


