#!/bin/bash

# by Gianluca Pernigotto

                                            ## INTRODUZIONE ALLE VARIABILI ED AI PARAMETRI ##

# Una variabile è una porzione di memoria e il suo nome è il contenitore del suo valore, un contenitore contenente dei dati.
# Questi valori diventano la definizione stessa delle variabili, e siccome i valori possono essere tanti e diversi, allora la stessa definizione di variabile è appunto "variabile".

# Per definire una variabile è necessario prima dichiararla attraverso un identificatore (nome), il quale è costituito da caratteri alfanumerici e dal simbolo underscore (_) l'unico ammesso. Il primo carattere della dichiarazione può essere soltanto una lettera o l'underscore. Poi attraverso l'operatore di assegnamento (=), si procede a inizializzarla con le stringhe: inserendo caratteri alfanumerici tra gli apici, o con interi: ossia inizializzata con caratteri numerici.

                                                        ## TIPI DI VARIABILI ##

# 1) VARIABILI LOCALI: sono variabili visibili solo all'interno di un blocco di codice o funzione (vedi anche variabili locali in funzioni), fanno parte solo del contesto in cui si utilizzano (script)

# 2) VARIABILI D'AMBIENTE: sono variabili relative al comportamento della shell e all'interfaccia utente, solitamente scritte in maiuscolo, non sono necessariamente le stesse su ogni sistema GNU/Linux. Per una lista si usi il comando: env
# Esempio: $PWD, $USER, $LOGNAME, etc, sono tutte variabili d'ambiente.

# 3) SOSTITUZIONE DI COMANDO: Si attua quando il valore della variabile è un comando, solitamente lungo o complesso e che viene sintetizzato sul nome stesso della variabile. Viene immesso dopo l'operatore di assegnamento (=) e deve essere quotato dagli apici inversi (``), questo per maggiore portabilità, oppure dalle parentesi tonde ( ). ESEMPI:
CPU_TYPE=`grep model\ name /proc/cpuinfo|sort -u|cut -d: -f2`
echo "Tipo di processore installato:" $CPU_TYPE 

# 4) ESPANSIONE DI VARIABILI: L'espansione di parametro consiste nel inizializzare una variabile con il nome o il valore di un'altra variabile. É permessa anche la concatenazione di più variabile in espansione mediante parentesi graffe: NOME_PERCORSO=${USER}${PWD}. L'alternativa delle parentesi graffe consente una gestione più pulita e non si avranno errori qualora l'espansione sia di due e più variabili. Esempi:
BASHPATH_SISTEMA=${SHELL}__${OSTYPE}
echo "Percorso della bash e il sistema operativo in uso:" $BASHPATH_SISTEMA  

user_lingua="${USER} -- ${LANG}" # si noti i doppi apici per far interpretare anche gli spazi tra $0 e $LANG
echo "Il mio nome e la lingua impostata:" $user_lingua

# 5) PARAMETRI POSIZIONALI: rappresentano gli argomenti passati allo script da riga di comando: $1, $2, $3  ${10}. . .
dir=`basename "$0"` # notare la sostituzione di comando per non includere anche il percorso
echo il nome dello script è: $dir
echo il primo argomento è $1
echo il secondo argomento è $2
echo il diciassettesimo argomento è ${17}
echo il numero degli argomenti è $#

# $0 è il nome dello script stesso, $1 è il primo argomento, $2 il secondo, $3 il terzo, ecc.. [1] Dopo $9 il numero degli argomenti deve essere racchiuso tra parentesi graffe, per esempio, ${10}, ${11}, ${12}. I parametri posizionali includono anche le variabili speciali:

#    VARIABILI SPECIALI: 
#    $0 : il nome dello script in esecuzione 
#    $* : tutti i parametri posizionali in una stringa unica (non quota)
#    $@ : tutti parametri posizionali suddivisi in stringhe (quota i parametri passati)
#    $# : il numero di tutti i parametri posizionali
#    $? : contiene il valore di uscita dell'ultimo comando o funzione. Il comando ha successo se ritorna zero, qualsiasi altro valore indica invece un codice di errore.
#    $n : Recupera il valore di uno specifico argomento su linea di comando, dove n varia tra 1 e 9. $1 corrisponde al primo argomento da sinistra
#    $- : Le opzioni  (flag) utilizzate dalla shell in esecuzione 
#    $$	: numero ID del processo corrente
#    $!	: numero ID del processo dell'ultimo comando messo in background
#    $_ : restituisce l'ultimo argomento del comando precedente





                                       ## USO DELLE VARIABILI SPECIALI ##

echo -e "\$? contiene il valore di uscita dell'ultimo comando o funzione. Il comando\nha successo se ritorna zero, qualsiasi altro valore indica invece\nun codice di errore;" 
echo "$?"
 
echo -e "\$@ contiene la lista dei parametri passati allo script corrente. Ogni parametro viene opportunamente quotato, questo permette l'utilizzo di questa variabile nei cicli for per processare (ad esempio) una lista di nomi di file che possono contenere anche spazi. L'uso di questa variabile è quindi in genere consigliato rispetto a \$* che ha la stessa funzione ma non quota i vari parametri;" 
echo $@
echo Esempio\: 

FILES=$@

for file in $FILES; do
    # Fare quello che si vuole con $file
    echo $file   
    # ...
done

  
echo "\$$ PID del processo corrente;" 
echo $$
echo "\$! PID dell'ultimo job in background." 
echo $!

