#!/bin/bash


clear
############ COLORE CARATTERI ####################
COLORE="\e[1;37m" # Alternativa di codice \e = \033
BLACK="\033[30m"
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
MAGENTA="\033[35m"
CYAN="\033[36m"
WHITE="\033[37m"

########### COLORE SFONDI #######################
YELLOW_DEFAULT="\033[43m" # Colora solo lo sfondo
WHITE_BK='\E[47;30m' # Colora lo sfondo e il carattere
GREEN_WH='\E[42;37m'
BLUE_WH='\e[44;37m'
WHITE_BL='\e[47;34m'
RED_YL='\e[41;33m'

########### SPECIALI ############################
BOLD="\e[1m" # Evidenzia
DIM="\e[2m" # Sbiadisce, opacizza
UNDERLINE="\e[4m" # sottolinea
REVERSE="\e[7m" # evidenzia: gira i colori


BOLD_C="\e[22m" # Stato iniziale
UNDERLINE_C="\e[24m" # stato iniziale
REVERSE_C="\e[27m" # stato iniziale

DEFAULT="\e[0m" # Torna allo stato iniziale

############### VARIABILI di FORMATTAZIONE #######################
INTERLINE="--------------------------------------------------------------------"

############### MODELLI PREDEFINITI ########################
clear
echo -e "       $BLUE $BOLD==================== ${WHITE_BL}NOME-PROGRAMMA V.0.0${DEFAULT}$BLUE$BOLD =====================$DEFAULT"
echo -e "        $BOLD bla bla bla bla bla, mettilo in centro, bla bla bla$DEFAULT"
echo -e "         Autore: $RED Jean-Luc Pernigò$DEFAULT --- mail: $RED jeanlucperni@gmail.com$DEFAULT"
echo -e "       $BLUE $BOLD===============================================================$DEFAULT"
echo
echo -e " ${UNDERLINE}${BOLD}Ciao $USER, benvenuto!$DEFAULT\n"

############## STAMPA VISUALIZZAZIONE COLORI ###################
echo -e "COLORI SEMPLICI\n"

echo -e "${BLACK} BLACK ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${RED} RED ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${GREEN} VERDE ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${YELLOW} YELLOW ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BLUE} BLUE ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${MAGENTA} MAGENTA ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${CYAN} CYAN ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${WHITE} WHITE ===== PROVA DI STAMPA ... --- ... Prova di stampa =====${DEFAULT}\n\n"


echo -e "COLORI SBIADITI, OPACHI\n"

echo -e "${DIM} COLOR DEFAULT DIM ===== PROVA DI STAMPA REVERSE ... --- ...Prova di stampa =====${DEFAULT}\n"
echo -e "${DIM}${BLACK} BLACK DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${RED} RED DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${GREEN} VERDE DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${YELLOW} YELLOW DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${BLUE} BLUE DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${MAGENTA} MAGENTA DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${CYAN} CYAN DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${DIM}${WHITE} WHITE DIM ===== PROVA DI STAMPA ... --- ... Prova di stampa =====${DEFAULT}\n\n"


echo -e "COLORI BRILLANTI O BOLD\n"

echo -e "${BOLD}${BLACK} BLACK BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${RED} RED BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${GREEN} VERDE BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${YELLOW} YELLOW BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${BLUE} BLUE BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${MAGENTA} MAGENTA BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${CYAN} CYAN BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====\n"
echo -e "${BOLD}${WHITE} WHITE BOLD ===== PROVA DI STAMPA ... --- ... Prova di stampa =====${DEFAULT}\${DEFAULT}\n\n"


echo -e "COLORI SFONDI\n"

echo -e "${WHITE_BK} \${WHITE_BK} ===== PROVA DI STAMPA ... --- ... Prova di stampa =====${DEFAULT}\n"
echo -e "${BOLD}${WHITE_BK} \${BOLD}\${WHITE_BK} ==== PROVA DI STAMPA ... --- ... Prova di stampa ====${DEFAULT}\n"
echo -e "${BOLD}${WHITE_BL} \${BOLD}\${WHITE_BL}==== PROVA DI STAMPA ... --- ... Prova di stampa ====${DEFAULT}\n"
echo -e "${GREEN_WH} \${GREEN_WH} ==== PROVA DI STAMPA ... --- ... Prova di stampa ====${DEFAULT}\n"
echo -e "${BOLD}${BLUE_WH} \${BOLD}\${BLUE_WH} ======= PROVA DI STAMPA ... --- ... Prova di stampa ====${DEFAULT}\n"
echo -e "${BOLD}${RED_YL} \${BOLD}\${RED_YL} ======= PROVA DI STAMPA ... --- ... Prova di stampa ====${DEFAULT}\n"
echo -e "${REVERSE} \${REVERSE} ==== PROVA DI STAMPA REVERSE ... --- ...$REVERSE_C\$REVERSE_C Prova di stampa ====${DEFAULT}\${DEFAULT}\n"


# ex30a.sh: Versione di ex30.sh "a colori".
#           Un database di indirizzi non molto elegante




echo -n "          "
echo -e '\E[37;44m'"\033[1mElenco Contatti\033[0m"
                                        # Bianco su sfondo blu
echo; echo
echo -e "\033[1mScegliete una delle persone seguenti:\033[0m"
                                        # Grassetto
tput sgr0
echo "(Inserite solo la prima lettera del nome.)"
echo
echo -en '\E[47;34m'"\033[1mE\033[0m"   # Blu
tput sgr0                               # Ripristina i colori "normali."
echo "vans, Roland"                     # "[E]vans, Roland"
echo -en '\E[47;35m'"\033[1mJ\033[0m"   # Magenta
tput sgr0
echo "ones, Mildred"
echo -en '\E[47;32m'"\033[1mS\033[0m"   # Verde
tput sgr0
echo "mith, Julie"
echo -en '\E[47;31m'"\033[1mZ\033[0m"   # Rosso
tput sgr0
echo "ane, Morris"
echo

read persona

case "$persona" in
# Notate l'uso del "quoting" per la variabile.

  "E" | "e" )
  # Accetta sia una lettera maiuscola che una minuscola.
  echo
  echo "Roland Evans"
  echo "4321 Floppy Dr."
  echo "Hardscrabble, CO 80753"
  echo "(303) 734-9874"
  echo "(303) 734-9892 fax"
  echo "revans@zzy.net"
  echo "Socio d'affari & vecchio amico"
  ;;

  "J" | "j" )
  echo
  echo "Mildred Jones"
  echo "249 E. 7th St., Apt. 19"
  echo "New York, NY 10009"
  echo "(212) 533-2814"
  echo "(212) 533-9972 fax"
  echo "milliej@loisaida.com"
  echo "Fidanzata"
  echo "Compleanno: Feb. 11"
  ;;

# Aggiungete in seguito le informazioni per Smith & Zane.

          * )
   # Opzione preefinita.
   # Anche un input vuoto (è stato premuto il tasto INVIO) viene verificato qui.
   echo
   echo "Non ancora inserito nel database."
  ;;

esac

tput sgr0                               # Ripristina i colori "normali."

echo

exit 0













