#!/bin/bash

# ESPANSIONE DI STRINGA.
# Introdotta con la versione 2 di Bash.

#  Le stringhe nella forma $'xxx'
#+ consentono l'interpretazione delle sequenze di escape standard.
#echo $'Tre segnali acustici \a \a \a'
     # Su alcuni terminali potrebbe venir eseguito un solo segnale acustico.
echo $'Tre form feed \f \f \f'
echo $'10 ritorni a capo \n\n\n\n\n\n\n\n\n\n'
echo $'\102\141\163\150'   # Bash
                           # Valori ottali di ciascun carattere.
echo $'\e[37;44m Si ma no voglio io\nin quanto conosco\033[0m'
exit 0
