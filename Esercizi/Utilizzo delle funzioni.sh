#!/bin/bash

# UNA FUNZIONE È UN BLOCCO DI CODICE RICHIAMABILE NEL CORSO DI UNO SCRIPT, MA PUÒ ESSERE ANCHE SEPARATA DALLO STESSO ED ESSERE UGUALMENTE RICHIAMATA. 
# LA DEFINIZIONE DELLA FUNZIONE DEVE PRECEDERE LA SUA PRIMA CHIAMATA, CIOÉ PRIMA DEVE ESSERE DEFINITA E POI INVOCATA.
# Ogni qual volta vi è del codice che si ripete o quando un compito viene iterato con leggere variazioni, allora è il momento di prendere in considerazione l'impiego di una funzione.
# Il modo corretto per definire una VARIABILE su una funzione è quello di farla precedere da local, poichè, in questa maniera non viene esportata ma esiste solo all'interno dello stesso script cioè in maniera locale.
# A differenza del C, una variabile Bash dichiarata all'interno di una funzione è locale "solo" se viene dichiarata come tale.
# ATTENZIONE: Prima che una funzione venga richiamata, tutte le variabili dichiarate all'interno della funzione sono invisibili al di fuori del corpo della funzione stessa, non soltanto quelle esplicitamente dichiarate come locali. 
# Le funzioni sono indipendenti dentro o fuori di uno script, come se fosse uno script a parte; le variabili dichiarate nella funzione, per esempio, sono viste solo nella funzione stessa 
# esempio:

# Variabili globali e locali in una funzione.

funz ()
{
  local var_locale=23       # Dichiarata come variabile locale.
  echo                      # Utilizza il builtin 'local'.
  echo "\"var_locale\" nella funzione = $var_locale"
  var_globale=999           # Non dichiarata come locale.
                            # Viene impostata per default come globale.
  echo "\"var_globale\" nella funzione = $var_globale"

  return 0 # Notate return 0 al posto di exit 0 per la funzione, altrimenti si esce dallo script se viene messo exit 0 .
}

funz

#  Adesso controlliamo se la variabile locale "var_locale" esiste al di fuori
#+ della funzione.

echo
echo "\"var_locale\" al di fuori della funzione = $var_locale"
                              # $var_locale al di fuori della funzione =
                              # No, $var_locale non ha visibilità globale.
echo "\"var_globale\" al di fuori della funzione = $var_globale"
                              # $var_globale al di fuori della funzione = 999
                              # $var_globale è visibile globalmente
echo

exit 0
#  A differenza del C, una variabile Bash dichiarata all'interno di una funzione
#+ è locale "solo" se viene dichiarata come tale.


## STILI DIVERSI DI DEFINIRE UNA FUNZIONE

function nome_funzione {   # Prima forma di una funzione 
comando...
} 

# oppure:

nome_funzione () {   # Questa seconda forma è quella che rallegra i cuori dei programmatori C (ed è più portabile).
comando...
} 
 

nome_funzione ()   # Come nel C, la parentesi graffa aperta può, opzionalmente, comparire nella riga successiva a quella del nome della funzione.
{
comando...
} 



# Una funzione può essere "compattata" su un'unica riga.
fun () { echo "Questa è una funzione"; echo; }

# In questo caso, però, occorre mettere un punto e virgola dopo l'ultimo comando della funzione.
fun () { echo "Questa è una funzione"; echo } # Errore!

# Le funzioni vengono richiamate, messe in esecuzione, semplicemente invocando i loro nomi.


# Le funzioni possono essere anche annidate, sebbene non sia molto utile:

funz1 ()
{

  funz2 () # annidata
  {
      echo "Funzione \"f2\", all'interno di \"f1\"."
  }

} 

exit 0



