#!/bin/bash
clear
echo "Sono lo script-2 che hai appena richiamato"
echo 
echo "Qui facciamo tutte le nostre operazioni che vogliamo..."
echo "per esempio questa:"
echo "Scrivere la lingua: italiano o inglese"
read lingua

                if test $lingua = "italiano" 
                then
echo "Ciao!"                
      
                elif test $lingua = "inglese" 
                then
echo "Hello!"
      
                else
echo "Non conosco questa lingua"
exit 1
                fi # chiudo il primo if annidato
echo
echo " Vuoi ritornare allo script di prima cioè lo script-1 ?"
echo
echo '"y" per eseguirlo, "n" per continuare con questo o "d" per chiudere:' 
read var
         if   [[ $var =  [yY] ]] 
         then 
              "$PWD/bin/Prove/Richiamo di 2 script/"./script-1.sh   
         
exit 0            # "exit 0" vanno annidati anch'essi sugli operatori condizionali (in questo caso), sotto a "echo" e prima di "fi"
         elif [[ $var =  [nN] ]]
         then 
clear 
              echo "non ritorno allo script-1, continuo..."
              echo "..e qui facciamo altre operazioni, comandi, etc, etc"
              echo "Arrivederci!"
exit 0

         elif [[ $var = [dD] ]]
         then 
              echo "Operazione conclusa"
              echo "Arrivederci!"
exit 0
         else 
              echo 'Non hai premuto il tasto corretto'
exit 1
         fi


