﻿#! /bin/bash 
#backup script:semplice esempio di backup tramite
#vengono settate le directory:
#la home dell'utente, la dir dove verranno conservati i backup in home
#la directory con gli host in /var/www
#la directory contentente i database di mysql

homedir="/home/user"
hbackups="/home/user/backups"
backnew="$hbackups/back_new/"
hostorig="/var/www"
mysqlorig="/var/lib/mysql"

cd $homedir

if [ ! -d "$homedir/backups" ]                #Controlla se esiste la directory backups nella home
then                                          #se non esiste viene creata
    mkdir "$homedir/backups"
fi

cd $hbackups

if [ -d "$hbackups/back_old" ]                #Controlla se esiste la directory back_old nella home
then                                          #se esiste viene rimossa
    rm -r "$hbackups/back_old"
fi

if [ -d "$hbackups/back_new" ]                #Controlla se esiste la directory back_new nella home
then                                          #se esiste viene passata ad old
    mv "$hbackups/back_new" "$hbackups/back_old"
fi

mkdir "$hbackups/back_new"                #Crea back_new

cp -ra $hostorig $backnew                 #Esegui backup degli host
cp -ra $mysqlorig $backnew                #Esegui backup mysql

cd $hbackups

tar -cjf backup-`date +%d%m%Y`.tar.bz2 back_new  #Crea l'archivio dei backup

exit