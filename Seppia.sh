#!/bin/bash

# "Opzioni di inserimento da 50 a 150. Valori consigliati: da 80 a 100
# convert -sepia-tone 80% "$@" sepia-tone.${format}
#for f in *.png; do convert -sepia-tone 80% "$f" "${f%.png}.png"; done
#----------------------------------------------------
# Valore Consigliato: min 101 max 300

#for f in *.png; do convert -colorize 250 "$f" "${f%.png}.png"; done

for f in *.png; do convert -colorize 50,25,10 "$f" "${f%.png}.png"; done

