#!/bin/bash

for module in $(find /lib/modules/$(uname -r)/kernel -name *.ko)  
 do  
     echo $(basename $module) | sed 's/\.ko//g' >> modules.list  
 done 
