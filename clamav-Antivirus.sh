#!/bin/sh

# eldino's automatic clamscan v1.0
# start an antivirus scan via command-line

# user variables
FOLDER_SCAN="/home/SCAMBIO"
LOG_TEMP="/home/amministratore/Scrivania/logs/clamav.temp"
LOG_FILE="/home/amministratore/Scrivania/logs/clamav.log"
VAULT="/home/amministratore/Scrivania/vault"
SHARED_FOLDER_LOG_NAME="risultati_scansione_antivirus_automatica.txt"

# some checks
if [ ! -f "$LOG_FILE" ]; then
touch "$LOG_FILE"
chmod 644 "$LOG_FILE"
fi

if [ ! -f "$LOG_TEMP" ]; then
touch "$LOG_TEMP"
chmod 644 "$LOG_TEMP"
fi

# format the date
dateToday=$(date "+%d-%m-%y")
timeNow=$(date "+%H:%M:%S")

# do the scan and save the raw log
/usr/bin/clamscan -r $FOLDER_SCAN --move=$VAULT > $LOG_TEMP

# polish the raw log and create the final log
echo "Scansione antivirus automatica:" $dateToday "-" $timeNow >> $LOG_FILE
cat $LOG_TEMP | grep "FOUND" >> $LOG_FILE
echo >> $LOG_FILE

# remove the raw log
rm -f $LOG_TEMP

# copy the final log into the shared folder
cp $LOG_FILE $FOLDER_SCAN/$SHARED_FOLDER_LOG_NAME

