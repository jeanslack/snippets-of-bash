#!/bin/bash


# mostra la finestra di selezione file e assegna il file scelto 
# che zenity manda allo standard output alla variabile "filescelto"
# notare i "backtick" -> ` `
filescelto=`zenity --file-selection --title="EasyAbcde: seleziona un file"`

# prende il valore di ritorno del comando precedente
ritorno=$?

# controlla se si Ã¨ premuto OK o Annulla
if [ "$ritorno" -ne 0 ]; then
  # Ã¨ stato premuto annulla o chiusa la finestra
  zenity --warning --title="Test di Zenity" --text="Hai annullato"
  exit 1
else
  # Ã¨ stato premuto OK
  zenity --info --title="Test di Zenity" --text="Hai scelto il file: '$filescelto'" #--width=300 --height=300

fi

