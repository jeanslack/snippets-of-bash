#!/bin/bash
#
# Author: Gianluca Pernigotto <jeanlucperni@gmail.com>
# 5 Agosto 2013
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Description: output print ffmpeg information video files for parsing

name=$1

if [ -e /tmp/analysisVid.txt ] ; then
	rm /tmp/analysisVid.txt
fi

audio=`ffmpeg -i "$name" 2>&1 | grep 'Audio:' | awk -F ':' '{ print $3 $4 $5 $6 $7 $8 $9 $10 }'`
echo "audio = $audio" > /tmp/analysisVid.txt

formato=`cat /tmp/analysisVid.txt | grep Audio | cut -d ' ' -f5`
freq=`cat /tmp/analysisVid.txt | grep Audio | cut -d ' ' -f9,10 | tr -d ,`
channel=`cat /tmp/analysisVid.txt | grep Audio | cut -d ' ' -f11 | tr -d ,`
bitres=`cat /tmp/analysisVid.txt | grep Audio | cut -d ' ' -f12 | tr -d ,`
bitrate=`cat /tmp/analysisVid.txt | grep Audio | cut -d ' ' -f13,14 | tr -d ,`

echo "formato = $formato" >> /tmp/analysisVid.txt
echo "samplerate = $freq" >> /tmp/analysisVid.txt
echo "channel = $channel" >> /tmp/analysisVid.txt
echo "bitdeph = $bitres" >> /tmp/analysisVid.txt
echo "bitrate = $bitrate" >> /tmp/analysisVid.txt



video=`ffmpeg -i "$name" 2>&1 | grep 'Video:' | awk -F ':' '{ print $3 $4 $5 $6 $7 $8 $9 }'` 
echo "video = $video" >> /tmp/analysisVid.txt

codecv=`cat /tmp/analysisVid.txt | grep Video | cut -d ' ' -f5`
#size=`cat /tmp/analysisVid.txt | grep Video | cut -d ' ' -f11 | tr -d ,`
#vbitrate=`cat /tmp/analysisVid.txt | grep Video | cut -d ' ' -f12,13 | tr -d ,`
#framerate=`cat /tmp/analysisVid.txt | grep Video | cut -d ' ' -f14,15 | tr -d ,`

echo "codecv = $codecv" >> /tmp/analysisVid.txt
#echo "size = $size" >> /tmp/analysisVid.txt
#echo "bitratev = $vbitrate" >> /tmp/analysisVid.txt
#echo "ratev = $framerate" >> /tmp/analysisVid.txt




