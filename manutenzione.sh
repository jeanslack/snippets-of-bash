#!/bin/bash
#Questo script effettua la manutenzione di un sistema operativo GNU/Linux Ubuntu

#------------------------------------------------------------------------------------------+
# Funzione aggiornamento                                                                   |
#------------------------------------------------------------------------------------------+
function aggiornamento {
echo
echo "********************** INIZIO AGGIORNAMENTO DEL SISTEMA **********************"
echo
echo -n "# Aggiorno gli indici dei pacchetti? Update "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get update
		echo "* Aggiornamento indici pacchetti...[OK]"
	else 
		echo "* Aggiornamento indici pacchetti...[NOT EXECUTED]"
fi
echo
echo -n "# Aggiorno le chiavi GPG degli indici dei pacchetti? GPG "
read sure
if [[ $sure = "s" ]]
	then
		touch err						
		sudo apt-get update 2>err
 		num=$(grep 'NO_PUBKEY' err | wc -l)	
		str=$(grep 'NO_PUBKEY'  err) 		
		tot=$num
		if [ $num != 0 ]			
			then
			echo "---RISOLVO NO_PUBKEY---"
			for (( i=0 ; i<$num ; i++ ))  	  
				do
				tmp=${str#*NO_PUBKEY }
				key=${tmp:0:16}
				str=${str#*NO_PUBKEY}
				echo "Aggiungo la chiave:$key"
				gpg --keyserver subkeys.pgp.net --recv $key			
				gpg --export --armor $key | sudo apt-key add -		
				done
		fi
	num=0
	num=$(grep 'BADSIG' err | wc -l)	
	str=$(grep 'BADSIG'  err) 		
	tot=$(( $tot + $num ))
	if [ $num != 0 ]			
		then
			echo "---RISOLVO BADSIG---"
			for (( i=0 ; i<$num ; i++ ))  	  
				do
				tmp=${str#*BADSIG }
				key=${tmp:0:16}
				str=${str#*BADSIG}
				echo "Aggiungo la chiave:$key"
				gpg --keyserver subkeys.pgp.net --recv $key			
				gpg --export --armor $key | sudo apt-key add -		
				done
	fi
	rm err					
	else 
		echo "* Aggiornamento chiavi GPG degli indici dei pacchetti...[NOT EXECUTED]"
fi
echo
echo -n "# Aggiorno i pacchetti? Upgrade "
read sure 
if [[ $sure = "s" ]]
	then
		sudo apt-get upgrade
		echo "* Aggiornamento pacchetti...[OK]"
	else 
		echo "* Aggiornamento pacchetti...[NOT EXECUTED]"
fi
echo
echo -n "# Aggiorno la data e l'ora del sistema? NTPDate"
read sure
if [[ $sure = "s" ]]
	then
		sudo ntpdate it.pool.ntp.org
		echo "* Aggiornamento data e ora...[OK]"
	else
		echo "* Aggiornamento data e ora...[NOT EXECUTED]"
fi
echo -n "# Aggiorno il kernel del sistema? Dist-Upgrade "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get dist-upgrade -f
		echo "* Aggiornamento kernel...[OK]"
	else
		echo "* Aggiornamento kernel...[NOT EXECUTED]"
fi
echo
echo -n "# Aggiorno l'immagine initramfs del kernel? Update - InitramFS "
read sure
if [[ $sure = "s" ]]
	then
		sudo update-initramfs -u
		echo "* Aggiornamento immagine initramfs...[OK]"
	else 
		echo "* Aggiornamento immagine initramfs...[NOT EXECUTED]"
fi
echo
echo -n "# Verifico che non ci siano pacchetti difettosi? Install -F "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get install -f
		echo "* Verifica pacchetti difettosi...[OK]"
	else
		echo "* Verifica pacchetti difettosi...[NOT EXECUTED]"
fi
echo
echo "ATTENZIONE: COMANDO POTENZIALMENTE PERICOLOSO!!"
echo -n "# Aggiorno le configurazioni di GRUB? Update - Grub "
read sure
if [[ $sure = "s" ]]
	then
		sudo cp /boot/grub/grub.cfg /boot/grub/grub.cfg.bak
		sudo update-grub
		echo "* Backup /boot/grub/grub.cfg...[OK]"
		echo "* Aggiornamento GRUB...[OK]"
	else 
		echo "* Aggiornamento GRUB...[NOT EXECUTED]"
fi
echo
echo "--------------------------- AGGIORNAMENTO TERMINATO --------------------------"
echo
}

#------------------------------------------------------------------------------------------+
# Funzione pulizia                                                                         |
#------------------------------------------------------------------------------------------+
function pulizia {
echo
echo "************************* INIZIO PULIZIA DEL SISTEMA *************************"
echo
echo -n "# Rimuovo le dipendenze inutili? Autoremove "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get --purge autoremove
		echo "* Rimozione dipendenze inutili...[OK]"
	else 
		echo "* Rimozione dipendenze inutili...[NOT EXECUTED]"
fi
echo
echo -n "# Rimuovo la cache dei pacchetti scaricati dai repository obsoleti? Autoclean "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get autoclean
		echo "* Rimozione cache pacchetti obsoleti...[OK]"
	else 
		echo "* Rimozione cache pacchetti obsoleti...[NOT EXECUTED]"
fi
echo
echo -n "# Rimuovo la cache dei pacchetti scaricati dai repository? Clean "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get clean
		echo "* Rimozione  cache pacchetti scaricati...[OK]"
	else
		echo "* Rimozione  cache pacchetti scaricati...[NOT EXECUTED]"
fi
echo
echo -n "# Elimino il contenuto di /temp? RM - Temp "
read sure
if [[ $sure = "s" ]]
	then
		sudo rm -rf /tmp/*
		echo "* Eliminazione contenuto /temp...[OK]"
	else 
		echo "* Eliminazione contenuto /temp...[NOT EXECUTED]" 
fi
echo
echo -n "# Elimino il contenuto della directory /.thumbnails? Thumbnails *"
read sure
if [[ $sure = "s" ]]
	then
		sudo rm -rfv ~/.thumbnails/*
		echo "* Eliminazione files da /.thumbnails ...[OK]"
	else 
		echo "*Eliminazione files da /.thumbnails ...[NOT EXECUTED]"
fi
echo
echo -n "# Effettuo una ricerca dei residui di files? .* "
read sure
if [[ $sure = "s" ]]
	then
	find $HOME -name "*~"
	find $HOME -name "Desktop.ini"
	find $HOME -name "Thumbs.db"
	echo -n "# Rimuovo questi residui di file? "
	read sure
	if [ $sure == "s" ]
		then
		find $HOME -name "*~" -print0|xargs -0 /bin/rm -f
		find $HOME -name "Desktop.ini" -print0|xargs -0 /bin/rm -f
		find $HOME -name "Thumbs.db" -print0|xargs -0 /bin/rm -f
		echo "* Eliminazione residui...[OK]"
	else echo "* Eliminazione residui...[NOT EXECUTED]"
	fi
else echo "* Ricerca residui di files...[NOT EXECUTED]"
fi
echo
echo -n "# Elimino il contenuto del Cestino? Trash *"
read sure
if [[ $sure = "s" ]]
	then
		sudo rm -rfv ~/.local/share/Trash/*
		echo "* Eliminazione files da /Trash ...[OK]"
	else
		echo "* Eliminazione files da /Trash ...[NOT EXECUTED]"
fi
echo
echo -n "# Pulisco i documenti recenti? Rcently "
read sure
if [[ $sure = "s" ]]
	then
		sudo mv ~/.recently-used.xbel .recently-used.xbel.bak
		sudo mv ~/.recently-used .recently-used.bak
		touch ~/.recently-used.xbel
		echo "<?xml version="1.0" encoding="UTF-8"?>\n<xbel version="1.0"\nxmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"\nxmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"\n></xbel>\n
 " > ~/.recently-used.xbel
		echo "* Pulizia documenti recenti...[OK]"
	else
		echo "* Pulizia documenti recenti...[NOT EXECUTED]"
fi
echo
echo
echo "Per l'esecuzione di questo comando verrà installato il pacchetto deborphan"
echo "ATTENZIONE: COMANDO POTENZIALMENTE PERICOLOSO!!"
echo -n "# Rimuovo le librerie orfane? Deborphan  "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get -y install deborphan
		sudo apt-get --purge remove `deborphan`
		echo "* Rimozione librerie orfane...[OK]"
	else 
		echo "* Rimozione librerie orfane...[NOT EXECUTED]"
fi
echo
echo "Per l'esecuzione di questo comando verrà installato il pacchetto deborphan"
echo "ATTENZIONE: COMANDO POTENZIALMENTE PERICOLOSO!!"
echo -n "# Rimuovo le librerie orfane di sviluppo? Deborphan Sviluppo "
read sure
if [[ $sure = "s" ]]
	then
		sudo apt-get -y install deborphan
		sudo apt-get --purge remove `deborphan --libdev`
		echo "* Rimozione librerie orfane...[OK]"
	else
		echo "* Rimozione librerie orfane...[NOT EXECUTED]"
fi
echo
echo "------------------------------ PULIZIA TERMINATA -----------------------------"
echo
}

#------------------------------------------------------------------------------------------+
# Programma principale                                                                     |
#------------------------------------------------------------------------------------------+
echo "+============================================================================+"
echo "|                           MANUTENZIONE DI UBUNTU                           |"
echo "|                                                                            |"
echo "|        Questo script effettua una manutenzione guidata del sistema         |"
echo "|        per rispondere affermativamente alle domande che verranno           |"  
echo "|        poste premete s e confermate premendo il tasto <invio>              |"
echo "|        per saltare la domanda premete <invio>                              |"
echo "|                                                                            |"
echo "+============================================================================+"
echo
echo -n "~~~ ESEGUIRE I COMANDI PER L'AGGIORNAMENTO DEL SISTEMA? "
read sure
if [[ $sure = "s" ]]
	then
		aggiornamento
	else 
		echo "*** Aggiornamento del sistema...[NOT EXECUTED]"
fi
echo
echo -n "~~~ ESEGUIRE I COMANDI PER LA PULIZIA DEL SISTEMA? "
read sure
if [[ $sure = "s" ]]
	then
		pulizia
	else 
		echo "*** Pulizia del sistema...[NOT EXECUTED]"
fi
echo
echo -n "~~~ E' CONSIGLIABILE RIAVVIARE IL SISTEMA. RIAVVIARE? "
read sure
if [[ $sure = "s" ]]
	then
		sudo reboot
	else 
		echo "*** Riavvio del sistema...[NOT EXECUTED]"
fi
echo
read -p "Premi <INVIO> per uscire"  
